## Related issues

<!-- Issues that are closed by or related to this merge request, e.g. "Closes XXX" or "Related to XXX" -->

...

## Acceptance criteria

<!-- Criteria for the MR to be considered mergeable -->

- [ ] Implemented code changes don't diverge from the related issue.
- [ ] Documentation reflects the status quo after merge.
- [ ] Version number is incremented according to [SemVer](https://semver.org/). (Snapshot versions are not merged/released.)
- [ ] Only released versions of dependencies are used (i.e. no snapshot versions).
- [ ] The changelog contains entries reflecting all changes of this MR (and their reasons).

## Proposed squash commit message

<!--
A proposed message for the eventual squashed commit.
Please stick to the following pattern:

- A short one-line summary (max. 50 characters).
- A blank line.
- A detailed explanation of the changes introduced by this merge request.
  Each line should not exceed 72 characters.
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
```
A short one-line summary (max. 50 characters)

* A more detailed explanation of the changes introduced by this merge
  request.
* Each line should not exceed 72 characters.

Co-authored-by: NAME <EMAIL>
```
<!--
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
