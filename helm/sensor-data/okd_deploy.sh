#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=iot-broker}"
FULLNAME="sensor-data"

# Upgrade or install
helm --set-string="fullnameOverride=${FULLNAME}" upgrade -n "$NAMESPACE" -i sensor-data . || exit 1
# Ensure image stream picks up the new docker image right away
oc -n "$NAMESPACE" import-image "${FULLNAME}-history-service"
oc -n "$NAMESPACE" import-image "${FULLNAME}-persistence-service"
oc -n "$NAMESPACE" import-image "${FULLNAME}-subscription-service"
