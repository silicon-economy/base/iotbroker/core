/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okio.Timeout;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.ClientErrorException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.HttpException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link DefaultRetrofitCallHandler}.
 *
 * @author M. Grzenia
 */
class DefaultRetrofitCallHandlerTest {

  /**
   * Class under test.
   */
  private DefaultRetrofitCallHandler retrofitCallHandler;
  /**
   * Test dependencies.
   */
  private RetrofitErrorResponseHandler retrofitErrorResponseHandler;

  @BeforeEach
  void setUp() {
    retrofitErrorResponseHandler = mock(RetrofitErrorResponseHandler.class);
    retrofitCallHandler = new DefaultRetrofitCallHandler(retrofitErrorResponseHandler);
  }

  @Test
  void execute_whenSuccessfulResponse_thenReturnsResponseBody() {
    // Arrange
    Call<String> call = new CallImpl(Response.success("successful response"));

    // Act
    String result = retrofitCallHandler.execute(call);

    // Assert & Verify
    assertThat(result).isEqualTo("successful response");
    verifyNoInteractions(retrofitErrorResponseHandler);
  }

  @Test
  void execute_whenUnsuccessfulResponse_thenDelegatesToErrorResponseHandler() {
    // Arrange
    ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"),
                                                    "unsuccessful response");
    Response<String> response = Response.error(400, responseBody);
    Call<String> call = new CallImpl(response);
    doThrow(ClientErrorException.class)
        .when(retrofitErrorResponseHandler).handleResponse(response);

    // Act & Assert
    assertThatThrownBy(() -> retrofitCallHandler.execute(call))
        .isInstanceOf(ClientErrorException.class);

    // Verify
    verify(retrofitErrorResponseHandler).handleResponse(response);
  }

  @Test
  void execute_whenUnsuccessfulResponseNotHandledByErrorResponseHandler_thenThrowsIllegalArgumentException() {
    // Arrange
    ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"),
                                                    "unsuccessful response");
    Response<String> response = Response.error(400, responseBody);
    Call<String> call = new CallImpl(response);

    // Act & Assert
    assertThatThrownBy(() -> retrofitCallHandler.execute(call))
        .isInstanceOf(IllegalArgumentException.class);

    // Verify
    verify(retrofitErrorResponseHandler).handleResponse(response);
  }

  @Test
  void execute_whenCallExecutionFails_thenThrowsHttpException() {
    // Arrange
    Call<String> call = new FaultyCallImpl();

    // Act & Assert
    assertThatThrownBy(() -> retrofitCallHandler.execute(call))
        .isInstanceOf(HttpException.class);

    // Verify
    verifyNoInteractions(retrofitErrorResponseHandler);
  }

  private static class FaultyCallImpl
      extends CallImpl {

    public FaultyCallImpl() {
      super(Response.success(""));
    }

    @Override
    public Response<String> execute() {
      throw new RuntimeException("some runtime error");
    }
  }

  private static class CallImpl
      implements Call<String> {

    private final Response<String> response;

    public CallImpl(Response<String> response) {
      this.response = response;
    }

    @Override
    public Response<String> execute() {
      return response;
    }

    @Override
    public void enqueue(Callback<String> callback) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean isExecuted() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void cancel() {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean isCanceled() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Call<String> clone() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Request request() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Timeout timeout() {
      throw new UnsupportedOperationException();
    }
  }
}
