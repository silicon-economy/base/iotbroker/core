/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.*;
import retrofit2.Response;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test cases for {@link DefaultRetrofitErrorResponseHandler}.
 *
 * @author M. Grzenia
 */
class DefaultRetrofitErrorResponseHandlerTest {

  /**
   * Class under test.
   */
  private DefaultRetrofitErrorResponseHandler retrofitErrorResponseHandler;

  @BeforeEach
  void setUp() {
    retrofitErrorResponseHandler = new DefaultRetrofitErrorResponseHandler();
  }

  @Test
  void handleErrorResponse_whenUnhandledStatusCode_thenThrowsIllegalArgument() {
    // Arrange
    Response<String> response = Response.success("response");

    // Act & Assert
    assertThatThrownBy(() -> retrofitErrorResponseHandler.handleResponse(response))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void handleErrorResponse_whenErrorResponseCode4XX_thenThrowsClientErrorException() {
    // Arrange
    ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"), "response");
    Response<String> response = Response.error(401, responseBody);

    // Act & Assert
    assertThatThrownBy(() -> retrofitErrorResponseHandler.handleResponse(response))
        .isInstanceOf(ClientErrorException.class);
  }

  @Test
  void handleErrorResponse_whenErrorResponseCode400_thenThrowsBadRequestException() {
    // Arrange
    ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"), "response");
    Response<String> response = Response.error(400, responseBody);

    // Act & Assert
    assertThatThrownBy(() -> retrofitErrorResponseHandler.handleResponse(response))
        .isInstanceOf(BadRequestException.class);
  }

  @Test
  void handleErrorResponse_whenErrorResponseCode404_thenThrowsNotFoundException() {
    // Arrange
    ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"), "response");
    Response<String> response = Response.error(404, responseBody);

    // Act & Assert
    assertThatThrownBy(() -> retrofitErrorResponseHandler.handleResponse(response))
        .isInstanceOf(NotFoundException.class);
  }

  @Test
  void handleErrorResponse_whenErrorResponseCode409_thenThrowsConflictException() {
    // Arrange
    ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"), "response");
    Response<String> response = Response.error(409, responseBody);

    // Act & Assert
    assertThatThrownBy(() -> retrofitErrorResponseHandler.handleResponse(response))
        .isInstanceOf(ConflictException.class);
  }

  @Test
  void handleErrorResponse_whenErrorResponseCode5XX_thenThrowsServerErrorException() {
    // Arrange
    ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"), "response");
    Response<String> response = Response.error(500, responseBody);

    // Act & Assert
    assertThatThrownBy(() -> retrofitErrorResponseHandler.handleResponse(response))
        .isInstanceOf(ServerErrorException.class);
  }
}
