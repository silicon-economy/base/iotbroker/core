/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit.RetrofitCallHandler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link DeviceRegistryServiceClientBuilder}.
 *
 * @author M. Grzenia
 */
class DeviceRegistryServiceClientBuilderTest {

  @Test
  void build_createsAClientInstanceReadyToUse() {
    // Arrange
    RetrofitCallHandler retrofitCallHandler = mock(RetrofitCallHandler.class);
    when(retrofitCallHandler.execute(any())).thenReturn(4711L);

    // Act
    DeviceRegistryServiceClient client = new DeviceRegistryServiceClientBuilder()
        .baseUrl("http://localhost")
        .retrofitCallHandler(retrofitCallHandler)
        .build();
    long result = client.fetchDeviceTypesCount();

    // Assert
    assertThat(result).isEqualTo(4711);
  }
}
