/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.time.Instant;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * Test cases for {@link ObjectMapperFactory}.
 *
 * @author M. Grzenia
 */
class ObjectMapperFactoryTest {

  @Test
  void createObjectMapper_createsObjectMapperCapableOfSerializingAndDeserializingDeviceInstanceAndDeviceTypeInstances() {
    // Arrange
    ObjectMapper objectMapper = ObjectMapperFactory.createObjectMapper();
    DeviceInstance deviceInstance = new DeviceInstance("device-instance-1",
                                                       "source-1",
                                                       "tenant-1",
                                                       "identifier-1",
                                                       Instant.EPOCH,
                                                       Instant.EPOCH,
                                                       true,
                                                       "device-instance-description-1",
                                                       "hardware-revision-1",
                                                       "firmware-version-1");
    DeviceType deviceType = new DeviceType("device-type-1",
                                           "source-1",
                                           "identifier-1",
                                           Set.of("adapter-1", "adapter-2"),
                                           "device-type-description-1",
                                           true,
                                           false,
                                           true);

    // Act & Assert
    String serializedDeviceInstance
        = assertDoesNotThrow(() -> objectMapper.writeValueAsString(deviceInstance));
    DeviceInstance deserializedDeviceInstance
        = assertDoesNotThrow(() -> objectMapper.readValue(serializedDeviceInstance,
                                                          DeviceInstance.class));
    assertThat(deserializedDeviceInstance).isEqualTo(deviceInstance);

    String serializedDeviceType
        = assertDoesNotThrow(() -> objectMapper.writeValueAsString(deviceType));
    DeviceType deserializedDeviceType
        = assertDoesNotThrow(() -> objectMapper.readValue(serializedDeviceType, DeviceType.class));
    assertThat(deserializedDeviceType).isEqualTo(deviceType);
  }
}
