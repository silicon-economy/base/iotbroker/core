/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import okhttp3.Request;
import okio.Timeout;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.Sort;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Test cases for {@link RetrofitDeviceRegistryServiceClient}.
 *
 * @author M. Grzenia
 */
class RetrofitDeviceRegistryServiceClientTest {

  /**
   * Class under test.
   */
  private RetrofitDeviceRegistryServiceClient client;
  /**
   * Test dependencies.
   */
  private DeviceRegistryServiceClientRetrofitService retrofitService;
  private RetrofitCallHandler retrofitCallHandler;

  @BeforeEach
  void setUp() {
    retrofitService = mock(DeviceRegistryServiceClientRetrofitService.class);
    retrofitCallHandler = mock(RetrofitCallHandler.class);
    client = new RetrofitDeviceRegistryServiceClient(retrofitService, retrofitCallHandler);
  }

  @Test
  void fetchDeviceInstances_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<List<DeviceInstance>> call = new DummyCall<>(List.of());
    when(retrofitService.fetchDeviceInstances("source", "tenant", 7, 13, Sort.ASC, Sort.DESC))
        .thenReturn(call);

    // Act
    client.fetchDeviceInstances("source", "tenant", 7, 13, Sort.ASC, Sort.DESC);

    // Verify
    verify(retrofitService).fetchDeviceInstances("source", "tenant", 7, 13, Sort.ASC, Sort.DESC);
    verify(retrofitCallHandler).execute(call);
  }


  @Test
  void createDeviceInstance_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<DeviceInstance> call = new DummyCall<>(DeviceInstance.class);
    DeviceInstanceCreateInputTO input = new DeviceInstanceCreateInputTO();
    when(retrofitService.createDeviceInstance(input)).thenReturn(call);

    // Act
    client.createDeviceInstance(input);

    // Verify
    verify(retrofitService).createDeviceInstance(input);
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void fetchDeviceInstancesCount_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<Long> call = new DummyCall<>(Long.class);
    when(retrofitService.fetchDeviceInstancesCount()).thenReturn(call);
    when(retrofitCallHandler.execute(any())).thenReturn(0L);

    // Act
    client.fetchDeviceInstancesCount();

    // Verify
    verify(retrofitService).fetchDeviceInstancesCount();
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void fetchDeviceInstancesSources_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<List<String>> call = new DummyCall<>(List.of());
    when(retrofitService.fetchDeviceInstancesSources()).thenReturn(call);

    // Act
    client.fetchDeviceInstancesSources();

    // Verify
    verify(retrofitService).fetchDeviceInstancesSources();
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void fetchDeviceInstance_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<DeviceInstance> call = new DummyCall<>(DeviceInstance.class);
    when(retrofitService.fetchDeviceInstance("id")).thenReturn(call);

    // Act
    client.fetchDeviceInstance("id");

    // Verify
    verify(retrofitService).fetchDeviceInstance("id");
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void updateDeviceInstance_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<DeviceInstance> call = new DummyCall<>(DeviceInstance.class);
    DeviceInstanceUpdateInputTO input = new DeviceInstanceUpdateInputTO();
    when(retrofitService.updateDeviceInstance("id", input)).thenReturn(call);

    // Act
    client.updateDeviceInstance("id", input);

    // Verify
    verify(retrofitService).updateDeviceInstance("id", input);
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void deleteDeviceInstance_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<Void> call = new DummyCall<>(Void.class);
    when(retrofitService.deleteDeviceInstance("id")).thenReturn(call);

    // Act
    client.deleteDeviceInstance("id");

    // Verify
    verify(retrofitService).deleteDeviceInstance("id");
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void fetchDeviceTypes_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<List<DeviceType>> call = new DummyCall<>(List.of());
    when(retrofitService.fetchDeviceTypes("source", "identifier", 7, 13, Sort.ASC, Sort.DESC))
        .thenReturn(call);

    // Act
    client.fetchDeviceTypes("source", "identifier", 7, 13, Sort.ASC, Sort.DESC);

    // Verify
    verify(retrofitService).fetchDeviceTypes("source", "identifier", 7, 13, Sort.ASC, Sort.DESC);
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void createDeviceType_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<DeviceType> call = new DummyCall<>(DeviceType.class);
    DeviceTypeCreateInputTO input = new DeviceTypeCreateInputTO();
    when(retrofitService.createDeviceType(input)).thenReturn(call);

    // Act
    client.createDeviceType(input);

    // Verify
    verify(retrofitService).createDeviceType(input);
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void fetchDeviceTypesCount_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<Long> call = new DummyCall<>(Long.class);
    when(retrofitService.fetchDeviceTypesCount()).thenReturn(call);
    when(retrofitCallHandler.execute(any())).thenReturn(0L);

    // Act
    client.fetchDeviceTypesCount();

    // Verify
    verify(retrofitService).fetchDeviceTypesCount();
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void fetchDeviceTypesSources_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<List<String>> call = new DummyCall<>(List.of());
    when(retrofitService.fetchDeviceTypesSources()).thenReturn(call);

    // Act
    client.fetchDeviceTypesSources();

    // Verify
    verify(retrofitService).fetchDeviceTypesSources();
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void fetchDeviceType_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<DeviceType> call = new DummyCall<>(DeviceType.class);
    when(retrofitService.fetchDeviceType("id")).thenReturn(call);

    // Act
    client.fetchDeviceType("id");

    // Verify
    verify(retrofitService).fetchDeviceType("id");
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void updateDeviceType_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<DeviceType> call = new DummyCall<>(DeviceType.class);
    DeviceTypeUpdateInputTO input = new DeviceTypeUpdateInputTO();
    when(retrofitService.updateDeviceType("id", input)).thenReturn(call);

    // Act
    client.updateDeviceType("id", input);

    // Verify
    verify(retrofitService).updateDeviceType("id", input);
    verify(retrofitCallHandler).execute(call);
  }

  @Test
  void deleteDeviceType_delegatesToRetrofitServiceAndCallHandler() {
    // Arrange
    Call<Void> call = new DummyCall<>(Void.class);
    when(retrofitService.deleteDeviceType("id")).thenReturn(call);

    // Act
    client.deleteDeviceType("id");

    // Verify
    verify(retrofitService).deleteDeviceType("id");
    verify(retrofitCallHandler).execute(call);
  }

  /**
   * Implementation of {@link Call} merely used to create dummy instances for specific types.
   *
   * @param <T> The type of the {@link Call}.
   */
  private static class DummyCall<T>
      implements Call<T> {

    public DummyCall(Class<T> clazz) {
    }

    public DummyCall(T object) {
    }

    @Override
    public Response<T> execute() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void enqueue(Callback<T> callback) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean isExecuted() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void cancel() {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean isCanceled() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Call<T> clone() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Request request() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Timeout timeout() {
      throw new UnsupportedOperationException();
    }
  }
}
