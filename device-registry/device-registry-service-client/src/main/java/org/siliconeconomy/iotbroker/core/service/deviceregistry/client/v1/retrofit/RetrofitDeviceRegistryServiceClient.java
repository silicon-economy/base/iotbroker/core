/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.Sort;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import retrofit2.Call;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of {@link DeviceRegistryServiceClient} that uses the Retrofit HTTP client
 * for performing HTTP requests.
 * <p>
 * This class merely creates Retrofit {@link Call}s using the provided
 * {@link DeviceRegistryServiceClientRetrofitService Retrofit service} and executes them using a
 * {@link RetrofitCallHandler}.
 *
 * @author M. Grzenia
 */
public class RetrofitDeviceRegistryServiceClient
    implements DeviceRegistryServiceClient {

  /**
   * The Retrofit service used for creating Retrofit {@link Call}s to the Device Registry Service's
   * web API.
   */
  private final DeviceRegistryServiceClientRetrofitService retrofitService;
  /**
   * The {@link RetrofitCallHandler} instance to use for handling Retrofit {@link Call}s.
   */
  private final RetrofitCallHandler retrofitCallHandler;

  /**
   * Creates a new instance.
   *
   * @param retrofitService     The Retrofit service used for creating Retrofit {@link Call}s to the
   *                            Device Registry Service's web API.
   * @param retrofitCallHandler The {@link RetrofitCallHandler} instance to use for handling
   *                            Retrofit {@link Call}s.
   */
  public RetrofitDeviceRegistryServiceClient(
      @NonNull DeviceRegistryServiceClientRetrofitService retrofitService,
      @NonNull RetrofitCallHandler retrofitCallHandler) {
    this.retrofitService = requireNonNull(retrofitService, "retrofitService");
    this.retrofitCallHandler = requireNonNull(retrofitCallHandler, "retrofitCallHandler");
  }

  @Override
  public @NonNull List<DeviceInstance> fetchDeviceInstances(@Nullable String source,
                                                            @Nullable String tenant,
                                                            @Nullable Integer pageIndex,
                                                            @Nullable Integer pageSize,
                                                            @Nullable Sort sortBySource,
                                                            @Nullable Sort sortByTenant) {
    Call<List<DeviceInstance>> call = retrofitService.fetchDeviceInstances(source,
                                                                           tenant,
                                                                           pageIndex,
                                                                           pageSize,
                                                                           sortBySource,
                                                                           sortByTenant);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull DeviceInstance createDeviceInstance(@NonNull DeviceInstanceCreateInputTO input) {
    Call<DeviceInstance> call = retrofitService.createDeviceInstance(input);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public long fetchDeviceInstancesCount() {
    Call<Long> call = retrofitService.fetchDeviceInstancesCount();
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull List<String> fetchDeviceInstancesSources() {
    Call<List<String>> call = retrofitService.fetchDeviceInstancesSources();
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull DeviceInstance fetchDeviceInstance(@NonNull String id) {
    Call<DeviceInstance> call = retrofitService.fetchDeviceInstance(id);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull DeviceInstance updateDeviceInstance(@NonNull String id,
                                                      @NonNull DeviceInstanceUpdateInputTO input) {
    Call<DeviceInstance> call = retrofitService.updateDeviceInstance(id, input);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public void deleteDeviceInstance(@NonNull String id) {
    Call<Void> call = retrofitService.deleteDeviceInstance(id);
    retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull List<DeviceType> fetchDeviceTypes(@Nullable String source,
                                                    @Nullable String identifier,
                                                    @Nullable Integer pageIndex,
                                                    @Nullable Integer pageSize,
                                                    @Nullable Sort sortBySource,
                                                    @Nullable Sort sortByIdentifier) {
    Call<List<DeviceType>> call = retrofitService.fetchDeviceTypes(source,
                                                                   identifier,
                                                                   pageIndex,
                                                                   pageSize,
                                                                   sortBySource,
                                                                   sortByIdentifier);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull DeviceType createDeviceType(@NonNull DeviceTypeCreateInputTO input) {
    Call<DeviceType> call = retrofitService.createDeviceType(input);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public long fetchDeviceTypesCount() {
    Call<Long> call = retrofitService.fetchDeviceTypesCount();
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull List<String> fetchDeviceTypesSources() {
    Call<List<String>> call = retrofitService.fetchDeviceTypesSources();
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull DeviceType fetchDeviceType(@NonNull String id) {
    Call<DeviceType> call = retrofitService.fetchDeviceType(id);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public @NonNull DeviceType updateDeviceType(@NonNull String id,
                                              @NonNull DeviceTypeUpdateInputTO input) {
    Call<DeviceType> call = retrofitService.updateDeviceType(id, input);
    return retrofitCallHandler.execute(call);
  }

  @Override
  public void deleteDeviceType(@NonNull String id) {
    Call<Void> call = retrofitService.deleteDeviceType(id);
    retrofitCallHandler.execute(call);
  }
}
