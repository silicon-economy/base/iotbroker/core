/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.jackson;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

import java.time.Instant;

/**
 * A Jackson Mixin for {@link DeviceInstance} instances.
 * <p>
 * Allows {@link DeviceInstance} instances to be deserialized which otherwise would not be possible
 * due to a missing default constructor.
 *
 * @author M. Grzenia
 */
public abstract class DeviceInstanceMixin {

  @JsonCreator
  DeviceInstanceMixin(@JsonProperty("id") String id,
                      @JsonProperty("source") String source,
                      @JsonProperty("tenant") String tenant,
                      @JsonProperty("deviceTypeIdentifier") String deviceTypeIdentifier,
                      @JsonProperty("registrationTime") Instant registrationTime,
                      @JsonProperty("lastSeen") Instant lastSeen,
                      @JsonProperty("enabled") boolean enabled,
                      @JsonProperty("description") String description,
                      @JsonProperty("hardwareRevision") String hardwareRevision,
                      @JsonProperty("firmwareVersion") String firmwareVersion) {
    // This constructor won't be called
  }
}
