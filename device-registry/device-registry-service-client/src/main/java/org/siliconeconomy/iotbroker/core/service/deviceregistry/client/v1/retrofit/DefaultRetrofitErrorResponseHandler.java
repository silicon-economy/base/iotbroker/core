/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.*;
import retrofit2.Response;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

/**
 * The default implementation of {@link RetrofitErrorResponseHandler}.
 *
 * @author M. Grzenia
 */
public class DefaultRetrofitErrorResponseHandler
    implements RetrofitErrorResponseHandler {

  @Override
  public void handleResponse(Response<?> response)
      throws ClientErrorException, ServerErrorException, IllegalArgumentException {
    if (isClientErrorCode(response.code())) {
      switch (response.code()) {
        case 400:
          throw new BadRequestException(extractErrorBodyAsString(response));
        case 404:
          throw new NotFoundException(extractErrorBodyAsString(response));
        case 409:
          throw new ConflictException(extractErrorBodyAsString(response));
        default:
          throw new ClientErrorException(extractErrorBodyAsString(response));
      }
    } else if (isServerErrorCode(response.code())) {
      throw new ServerErrorException(extractErrorBodyAsString(response));
    } else {
      throw new IllegalArgumentException(String.format("Unhandled (status code=%d) response: %s",
                                                       response.code(),
                                                       response.raw()));
    }
  }

  private boolean isClientErrorCode(int errorCode) {
    return errorCode >= 400 && errorCode <= 499;
  }

  private boolean isServerErrorCode(int errorCode) {
    return errorCode >= 500 && errorCode <= 599;
  }

  private String extractErrorBodyAsString(Response<?> response) {
    try (var responseBody = response.errorBody()) {
      requireNonNull(responseBody, "The provided response doesn't contain an error body.");
      return responseBody.string();
    } catch (IOException e) {
      throw new IllegalArgumentException("Could not retrieve the error body as a string.", e);
    }
  }
}
