/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.HttpException;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

/**
 * The default implementation of {@link RetrofitCallHandler} which uses a
 * {@link RetrofitErrorResponseHandler} to handle error {@link Response}s.
 *
 * @author M. Grzenia
 */
public class DefaultRetrofitCallHandler
    implements RetrofitCallHandler {

  /**
   * The {@link RetrofitErrorResponseHandler} instance to use for handling error {@link Response}s.
   */
  private final RetrofitErrorResponseHandler errorResponseHandler;

  /**
   * Creates a new instance.
   *
   * @param errorResponseHandler The {@link RetrofitErrorResponseHandler} instance to use for
   *                             handling error {@link Response}s.
   */
  public DefaultRetrofitCallHandler(RetrofitErrorResponseHandler errorResponseHandler) {
    this.errorResponseHandler = requireNonNull(errorResponseHandler, "errorResponseHandler");
  }

  @Override
  public <T> T execute(Call<T> call)
      throws HttpException {
    Response<T> response;
    try {
      response = call.execute();
    } catch (RuntimeException | IOException e) {
      throw new HttpException("An error occurred while preparing or executing an HTTP request.", e);
    }

    return deserializedResponseBody(response);
  }

  private <T> T deserializedResponseBody(Response<T> response) {
    if (!response.isSuccessful()) {
      errorResponseHandler.handleResponse(response);
      // This should never happen because the errorResponseHandler is expected to throw an exception
      // in any case.
      throw new IllegalArgumentException(String.format("Unhandled unsuccessful response: %s",
                                                       response.raw()));
    }

    return response.body();
  }
}
