/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.ClientErrorException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.ServerErrorException;
import retrofit2.Response;

/**
 * Provides methods for handling error {@link Response}s.
 *
 * @author M. Grzenia
 */
public interface RetrofitErrorResponseHandler {

  /**
   * Handles error {@link Response}s by throwing appropriate exceptions depending on the
   * {@link Response#code()}.
   *
   * @param response The (error) {@link Response} to handle.
   * @throws ClientErrorException     If the provided response is an error response representing a
   *                                  client-side error. If applicable, a subclass of
   *                                  {@link ClientErrorException} might be thrown.
   * @throws ServerErrorException     If the provided response is an error response representing a
   *                                  server-side error. If applicable, a subclass of
   *                                  {@link ServerErrorException} might be thrown.
   * @throws IllegalArgumentException If the provided response has an unhandled HTTP status code.
   */
  void handleResponse(Response<?> response)
      throws ClientErrorException,
             ServerErrorException,
             IllegalArgumentException;
}
