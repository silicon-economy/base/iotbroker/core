/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1;

/**
 * Defines the possible sort directions for an enumeration of elements.
 *
 * @author M. Grzenia
 */
public enum Sort {

  /**
   * Ascending sort direction.
   */
  ASC,
  /**
   * Descending sort direction.
   */
  DESC;
}
