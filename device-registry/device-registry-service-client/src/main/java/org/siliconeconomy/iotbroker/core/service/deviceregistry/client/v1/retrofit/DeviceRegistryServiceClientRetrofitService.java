/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.Sort;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * Retrofit-specific service interface defining the endpoints (for major version 1) of the Device
 * Registry Service's web API specification.
 * <p>
 * The methods declared here have signatures analogous to their counterparts in
 * {@link DeviceRegistryServiceClient}, with their return types being <em>wrapped</em> in a
 * {@link Call} and, where necessary, additional Retrofit-specific annotations provided.
 * <p>
 * Since the methods are otherwise <em>identical</em> and to avoid redundancy, the semantics of the
 * declared methods are not explicitly documented here again. Instead, refer to the corresponding
 * API documentation in {@link DeviceRegistryServiceClient}.
 *
 * @author M. Grzenia
 */
public interface DeviceRegistryServiceClientRetrofitService {

  @GET("/deviceInstances")
  Call<List<DeviceInstance>> fetchDeviceInstances(
      @Nullable @Query("source") String source,
      @Nullable @Query("tenant") String tenant,
      @Nullable @Query("pageIndex") Integer pageIndex,
      @Nullable @Query("pageSize") Integer pageSize,
      @Nullable @Query("sortBySource") Sort sortBySource,
      @Nullable @Query("sortByTenant") Sort sortByTenant);

  @POST("/deviceInstances")
  Call<DeviceInstance> createDeviceInstance(
      @NonNull @Body DeviceInstanceCreateInputTO deviceInstance);

  @GET("/deviceInstances/count")
  Call<Long> fetchDeviceInstancesCount();

  @GET("/deviceInstances/sources")
  Call<List<String>> fetchDeviceInstancesSources();

  @GET("/deviceInstances/{id}")
  Call<DeviceInstance> fetchDeviceInstance(@NonNull @Path("id") String id);

  @PUT("/deviceInstances/{id}")
  Call<DeviceInstance> updateDeviceInstance(@NonNull @Path("id") String id,
                                            @NonNull @Body DeviceInstanceUpdateInputTO input);

  @DELETE("/deviceInstances/{id}")
  Call<Void> deleteDeviceInstance(@NonNull @Path("id") String id);

  @GET("/deviceTypes")
  Call<List<DeviceType>> fetchDeviceTypes(
      @Nullable @Query("source") String source,
      @Nullable @Query("identifier") String identifier,
      @Nullable @Query("pageIndex") Integer pageIndex,
      @Nullable @Query("pageSize") Integer pageSize,
      @Nullable @Query("sortBySource") Sort sortBySource,
      @Nullable @Query("sortByIdentifier") Sort sortByIdentifier);

  @POST("/deviceTypes")
  Call<DeviceType> createDeviceType(@NonNull @Body DeviceTypeCreateInputTO input);

  @GET("/deviceTypes/count")
  Call<Long> fetchDeviceTypesCount();

  @GET("/deviceTypes/sources")
  Call<List<String>> fetchDeviceTypesSources();

  @GET("/deviceTypes/{id}")
  Call<DeviceType> fetchDeviceType(@NonNull @Path("id") String id);

  @PUT("/deviceTypes/{id}")
  Call<DeviceType> updateDeviceType(@NonNull @Path("id") String id,
                                    @NonNull @Body DeviceTypeUpdateInputTO input);

  @DELETE("/deviceTypes/{id}")
  Call<Void> deleteDeviceType(@NonNull @Path("id") String id);
}
