/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.jackson.ObjectMapperFactory;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit.*;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static java.util.Objects.requireNonNull;

/**
 * A builder for {@link DeviceRegistryServiceClient} instances.
 * <p>
 * Internally, this builder configures and creates {@link RetrofitDeviceRegistryServiceClient}
 * instances. The underlying {@link Retrofit} instance that is used to create
 * {@link DeviceRegistryServiceClientRetrofitService Retrofit service} instances uses Jackson for
 * serialization and deserialization of objects.
 *
 * @author M. Grzenia
 */
public class DeviceRegistryServiceClientBuilder {

  /**
   * The base URL to the Device Registry Service's web API.
   */
  private String baseUrl;
  /**
   * The {@link RetrofitCallHandler} instance to use for handling Retrofit {@link Call}s.
   */
  private RetrofitCallHandler retrofitCallHandler;

  /**
   * Creates a new instance.
   * <p>
   * Uses the following defaults:
   * <ul>
   *   <li><a href="http://localhost">http://localhost</a> as the {@code baseUrl}.</li>
   *   <li>An instance of {@link DefaultRetrofitCallHandler} as the {@code retrofitCallHandler}.
   *   </li>
   * </ul>
   */
  public DeviceRegistryServiceClientBuilder() {
    this("http://localhost",
         new DefaultRetrofitCallHandler(new DefaultRetrofitErrorResponseHandler()));
  }

  /**
   * Creates a new instance.
   *
   * @param baseUrl             The base URL to the Device Registry Service's web API.
   * @param retrofitCallHandler The {@link RetrofitCallHandler} instance to use for handling
   *                            Retrofit {@link Call}s.
   */
  public DeviceRegistryServiceClientBuilder(@NonNull String baseUrl,
                                            @NonNull RetrofitCallHandler retrofitCallHandler) {
    this.baseUrl = requireNonNull(baseUrl, "baseUrl");
    this.retrofitCallHandler = requireNonNull(retrofitCallHandler, "retrofitCallHandler");
  }

  /**
   * Sets the base URL to the Device Registry Service's web API.
   *
   * @param baseUrl The base URL to the Device Registry Service's web API.
   * @return This {@link DeviceRegistryServiceClientBuilder}.
   */
  public DeviceRegistryServiceClientBuilder baseUrl(@NonNull String baseUrl) {
    this.baseUrl = requireNonNull(baseUrl, "baseUrl");
    return this;
  }

  /**
   * Sets the {@link RetrofitCallHandler} instance to use for handling Retrofit {@link Call}s.
   *
   * @param retrofitCallHandler The {@link RetrofitCallHandler} instance to use for handling
   *                            Retrofit {@link Call}s.
   * @return This {@link DeviceRegistryServiceClientBuilder}.
   */
  public DeviceRegistryServiceClientBuilder retrofitCallHandler(
      @NonNull RetrofitCallHandler retrofitCallHandler) {
    this.retrofitCallHandler = requireNonNull(retrofitCallHandler, "retrofitCallHandler");
    return this;
  }

  /**
   * Creates a {@link DeviceRegistryServiceClient} instance using the configured values.
   *
   * @return A {@link DeviceRegistryServiceClient} instance.
   */
  public DeviceRegistryServiceClient build() {
    var objectMapper = ObjectMapperFactory.createObjectMapper();
    var retrofit = new Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(JacksonConverterFactory.create(objectMapper))
        .build();
    var retrofitService
        = retrofit.create(DeviceRegistryServiceClientRetrofitService.class);

    return new RetrofitDeviceRegistryServiceClient(retrofitService, retrofitCallHandler);
  }
}
