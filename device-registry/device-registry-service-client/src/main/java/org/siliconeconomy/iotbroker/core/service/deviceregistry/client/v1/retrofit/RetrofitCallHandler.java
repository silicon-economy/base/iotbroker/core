/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.HttpException;
import retrofit2.Call;

/**
 * Defines methods to handle Retrofit {@link Call}s.
 *
 * @author M. Grzenia
 */
public interface RetrofitCallHandler {

  /**
   * Executes the given {@link Call} by invoking its {@link Call#execute()} method and, in case the
   * request was successful, returns the deserialized response body.
   *
   * @param call The {@link Call} to execute.
   * @param <T>  The type of the (successful) response body.
   * @return The deserialized response body.
   * @throws HttpException If an error occurs while preparing or executing the actual HTTP request
   *                       (e.g. in case the request was not successful). If applicable, a subclass
   *                       of {@link HttpException} might be thrown.
   */
  <T> T execute(Call<T> call)
      throws HttpException;
}
