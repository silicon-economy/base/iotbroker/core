/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.BadRequestException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.ConflictException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.NotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.List;

/**
 * Defines methods for accessing the Device Registry Service's web API.
 * <p>
 * This interface specifically defines methods according to <em>major version 1</em> of the Device
 * Registry Service's web API specification. For more details on this version of the web API, refer
 * to the respective OpenAPI specification document.
 *
 * @author M. Grzenia
 */
public interface DeviceRegistryServiceClient {

  /**
   * Retrieves registered {@link DeviceInstance}s.
   *
   * @param source       The source for which associated {@link DeviceInstance}s are to be
   *                     retrieved. Can be {@code null} to retrieve {@link DeviceInstance}s for all
   *                     sources.
   * @param tenant       The tenant for which {@link DeviceInstance}s are to be retrieved.
   *                     Can be {@code null} to retrieve {@link DeviceInstance}s for all tenants.
   * @param pageIndex    The page index to retrieve {@link DeviceInstance}s for. A zero-based page
   *                     index is used. The page index must not be negative. Can be {@code null} to
   *                     use the default page index.
   * @param pageSize     The size of a single page (i.e. the maximum amount of
   *                     {@link DeviceInstance}s a page shall contain). The page size must be
   *                     greater than 0. Can be {@code null} to use the default page size.
   * @param sortBySource The direction in which the retrieved {@link DeviceInstance}s are to be
   *                     sorted according to their source. Can be {@code null} to use the default
   *                     sort direction.
   * @param sortByTenant The direction in which the retrieved {@link DeviceInstance}s are to be
   *                     sorted according to their tenant value. Can be {@code null} to use the
   *                     default sort direction.
   * @return A list of {@link DeviceInstance}s.
   * @throws BadRequestException If the parameters provided are invalid.
   */
  @NonNull
  List<DeviceInstance> fetchDeviceInstances(@Nullable String source,
                                            @Nullable String tenant,
                                            @Nullable Integer pageIndex,
                                            @Nullable Integer pageSize,
                                            @Nullable Sort sortBySource,
                                            @Nullable Sort sortByTenant);

  /**
   * Creates a new {@link DeviceInstance} (i.e. registers it).
   *
   * @param input The information about the {@link DeviceInstance} to register. Optional fields set
   *              to {@code null} in the given {@link DeviceInstanceCreateInputTO} result in the
   *              respective fields in the {@link DeviceInstance} to be set to default values (e.g.
   *              an empty string).
   * @return The registered {@link DeviceInstance}.
   * @throws NotFoundException If the {@link DeviceType} referenced in the given
   *                           {@link DeviceInstanceCreateInputTO} could not be found.
   * @throws ConflictException If a {@link DeviceInstance} with the {@code source} and
   *                           {@code tenant} referenced in the given
   *                           {@link DeviceInstanceCreateInputTO} already exists.
   */
  @NonNull
  DeviceInstance createDeviceInstance(@NonNull DeviceInstanceCreateInputTO input);

  /**
   * Retrieves the total number of registered {@link DeviceInstance}s.
   *
   * @return The total number of registered {@link DeviceInstance}s.
   */
  long fetchDeviceInstancesCount();

  /**
   * Retrieves a list containing all sources of registered {@link DeviceInstance}s.
   *
   * @return A list containing all sources of registered {@link DeviceInstance}s.
   */
  @NonNull
  List<String> fetchDeviceInstancesSources();

  /**
   * Retrieves the {@link DeviceInstance} with the given ID.
   *
   * @param id The ID of the {@link DeviceInstance}.
   * @return A single {@link DeviceInstance}.
   * @throws NotFoundException If a {@link DeviceInstance} with the given ID could not be found.
   */
  @NonNull
  DeviceInstance fetchDeviceInstance(@NonNull String id);

  /**
   * Updates the {@link DeviceInstance} with the given ID.
   *
   * @param id    The ID of the {@link DeviceInstance}.
   * @param input The information to use for the update. Optional fields set to {@code null} in the
   *              given {@link DeviceInstanceCreateInputTO} result in the respective fields in the
   *              target {@link DeviceInstance} to remain unaffected.
   * @return The updated {@link DeviceInstance}.
   * @throws NotFoundException If a {@link DeviceInstance} with the given ID or the
   *                           {@link DeviceType} referenced in the given
   *                           {@link DeviceInstanceUpdateInputTO} could not be found.
   */
  @NonNull
  DeviceInstance updateDeviceInstance(@NonNull String id,
                                      @NonNull DeviceInstanceUpdateInputTO input);

  /**
   * Deletes the {@link DeviceInstance} with the given ID.
   *
   * @param id The ID of the {@link DeviceInstance}.
   */
  void deleteDeviceInstance(@NonNull String id);

  /**
   * Retrieves registered {@link DeviceType}s.
   *
   * @param source           The source for which associated {@link DeviceType}s are to be
   *                         retrieved. Can be {@code null} to retrieve {@link DeviceType}s for all
   *                         sources.
   * @param identifier       The identifier for which {@link DeviceType}s are to be retrieved.
   *                         Can be {@code null} to retrieve {@link DeviceType}s for all
   *                         identifiers.
   * @param pageIndex        The page index to retrieve {@link DeviceType}s for. A zero-based page
   *                         index is used. The page index must not be negative. Can be {@code null}
   *                         to use the default page index.
   * @param pageSize         The size of a single page (i.e. the maximum amount of
   *                         {@link DeviceType}s a page shall contain). The page size must be
   *                         greater than 0. Can be {@code null} to use the default page size.
   * @param sortBySource     The direction in which the retrieved {@link DeviceType}s are to be
   *                         sorted according to their source. Can be {@code null} to use the
   *                         default sort direction.
   * @param sortByIdentifier The direction in which the retrieved {@link DeviceType}s are to be
   *                         sorted according to their identifier value. Can be {@code null} to use
   *                         the default sort direction.
   * @return A list of {@link DeviceType}s.
   * @throws BadRequestException If the parameters provided are invalid.
   */
  @NonNull
  List<DeviceType> fetchDeviceTypes(@Nullable String source,
                                    @Nullable String identifier,
                                    @Nullable Integer pageIndex,
                                    @Nullable Integer pageSize,
                                    @Nullable Sort sortBySource,
                                    @Nullable Sort sortByIdentifier);

  /**
   * Creates a new {@link DeviceType} (i.e. registers it).
   *
   * @param input The information about the {@link DeviceType} to register. Optional fields set
   *              to {@code null} in the given {@link DeviceTypeCreateInputTO} result in the
   *              respective fields in the {@link DeviceType} to be set to default values (e.g.
   *              an empty string).
   * @return The registered {@link DeviceType}.
   * @throws ConflictException If a {@link DeviceType} with the {@code source} and
   *                           {@code identifier} referenced in the given
   *                           {@link DeviceTypeCreateInputTO} already exists.
   */
  @NonNull
  DeviceType createDeviceType(@NonNull DeviceTypeCreateInputTO input);

  /**
   * Retrieves the total number of registered {@link DeviceType}s.
   *
   * @return The total number of registered {@link DeviceType}s.
   */
  long fetchDeviceTypesCount();

  /**
   * Retrieves a list containing all sources of registered {@link DeviceType}s.
   *
   * @return A list containing all sources of registered {@link DeviceType}s.
   */
  @NonNull
  List<String> fetchDeviceTypesSources();

  /**
   * Retrieves the {@link DeviceType} with the given ID.
   *
   * @param id The ID of the {@link DeviceType}.
   * @return A single {@link DeviceType}.
   * @throws NotFoundException If a {@link DeviceType} with the given ID could not be found.
   */
  @NonNull
  DeviceType fetchDeviceType(@NonNull String id);

  /**
   * Updates the {@link DeviceType} with the given ID.
   *
   * @param id    The ID of the {@link DeviceType}.
   * @param input The information to use for the update. Optional fields set to {@code null} in the
   *              given {@link DeviceTypeCreateInputTO} result in the respective fields in the
   *              target {@link DeviceType} to remain unaffected.
   * @return The updated {@link DeviceType}.
   * @throws NotFoundException If a {@link DeviceType} with the given ID could not be found.
   */
  @NonNull
  DeviceType updateDeviceType(@NonNull String id, @NonNull DeviceTypeUpdateInputTO input);

  /**
   * Deletes the {@link DeviceType} with the given ID.
   *
   * @param id The ID of the {@link DeviceType}.
   * @throws ConflictException If the {@link DeviceType} with the given ID is still being referenced
   *                           by one or more {@link DeviceInstance}s.
   */
  void deleteDeviceType(@NonNull String id);
}
