/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions;

/**
 * Base class for exceptions thrown in the context of an HTTP request.
 *
 * @author M. Grzenia
 */
public class HttpException
    extends RuntimeException {

  public HttpException(String message) {
    super(message);
  }

  public HttpException(String message, Throwable cause) {
    super(message, cause);
  }
}
