/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions;

/**
 * Indicates that an HTTP request resulted in a response with a status code 409.
 *
 * @author M. Grzenia
 */
public class ConflictException
    extends ClientErrorException {

  public ConflictException(String message) {
    super(message);
  }
}
