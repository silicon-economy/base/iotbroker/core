/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions;

/**
 * Base class for exceptions indicating that an HTTP request resulted in a response with a status
 * code 5XX.
 *
 * @author M. Grzenia
 */
public class ServerErrorException
    extends HttpException {

  public ServerErrorException(String message) {
    super(message);
  }
}
