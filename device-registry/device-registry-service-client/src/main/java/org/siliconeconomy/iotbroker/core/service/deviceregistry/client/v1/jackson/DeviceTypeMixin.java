/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.jackson;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Set;

/**
 * A Jackson Mixin for {@link DeviceType} instances.
 * <p>
 * Allows {@link DeviceType} instances to be deserialized which otherwise would not be possible
 * due to a missing default constructor.
 *
 * @author M. Grzenia
 */
public abstract class DeviceTypeMixin {

  @JsonCreator
  DeviceTypeMixin(@JsonProperty("id") String id,
                  @JsonProperty("source") String source,
                  @JsonProperty("identifier") String identifier,
                  @JsonProperty("providedBy") Set<String> providedBy,
                  @JsonProperty("description") String description,
                  @JsonProperty("enabled") boolean enabled,
                  @JsonProperty("autoRegisterDeviceInstances") boolean autoRegisterDeviceInstances,
                  @JsonProperty("autoEnableDeviceInstances") boolean autoEnableDeviceInstances) {
    // This constructor won't be called
  }
}
