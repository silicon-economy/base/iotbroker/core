/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

/**
 * A factory for creating {@link ObjectMapper} instances.
 *
 * @author M. Grzenia
 */
public class ObjectMapperFactory {

  private ObjectMapperFactory() {
  }

  /**
   * Creates an {@link ObjectMapper} instance that is configured to serialize and deserialize
   * {@link DeviceInstance} and {@link DeviceType} objects.
   *
   * @return An {@link ObjectMapper} instance.
   */
  public static ObjectMapper createObjectMapper() {
    return new ObjectMapper()
        .registerModule(new JavaTimeModule())
        .addMixIn(DeviceInstance.class, DeviceInstanceMixin.class)
        .addMixIn(DeviceType.class, DeviceTypeMixin.class);
  }
}
