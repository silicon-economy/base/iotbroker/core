/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

/**
 * This transfer object is used for updating existing {@link DeviceInstance}s.
 * <p>
 * Where possible, documentation of private fields is omitted, since in most cases it is identical
 * to the corresponding documentation in {@link DeviceInstance}.
 *
 * @author M. Grzenia
 */
@Setter
@Getter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DeviceInstanceUpdateInputTO extends DeviceInstanceCommonInputTO {

    private String deviceTypeIdentifier;
}
