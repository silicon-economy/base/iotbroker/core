# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.4] - 2022-10-27
### Added
- A dedicated integration-test module.
- Integration tests that test the interaction between the Device Registry Service and the Device Registry Service Client.

## [1.3.3] - 2022-08-29
### Fixed
- Fix the Docker build by skipping generation of `sources` and `javadoc` artifacts (which are not necessary for docker images).

## [1.3.2] - 2022-08-29
### Changed
- Adjust the project's Maven configuration to include `sources` and `javadoc` artifacts on deployment.

## [1.3.1] - 2022-08-25
### Fixed
- Fix the Maven configuration of the Device Registry Service project to properly execute and create reports for integration tests.

## [1.3.0] - 2022-08-23
### Added
- Add a (Java) client for the Device Registry Service.
  This client can be used by custom IoT Broker components to access the Device Registry Service via its web API.

## [1.2.1] - 2022-07-08
### Fixed
- Fix a bug where the creation of a new device type would fail due to incorrect mapping of the provided input data.

## [1.2.0] - 2022-04-01
### Added
- Add an AMQP integration for the Device Registry Service and the new device registration process.
  When there are updates to device instances or device types (e.g., due to a new registration or a registration update), corresponding messages are now published via AMQP.

### Changed
- Update the arc42 documentation with regard to the changes to the Device Registry Service as well as the new `DeviceInstanceUpdate` and `DeviceTypeUpdate` data structures introduced in the IoT Broker SDK.

## [1.1.1] - 2022-04-01
### Changed
- Update Spring Boot to v2.5.12.
  This Spring Boot version addresses CVE-2022-22965.

## [1.1.0] - 2022-03-03
### Added
- Endpoints for a new, more sophisticated device registration process.
  This process revolves around `org.siliconeconomy.iotbroker.model.device.DeviceInstance`s and `org.siliconeconomy.iotbroker.model.device.DeviceType`s.
  For detailed information, refer to the Device Registry Service's OpenAPI specification and the `/deviceInstances` and `/deviceTypes` endpoints.

### Changed
- Deprecate the old `/devices` endpoints in favor of the new `/deviceInstances` and `/deviceTypes` endpoints.
- Update the arc42 documentation with regard to the changes to the Device Registry Service as well as the new `DeviceInstance` and `DeviceType` data structures introduced in the IoT Broker SDK.

## [1.0.3] - 2022-01-21
### Changed
- Update Spring Boot to v2.5.9.
  This Spring Boot version includes Log4j v2.17.1, which fixes a number of vulnerabilities that have been reported in recent weeks.
  For more information on the reported vulnerabilities, see the following links:
  - CVE-2021-44228, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228
  - CVE-2021-45046, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046
  - CVE-2021-45105, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105
  - CVE-2021-44832, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832

## [1.0.2] - 2021-12-14
### Changed
- Update Log4j dependencies (especially `log4j-core`) to v2.15.0.
  A vulnerability has been reported with CVE-2021-44228 against the `log4j-core` jar and has been fixed in Log4j v2.15.0.
  Until Log4j v2.15.0 is picked up by Spring Boot, which is planned for the Spring Boot v2.5.8 & v2.6.2 releases (due Dec 23, 2021), the Log4j dependencies are overridden manually.
  For more information on CVE-2021-44228, see https://nvd.nist.gov/vuln/detail/CVE-2021-44228.

## [1.0.1] - 2021-11-17
### Added
- This CHANGELOG file to keep track of the changes in this project.

### Changed
- Adjust the style of license headers.
  Use the slash-star style to avoid dangling Javadoc comments.
  Using this style also prevents the license headers from being affected when reformatting code via the IntelliJ IDEA IDE.

## [1.0.0] - 2021-10-19
This is the first public release.

The Device Registry Service manages the registration of IoT devices with the IoT Broker.

This release corresponds to IoT Broker v0.9.
