# OpenAPI 3 specification:
# - https://swagger.io/docs/specification/
# - https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.1.md
# Online validator:
# - https://apidevtools.org/swagger-parser/online/
openapi: 3.0.1
info:
  title: IoT Broker - Device Registry Service API
  description: |-
    This API provides endpoints for most, if not all, operations related to device registration in the context of an IoT Broker.
    The data handled and provided by this API is designed to be both as generic as possible and as specific as necessary.
    Very device type-specific information is therefore outside the scope of this API and may instead be provided by separate, device type-specific APIs.
  contact:
    name: Martin Grzenia
    email: martin.grzenia@iml.fraunhofer.de
  license:
    name: Open Logistics License, Version 1.0
  version: 1.1.0
servers:
  - url: http://localhost:8080
    description: Local server
tags:
  - name: Devices
    description: Endpoints for operations related to device registration.
  - name: Device instances
    description: Endpoints for operations related to device instance registration.
  - name: Device types
    description: Endpoints for operations related to device type registration.
paths:
  /devices:
    get:
      deprecated: true
      tags:
        - Devices
      summary: Retrieves information about all registered devices.
      description: |-
        Use `GET /deviceInstances` instead.

        Pagination is supported by providing the corresponding query parameters.

        Example: A page index of 0 and a page size of 10 will result in the first 10 devices to be returned.

        By default, devices are sorted in lexicographical order first according to their source and then according to their tenant value.
      parameters:
        - name: pageIndex
          in: query
          description: |-
            The page index to retrieve devices for.
            A zero-based page index is used.
            The page index must not be negative.

            Example: A page index of 0 will result in the first or last devices to be returned, depending on the provided sort parameters.
          required: true
          schema:
            type: integer
            format: int32
        - name: pageSize
          in: query
          description: |-
            The size of a single page (i.e. the maximum amount of devices a page shall contain).
            Must be greater than 0.

            Example: A page size of 10 will result in a single page to contain a maximum of 10 devices.
          required: true
          schema:
            type: integer
            format: int32
        - name: sortBySource
          in: query
          description: |-
            The direction in which the retrieved devices are to be sorted according to their source.

            Sort order:
              * `ASC` - Ascending, in lexicographical order.
              * `DESC` - Descending, in reverse lexicographical order.
          required: false
          schema:
            type: string
            enum: [ ASC, DESC ]
            default: ASC
        - name: sortByTenant
          in: query
          description: |-
            The direction in which the retrieved devices are to be sorted according to their tenant value.

            Sort order:
              * `ASC` - Ascending, in lexicographical order.
              * `DESC` - Descending, in reverse lexicographical order.
          required: false
          schema:
            type: string
            enum: [ ASC, DESC ]
            default: ASC
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Device"
        "400":
          description: Invalid parameter value(s).
  /devices/count:
    get:
      deprecated: true
      tags:
        - Devices
      summary: Retrieves the number of registered devices.
      description: Use `GET /deviceInstances/count` instead.
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: integer
                format: int64
  /devices/sources:
    get:
      deprecated: true
      tags:
        - Devices
      summary: Retrieves information about all registered sources of devices.
      description: Use `GET /deviceInstances/sources` instead.
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                example:
                  - lowcosttracker
                  - sensingpuck
  /devices/sources/{SOURCE}/tenants:
    get:
      deprecated: true
      tags:
        - Devices
      summary: Retrieves information about all registered devices that represent a specific source.
      description: |-
        Use `GET /deviceInstances?source={source}` instead.

        Pagination is supported by providing the corresponding query parameters.

        Example: A page index of 0 and a page size of 10 will result in the first 10 devices to be returned.

        By default, devices are sorted in lexicographical order according to their tenant value.
      parameters:
        - name: SOURCE
          in: path
          description: The source that the device represents.
          required: true
          schema:
            type: string
        - name: pageIndex
          in: query
          description: |-
            The page index to retrieve devices for.
            A zero-based page index is used.
            The page index must not be negative.

            Example: A page index of 0 will result in the first or last devices to be returned, depending on the provided sort parameter.
          required: true
          schema:
            type: integer
            format: int32
        - name: pageSize
          in: query
          description: |-
            The size of a single page (i.e. the maximum amount of devices a page shall contain).
            Must be greater than 0.

            Example: A page size of 10 will result in a single page to contain a maximum of 10 devices.
          required: true
          schema:
            type: integer
            format: int32
        - name: sortByTenant
          in: query
          description: |-
            The direction in which the retrieved devices are to be sorted according to their source.

            Sort order:
              * `ASC` - Ascending, in lexicographical order.
              * `DESC` - Descending, in reverse lexicographical order.
          required: false
          schema:
            type: string
            enum: [ ASC, DESC ]
            default: ASC
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Device"
        "400":
          description: Invalid parameter value(s).
  /devices/sources/{SOURCE}/tenants/{TENANT}:
    get:
      deprecated: true
      tags:
        - Devices
      summary: Retrieves information about a single registered device.
      description: Use `GET /deviceInstances?source={source}&tenant={tenant}` instead.
      parameters:
        - name: SOURCE
          in: path
          description: The source that the device represents.
          required: true
          schema:
            type: string
        - name: TENANT
          in: path
          description: The identifier of the device.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Device"
        "404":
          description: Referencing source or tenant that could not be found.
    post:
      deprecated: true
      tags:
        - Devices
      summary: Registers a new device.
      description: Use `POST /deviceInstances` instead.
      parameters:
        - name: SOURCE
          in: path
          description: The source that the device represents.
          required: true
          schema:
            type: string
        - name: TENANT
          in: path
          description: The identifier of the device.
          required: true
          schema:
            type: string
      requestBody:
        description: >-
          Information about the device to register.
          Missing optional fields result in the respective fields for the device to be set to an empty string.
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DeviceInput"
      responses:
        "201":
          description: Successful operation. Returns the registered device.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Device"
        "409":
          description: A device with the same source and tenant is already registered.
    put:
      deprecated: true
      tags:
        - Devices
      summary: Updates information about an already registered device.
      description: Use `PUT /deviceInstances/{id}` instead.
      parameters:
        - name: SOURCE
          in: path
          description: The source that the device represents.
          required: true
          schema:
            type: string
        - name: TENANT
          in: path
          description: The identifier of the device.
          required: true
          schema:
            type: string
      requestBody:
        description: >-
          Information to use for the update.
          Missing optional fields will result to the respective fields for the device to be cleared (overridden with an empty string).
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DeviceInput"
      responses:
        "200":
          description: Successful operation. Returns the updated device.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Device"
        "404":
          description: Referencing source or tenant that could not be found.
    delete:
      deprecated: true
      tags:
        - Devices
      summary: Deletes a device registration.
      description: Use `DELETE /deviceInstances/{id}` instead.
      parameters:
        - name: SOURCE
          in: path
          description: The source that the device represents.
          required: true
          schema:
            type: string
        - name: TENANT
          in: path
          description: The identifier of the device.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Successful operation.
  /deviceInstances:
    get:
      tags:
        - Device instances
      summary: Retrieves information about registered device instances.
      description: |-
        Pagination is supported by providing the corresponding query parameters.

        Example: A page index of 0 and a page size of 10 will result in the first 10 device instances to be returned.

        By default, device instances are sorted in lexicographical order first according to their source and then according to their tenant value.

        The `source` and `tenant` parameters can be used to retrieve information about device instances associated with a specific source (by providing only a source) or a single device instance (by providing both source and tenant).
      parameters:
        - name: source
          in: query
          description: A filter to retrieve only information about device instances associated with a specific source.
        - name: tenant
          in: query
          description: A filter to retrieve only information about device instances with a specific tenant.
        - name: pageIndex
          in: query
          description: |-
            The page index to retrieve device instances for.
            A zero-based page index is used.
            The page index must not be negative.

            Example: A page index of 0 will result in the first or last device instances to be returned, depending on the provided sort parameters.
          schema:
            type: integer
            format: int32
            default: 0
        - name: pageSize
          in: query
          description: |-
            The size of a single page (i.e. the maximum amount of device instances a page shall contain).
            Must be greater than 0.

            Example: A page size of 10 will result in a single page to contain a maximum of 10 device instances.
          schema:
            type: integer
            format: int32
            default: 10
        - name: sortBySource
          in: query
          description: |-
            The direction in which the retrieved device instances are to be sorted according to their source.

            Sort order:
              * `ASC` - Ascending, in lexicographical order.
              * `DESC` - Descending, in reverse lexicographical order.
          required: false
          schema:
            type: string
            enum: [ ASC, DESC ]
            default: ASC
        - name: sortByTenant
          in: query
          description: |-
            The direction in which the retrieved device instances are to be sorted according to their tenant value.

            Sort order:
              * `ASC` - Ascending, in lexicographical order.
              * `DESC` - Descending, in reverse lexicographical order.
          required: false
          schema:
            type: string
            enum: [ ASC, DESC ]
            default: ASC
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/DeviceInstance"
        "400":
          description: Invalid parameter value(s).
    post:
      tags:
        - Device instances
      summary: Registers a new device instance.
      requestBody:
        description: >-
          The information about the device instance to register.
          Missing optional fields result in the respective fields for the device instance to be set to default values (e.g. an empty string).
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DeviceInstanceCreateInput"
      responses:
        "201":
          description: Successful operation. Returns the registered device instance.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/DeviceInstance"
        "404":
          description: Referencing device type that could not be found.
        "409":
          description: A device instance with the same source and tenant is already registered.
  /deviceInstances/count:
    get:
      tags:
        - Device instances
      summary: Retrieves the total number of registered device instances.
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: integer
                format: int64
  /deviceInstances/sources:
    get:
      tags:
        - Device instances
      summary: Retrieves information about all registered sources of device instances.
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                uniqueItems: true
                example:
                  - lowcosttracker
                  - sensingpuck
  /deviceInstances/{id}:
    get:
      tags:
        - Device instances
      summary: Retrieves information about a single registered device instance.
      parameters:
        - name: id
          in: path
          description: The unique identifier of the `DeviceInstance` instance.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/DeviceInstance"
        "404":
          description: Referencing device instance that could not be found.
    put:
      tags:
        - Device instances
      summary: Updates information about an already registered device instance.
      parameters:
        - name: id
          in: path
          description: The unique identifier of the `DeviceInstance` instance.
          required: true
          schema:
            type: string
      requestBody:
        description: >-
          The information to use for the update.
          If optional fields are omitted, the respective fields of the target `DeviceInstance` remain unaffected.
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DeviceInstanceUpdateInput"
      responses:
        "200":
          description: Successful operation. Returns the updated device instance.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/DeviceInstance"
        "404":
          description: Referencing device instance or device type that could not be found.
    delete:
      tags:
        - Device instances
      summary: Deletes a device instance registration.
      parameters:
        - name: id
          in: path
          description: The unique identifier of the `DeviceInstance` instance.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Successful operation.
  /deviceTypes:
    get:
      tags:
        - Device types
      summary: Retrieves information about registered device types.
      description: |-
        Pagination is supported by providing the corresponding query parameters.

        Example: A page index of 0 and a page size of 10 will result in the first 10 device types to be returned.

        By default, devices are sorted in lexicographical order first according to their source and then according to their identifier value.

        The `source` and `identifier` parameters can be used to retrieve information about device instances associated with a specific source (by providing only a source) or a single device type (by providing both source and identifier).
      parameters:
        - name: source
          in: query
          description: A filter to retrieve only information about device types associated with a specific source.
        - name: identifier
          in: query
          description: A filter to retrieve only information about device types with a specific identifier.
        - name: pageIndex
          in: query
          description: |-
            The page index to retrieve device types for.
            A zero-based page index is used.
            The page index must not be negative.

            Example: A page index of 0 will result in the first or last device types to be returned, depending on the provided sort parameters.
          schema:
            type: integer
            format: int32
            default: 0
        - name: pageSize
          in: query
          description: |-
            The size of a single page (i.e. the maximum amount of device types a page shall contain).
            Must be greater than 0.

            Example: A page size of 10 will result in a single page to contain a maximum of 10 device types.
          schema:
            type: integer
            format: int32
            default: 10
        - name: sortBySource
          in: query
          description: |-
            The direction in which the retrieved device types are to be sorted according to their source.

            Sort order:
              * `ASC` - Ascending, in lexicographical order.
              * `DESC` - Descending, in reverse lexicographical order.
          required: false
          schema:
            type: string
            enum: [ ASC, DESC ]
            default: ASC
        - name: sortByIdentifier
          in: query
          description: |-
            The direction in which the retrieved device tyoes are to be sorted according to their identifier value.

            Sort order:
              * `ASC` - Ascending, in lexicographical order.
              * `DESC` - Descending, in reverse lexicographical order.
          required: false
          schema:
            type: string
            enum: [ ASC, DESC ]
            default: ASC
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/DeviceType"
        "400":
          description: Invalid parameter value(s).
    post:
      tags:
        - Device types
      summary: Registers a new device type.
      requestBody:
        description: >-
          The information about the device type to register.
          Missing optional fields result in the respective fields for the device type to be set to default values (e.g. an empty string).
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DeviceTypeCreateInput"
      responses:
        "201":
          description: Successful operation. Returns the registered device type.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/DeviceType"
        "409":
          description: A device type with the same source and identifier is already registered.
  /deviceTypes/count:
    get:
      tags:
        - Device types
      summary: Retrieves the total number of registered device types.
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: integer
                format: int64
  /deviceTypes/sources:
    get:
      tags:
        - Device types
      summary: Retrieves information about all registered sources of device types.
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                uniqueItems: true
                example:
                  - lowcosttracker
                  - sensingpuck
  /deviceTypes/{id}:
    get:
      tags:
        - Device types
      summary: Retrieves information about a single registered device type.
      parameters:
        - name: id
          in: path
          description: The unique identifier of the `DeviceType` instance.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/DeviceType"
        "404":
          description: Referencing device type that could not be found.
    put:
      tags:
        - Device types
      summary: Updates information about an already registered device type.
      parameters:
        - name: id
          in: path
          description: The unique identifier of the `DeviceType` instance.
          required: true
          schema:
            type: string
      requestBody:
        description: >-
          The information to use for the update.
          If optional fields are omitted, the respective fields of the target `DeviceType` remain unaffected.
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DeviceTypeUpdateInput"
      responses:
        "200":
          description: Successful operation. Returns the updated device type.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/DeviceType"
        "404":
          description: Referencing device type that could not be found.
    delete:
      tags:
        - Device types
      summary: Deletes a device type registration.
      parameters:
        - name: id
          in: path
          description: The unique identifier of the `DeviceType` instance.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Successful operation.
        "409":
          description: >-
            Device type cannot be deleted.
            The device type is still referenced by one or more device instances.
components:
  schemas:
    Device:
      type: object
      properties:
        source:
          type: string
          description: >-
            The source that the device represents.
            (E.g. as a source of data/messages.)
          example: lowcosttracker
        tenant:
          type: string
          description: >-
            A unique identifier for the device.
            (E.g. the device ID. Only unique in the context of the device's source.)
          example: Tracker-42
        type:
          type: string
          description: A descriptive text for the device type. (E.g. Button, Tracker, etc.)
          example: Tracker
        model:
          type: string
          description: A descriptive text for the device model. (E.g. the device's code name.)
          example: Low Cost Tracker
        description:
          type: string
          description: A description of the device.
          example: Device for locating and tracking goods.
        hardwareRevision:
          type: string
          description: The device's hardware revision.
          example: v1.0.0
        firmwareVersion:
          type: string
          description: The device's firmware version.
          example: v1.0.0
      required: [ source, tenant, type, model, description, hardwareRevision, firmwareVersion ]
    DeviceInput:
      type: object
      properties:
        type:
          type: string
          description: A descriptive text for the device type. (E.g. Button, Tracker, etc.)
          example: Tracker
        model:
          type: string
          description: A descriptive text for the device model. (E.g. the device's code name.)
          example: Low Cost Tracker
        description:
          type: string
          description: A description of the device.
          example: Device for locating and tracking goods.
        hardwareRevision:
          type: string
          description: The device's hardware revision.
          example: v1.0.0
        firmwareVersion:
          type: string
          description: The device's firmware version.
          example: v1.0.0
    DeviceInstance:
      type: object
      properties:
        id:
          type: string
          description: The unique identifier of the `DeviceInstance` instance.
          example: 652e9dd6-881d-11ec-a8a3-0242ac120002
        source:
          type: string
          description: >-
            The source that the device is associated with.
            (E.g. as a source of data.)
          example: lowcosttracker
        tenant:
          type: string
          description: >-
            The unique identifier for the device.
            (E.g. the device ID. Only unique in the context of the device's source.)
          example: Tracker-42
        deviceTypeIdentifier:
          type: string
          description: The identifier of the `DeviceType` the device is associated with.
          example: lowcosttracker_v1
        registrationTime:
          type: string
          format: date-time
          description: The date and time at which the device was registered (expressed according to ISO 8601).
          example: "2020-11-12T12:56:55Z"
        lastSeen:
          type: string
          format: date-time
          description: >-
            The date and time when the device was last seen (expressed according to ISO 8601).
            If there has been no communication with the device so far, the value is "1970-01-01T00:00:00Z".
          example: "2020-11-12T12:56:55Z"
        enabled:
          type: boolean
          description: >-
            Whether the device is enabled or not.
            For disabled devices, incoming data will not be processed by their associated adapter.
        description:
          type: string
          description: A description for the device.
          example: Device for locating and tracking goods.
        hardwareRevision:
          type: string
          description: The device's hardware revision.
          example: v1.0.0
        firmwareVersion:
          type: string
          description: The device's firmware version.
          example: v1.0.0
      required: [ id, source, tenant, deviceTypeIdentifier, registrationTime, lastSeen, enabled, description, hardwareRevision, firmwareVersion ]
    DeviceInstanceCreateInput:
      allOf:
        - type: object
          properties:
            source:
              type: string
              description: >-
                The source that the device is associated with.
                (E.g. as a source of data.)
              example: lowcosttracker
            tenant:
              type: string
              description: >-
                The unique identifier for the device.
                (E.g. the device ID. Only unique in the context of the device's source.)
              example: Tracker-42
        # Inherit from DeviceInstanceCommonInput
        - $ref: '#/components/schemas/DeviceInstanceCommonInput'
      required: [ id, source, tenant, deviceTypeIdentifier ]
    DeviceInstanceUpdateInput:
      allOf:
        # Inherit from DeviceInstanceCommonInput
        - $ref: '#/components/schemas/DeviceInstanceCommonInput'
    DeviceInstanceCommonInput:
      type: object
      properties:
        deviceTypeIdentifier:
          type: string
          description: The identifier of the `DeviceType` the device is associated with.
          example: lowcosttracker_v1
        lastSeen:
          type: string
          format: date-time
          description: >-
            The date and time when the device was last seen (expressed according to ISO 8601).
            If there has been no communication with the device so far, the value is "1970-01-01T00:00:00Z".
          example: "2020-11-12T12:56:55Z"
        enabled:
          type: boolean
          description: >-
            Whether the device is enabled or not.
            For disabled devices, incoming data will not be processed by their associated adapter.
        description:
          type: string
          description: A description for the device.
          example: Device for locating and tracking goods.
        hardwareRevision:
          type: string
          description: The device's hardware revision.
          example: v1.0.0
        firmwareVersion:
          type: string
          description: The device's firmware version.
          example: v1.0.0
    DeviceType:
      type: object
      properties:
        id:
          type: string
          description: >-
            The unique identifier of the `DeviceType` instance.
            This field is not to be confused with the `identifier` field.
            While the `id` is used to uniquely identify a device type across _all_ device types, regardless of any other device type properties, the identifier can only be used to uniquely identify a device type in the context of the `source` it is associated with.
          example: 652e9dd6-881d-11ec-a8a3-0242ac120002
        source:
          type: string
          description: >-
            The source that the device type is associated with.
            (E.g. as a source of data.)
          example: lowcosttracker
        identifier:
          type: string
          description: >-
            The unique identifier for the device type.
            (Only unique in the context of the device type's source.)
          example: lowcosttracker_v1
        providedBy:
          type: array
          description: A list of adapters (more specifically adapter identifiers) via which devices of this type are provided within the IoT Broker.
          items:
            type: string
          example:
            - lowcosttracker-adapter
        description:
          type: string
          description: A description for the device type.
          example: Devices for locating and tracking goods.
        enabled:
          type: boolean
          description: >-
            Whether the entire device type is enabled or not.
            For devices that are associated with a disabled device type, incoming data will not be processed by the associated adapters.
        autoRegisterDeviceInstances:
          type: boolean
          description: Whether new, as yet unknown `DeviceInstance`s associated with this device type should be automatically registered with the IoT Broker.
        autoEnableDeviceInstances:
          type: boolean
          description: Whether newly registered `DeviceInstance`s associated with this device type should be enabled automatically.
      required: [ id, source, identifier, providedBy, description, enabled, autoRegisterDeviceInstances, autoEnableDeviceInstances ]
    DeviceTypeCreateInput:
      allOf:
        - type: object
          properties:
            source:
              type: string
              description: >-
                The source that the device type is associated with.
                (E.g. as a source of data.)
              example: lowcosttracker
            identifier:
              type: string
              description: >-
                The unique identifier for the device type.
                (Only unique in the context of the device type's source.)
              example: lowcosttracker_v1
        # Inherit from DeviceTypeCommonInput
        - $ref: '#/components/schemas/DeviceTypeCommonInput'
      required: [ id, source, identifier ]
    DeviceTypeUpdateInput:
      allOf:
        # Inherit from DeviceTypeCommonInput
        - $ref: '#/components/schemas/DeviceTypeCommonInput'
    DeviceTypeCommonInput:
      type: object
      properties:
        providedBy:
          type: array
          description: A list of adapters (more specifically adapter identifiers) via which devices of this type are provided within the IoT Broker.
          items:
            type: string
          uniqueItems: true
          example:
            - lowcosttracker-adapter
        description:
          type: string
          description: A description for the device type.
          example: Devices for locating and tracking goods.
        enabled:
          type: boolean
          description: >-
            Whether the entire device type is enabled or not.
            For devices that are associated with a disabled device type, incoming data will not be processed by the associated adapters.
        autoRegisterDeviceInstances:
          type: boolean
          description: Whether new, as yet unknown `DeviceInstance`s associated with this device type should be automatically registered with the IoT Broker.
        autoEnableDeviceInstances:
          type: boolean
          description: Whether newly registered `DeviceInstance`s associated with this device type should be enabled automatically.
