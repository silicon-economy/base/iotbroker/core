/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp.DeviceRegistryEventPublisher;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.config.ModelMapperConfig;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.*;

/**
 * Unit tests for {@link DeviceInstanceService}.
 *
 * @author M. Grzenia
 */
class DeviceInstanceServiceTest {

    /**
     * Class under test.
     */
    private DeviceInstanceService service;
    /**
     * Test dependencies.
     */
    private DeviceInstanceRepository deviceInstanceRepository;
    private DeviceTypeRepository deviceTypeRepository;
    private DeviceRegistryEventPublisher deviceRegistryEventPublisher;

    @BeforeEach
    void setUp() {
        deviceInstanceRepository = mock(DeviceInstanceRepository.class);
        deviceTypeRepository = mock(DeviceTypeRepository.class);
        deviceRegistryEventPublisher = mock(DeviceRegistryEventPublisher.class);
        ModelMapper modelMapper = new ModelMapperConfig().modelMapper();
        service = new DeviceInstanceService(
            deviceInstanceRepository,
            deviceTypeRepository,
            deviceRegistryEventPublisher,
            modelMapper
        );
    }

    @Test
    void create_whenNewDeviceInstance_thenCallsSaveOnRepositoryAndPublishesUpdateEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstanceCreateInputTO input
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstanceEntity createdEntity
            = deviceInstanceEntityWithDefaults(uuid, "source1", "tenant1", "deviceTypeIdentifier1");
        when(deviceInstanceRepository.existsBySourceAndTenant(anyString(), anyString()))
            .thenReturn(false);
        when(deviceTypeRepository.existsBySourceAndIdentifier(anyString(), anyString()))
            .thenReturn(true);
        when(deviceInstanceRepository.save(any(DeviceInstanceEntity.class)))
            .thenReturn(createdEntity);

        // Act
        service.create(input);

        // Verify
        verify(deviceInstanceRepository).save(any());
        verify(deviceRegistryEventPublisher).publishDeviceInstanceUpdate(isNotNull(), isNull());
    }

    @Test
    void create_whenDeviceInstanceAlreadyExists_thenThrowsException() {
        // Arrange
        DeviceInstanceCreateInputTO input
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        when(deviceInstanceRepository.existsBySourceAndTenant(anyString(), anyString()))
            .thenReturn(true);

        // Act & Assert
        assertThatThrownBy(() -> service.create(input))
            .isInstanceOf(DeviceInstanceAlreadyExistsException.class);

        // Verify
        verify(deviceInstanceRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceInstanceUpdate(any(), any());
    }

    @Test
    void create_whenUnknownDeviceType_thenThrowsException() {
        // Arrange
        DeviceInstanceCreateInputTO input
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        when(deviceInstanceRepository.existsBySourceAndTenant(anyString(), anyString()))
            .thenReturn(false);
        when(deviceTypeRepository.existsBySourceAndIdentifier(anyString(), anyString()))
            .thenReturn(false);

        // Act & Assert
        assertThatThrownBy(() -> service.create(input))
            .isInstanceOf(DeviceTypeNotFoundException.class);

        // Verify
        verify(deviceInstanceRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceInstanceUpdate(any(), any());
    }

    @Test
    void findById_whenValidId_thenCallsFindByIdOnRepository() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstanceEntity existingEntity
            = deviceInstanceEntityWithDefaults(uuid, "source1", "tenant1", "deviceTypeIdentifier1");
        when(deviceInstanceRepository.findById(any())).thenReturn(Optional.of(existingEntity));

        // Act
        service.findById(uuid.toString());

        // Verify
        verify(deviceInstanceRepository).findById(uuid);
    }

    @Test
    void findById_whenInvalidId_thenThrowsException() {
        // Act & Assert
        assertThatThrownBy(() -> service.findById("not-a-uuid-string"))
            .isInstanceOf(DeviceInstanceNotFoundException.class);

        // Verify
        verify(deviceInstanceRepository, never()).findById(any());
    }

    @Test
    void findById_whenUnknownDeviceInstance_thenThrowsException() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString();
        when(deviceInstanceRepository.findById(any())).thenReturn(Optional.empty());

        // Act
        assertThatThrownBy(() -> service.findById(uuidString))
            .isInstanceOf(DeviceInstanceNotFoundException.class);

        // Verify
        verify(deviceInstanceRepository).findById(uuid);
    }

    @Test
    void update_whenValidIdAndInput_thenCallsSaveOnRepositoryAndPublishesUpdateEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstanceEntity existingEntity
            = deviceInstanceEntityWithDefaults(uuid, "source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstanceUpdateInputTO input = updateInputWithDefaults("deviceTypeIdentifier2");
        DeviceInstanceEntity updatedEntity
            = deviceInstanceEntityWithDefaults(uuid, "source1", "tenant1", "deviceTypeIdentifier2");
        when(deviceInstanceRepository.findById(uuid)).thenReturn(Optional.of(existingEntity));
        when(deviceTypeRepository.existsBySourceAndIdentifier("source1", "deviceTypeIdentifier2"))
            .thenReturn(true);
        when(deviceInstanceRepository.save(any(DeviceInstanceEntity.class)))
            .thenReturn(updatedEntity);

        // Act
        service.update(uuid.toString(), input);

        // Verify
        verify(deviceInstanceRepository).save(any());
        verify(deviceRegistryEventPublisher).publishDeviceInstanceUpdate(isNotNull(), isNotNull());
    }

    @Test
    void update_whenInvalidId_thenThrowsException() {
        // Arrange
        DeviceInstanceUpdateInputTO input = updateInputWithDefaults("deviceTypeIdentifier2");

        // Act & Assert
        assertThatThrownBy(() -> service.update("not-a-uuid-string", input))
            .isInstanceOf(DeviceInstanceNotFoundException.class);

        // Verify
        verify(deviceInstanceRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceInstanceUpdate(any(), any());
    }

    @Test
    void update_whenUnknownDeviceInstance_thenThrowsException() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString();
        DeviceInstanceUpdateInputTO input = updateInputWithDefaults("deviceTypeIdentifier2");
        when(deviceInstanceRepository.findById(uuid)).thenReturn(Optional.empty());

        // Act & Assert
        assertThatThrownBy(() -> service.update(uuidString, input))
            .isInstanceOf(DeviceInstanceNotFoundException.class);

        // Verify
        verify(deviceInstanceRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceInstanceUpdate(any(), any());
    }

    @Test
    void update_whenUnknownDeviceTypeReferencedInUpdateInput_thenThrowsException() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString();
        DeviceInstanceEntity existingEntity
            = deviceInstanceEntityWithDefaults(uuid, "source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstanceUpdateInputTO input = updateInputWithDefaults("deviceTypeIdentifier2");
        when(deviceInstanceRepository.findById(uuid)).thenReturn(Optional.of(existingEntity));
        when(deviceTypeRepository.existsBySourceAndIdentifier("source1", "deviceTypeIdentifier2"))
            .thenReturn(false);

        // Act & Assert
        assertThatThrownBy(() -> service.update(uuidString, input))
            .isInstanceOf(DeviceTypeNotFoundException.class);

        // Verify
        verify(deviceInstanceRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceInstanceUpdate(any(), any());
    }

    @Test
    void deleteById_whenKnownDeviceInstance_thenCallsDeleteByIdOnRepositoryAndPublishesUpdateEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstanceEntity existingEntity
            = deviceInstanceEntityWithDefaults(uuid, "source1", "tenant1", "deviceTypeIdentifier1");
        when(deviceInstanceRepository.findById(uuid)).thenReturn(Optional.of(existingEntity));

        // Act
        service.deleteById(uuid.toString());

        // Verify
        verify(deviceInstanceRepository).deleteById(uuid);
        verify(deviceRegistryEventPublisher).publishDeviceInstanceUpdate(isNull(), isNotNull());
    }

    @Test
    void deleteById_whenInvalidId_thenDoesNothing() {
        // Act
        service.deleteById("not-a-uuid-string");

        // Verify
        verify(deviceInstanceRepository, never()).deleteById(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceInstanceUpdate(any(), any());
    }

    @Test
    void deleteById_whenUnknownDeviceInstance_thenDoesNothing() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        when(deviceInstanceRepository.findById(uuid)).thenReturn(Optional.empty());

        // Act
        service.deleteById(uuid.toString());

        // Verify
        verify(deviceInstanceRepository, never()).deleteById(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceInstanceUpdate(any(), any());
    }

    @Test
    void findAll_whenNoFilterProvided_thenCallsFindAllOnRepository() {
        // Arrange
        Pageable pageable = Pageable.unpaged();
        when(deviceInstanceRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());

        // Act
        service.findAll(null, null, pageable);

        // Verify
        verify(deviceInstanceRepository).findAll(pageable);
    }

    @Test
    void findAll_whenSourceFilterProvided_thenCallsFindAllBySourceOnRepository() {
        // Arrange
        Pageable pageable = Pageable.unpaged();
        when(deviceInstanceRepository.findAllBySource(anyString(), any(Pageable.class)))
            .thenReturn(Page.empty());

        // Act
        service.findAll("source1", null, pageable);

        // Verify
        verify(deviceInstanceRepository).findAllBySource("source1", pageable);
    }

    @Test
    void findAll_whenTenantFilterProvided_thenCallsFindAllByTenantOnRepository() {
        // Arrange
        Pageable pageable = Pageable.unpaged();
        when(deviceInstanceRepository.findAllByTenant(anyString(), any(Pageable.class)))
            .thenReturn(Page.empty());

        // Act
        service.findAll(null, "tenant1", pageable);

        // Verify
        verify(deviceInstanceRepository).findAllByTenant("tenant1", pageable);
    }

    @Test
    void findAll_whenSourceAndTenantFilterProvided_thenCallsFindBySourceAndTenantOnRepository() {
        // Arrange
        when(deviceInstanceRepository.findBySourceAndTenant(anyString(), anyString()))
            .thenReturn(Optional.empty());

        // Act
        service.findAll("source1", "tenant1", Pageable.unpaged());

        // Verify
        verify(deviceInstanceRepository).findBySourceAndTenant("source1", "tenant1");
    }

    @Test
    void count_whenCalled_thenCallsCountOnRepository() {
        // Act
        service.count();

        // Verify
        verify(deviceInstanceRepository).count();
    }

    @Test
    void findAllSources_whenCalled_thenCallsFindAllSourcesOnRepository() {
        // Act
        service.findAllSources();

        // Verify
        verify(deviceInstanceRepository).findAllSources();
    }
}
