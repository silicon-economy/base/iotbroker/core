/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;

import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Unit tests for {@link DeviceInstanceUpdateInputConverter}.
 *
 * @author M. Grzenia
 */
class DeviceInstanceUpdateInputConverterTest {

    @Test
    void convert_whenAttributesWithNonNullValues_thenReturnsEntityWithUpdatedValues() {
        // Arrange
        DeviceInstanceEntity entity = new DeviceInstanceEntity(
            UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"),
            "source1",
            "tenant1",
            "deviceTypeIdentifier1",
            Instant.ofEpochMilli(1646122484120L),
            Instant.ofEpochMilli(1646122485000L),
            true,
            "Device instance 1 description",
            "v3.2.7",
            "v6.3.1"
        );
        DeviceInstanceUpdateInputTO update = new DeviceInstanceUpdateInputTO()
            .setDeviceTypeIdentifier("deviceTypeIdentifier42");
        update.setLastSeen(Instant.ofEpochMilli(1646122969129L))
            .setEnabled(false)
            .setDescription("New device instance 1 description")
            .setHardwareRevision("v4.0.0")
            .setFirmwareVersion("v7.0.0");

        // Act
        DeviceInstanceEntity result = DeviceInstanceUpdateInputConverter.convert(entity, update);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo(UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"));
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getTenant()).isEqualTo("tenant1");
            softly.assertThat(result.getDeviceTypeIdentifier()).isEqualTo("deviceTypeIdentifier42");
            softly.assertThat(result.getRegistrationTime()).isNotNull();
            softly.assertThat(result.getRegistrationTime().toEpochMilli()).isEqualTo(1646122484120L);
            softly.assertThat(result.getLastSeen()).isNotNull();
            softly.assertThat(result.getLastSeen().toEpochMilli()).isEqualTo(1646122969129L);
            softly.assertThat(result.isEnabled()).isFalse();
            softly.assertThat(result.getDescription()).isEqualTo("New device instance 1 description");
            softly.assertThat(result.getHardwareRevision()).isEqualTo("v4.0.0");
            softly.assertThat(result.getFirmwareVersion()).isEqualTo("v7.0.0");
        });
    }

    @Test
    void convert_whenAttributesWithNullValues_thenReturnsEntityWithOriginalValues() {
        // Arrange
        DeviceInstanceEntity entity = new DeviceInstanceEntity(
            UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"),
            "source1",
            "tenant1",
            "deviceTypeIdentifier1",
            Instant.ofEpochMilli(1646122484120L),
            Instant.ofEpochMilli(1646122485000L),
            true,
            "Device instance 1 description",
            "v3.2.7",
            "v6.3.1"
        );
        DeviceInstanceUpdateInputTO update = new DeviceInstanceUpdateInputTO()
            .setDeviceTypeIdentifier(null);
        update.setLastSeen(null)
            .setEnabled(null)
            .setDescription(null)
            .setHardwareRevision(null)
            .setFirmwareVersion(null);

        // Act
        DeviceInstanceEntity result = DeviceInstanceUpdateInputConverter.convert(entity, update);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo(UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"));
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getTenant()).isEqualTo("tenant1");
            softly.assertThat(result.getDeviceTypeIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getRegistrationTime()).isNotNull();;
            softly.assertThat(result.getRegistrationTime().toEpochMilli()).isEqualTo(1646122484120L);
            softly.assertThat(result.getLastSeen()).isNotNull();
            softly.assertThat(result.getLastSeen().toEpochMilli()).isEqualTo(1646122485000L);
            softly.assertThat(result.isEnabled()).isTrue();
            softly.assertThat(result.getDescription()).isEqualTo("Device instance 1 description");
            softly.assertThat(result.getHardwareRevision()).isEqualTo("v3.2.7");
            softly.assertThat(result.getFirmwareVersion()).isEqualTo("v6.3.1");
        });
    }
}
