/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;

import java.time.Instant;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Unit tests for {@link DeviceInstanceCreateInputConverter}.
 *
 * @author M. Grzenia
 */
class DeviceInstanceCreateInputConverterTest {

    /**
     * Class under test.
     */
    private DeviceInstanceCreateInputConverter converter;

    @BeforeEach
    void setUp() {
        converter = new DeviceInstanceCreateInputConverter();
    }

    @Test
    void convert_whenAttributesWithNonNullValues_thenReturnsEntityWithAppliedValues() {
        // Arrange
        DeviceInstanceCreateInputTO input = new DeviceInstanceCreateInputTO()
            .setSource("source1")
            .setTenant("tenant1")
            .setDeviceTypeIdentifier("deviceTypeIdentifier1");
        input.setLastSeen(Instant.ofEpochMilli(1646120164120L))
            .setEnabled(true)
            .setDescription("Device instance 1 description")
            .setHardwareRevision("v3.2.7")
            .setFirmwareVersion("v6.3.1");

        // Act
        DeviceInstanceEntity result = converter.convert(input);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isNotNull();
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getTenant()).isEqualTo("tenant1");
            softly.assertThat(result.getDeviceTypeIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getRegistrationTime()).isNotNull();
            softly.assertThat(result.getLastSeen()).isNotNull();
            softly.assertThat(result.getLastSeen().toEpochMilli()).isEqualTo(1646120164120L);
            softly.assertThat(result.isEnabled()).isTrue();
            softly.assertThat(result.getDescription()).isEqualTo("Device instance 1 description");
            softly.assertThat(result.getHardwareRevision()).isEqualTo("v3.2.7");
            softly.assertThat(result.getFirmwareVersion()).isEqualTo("v6.3.1");
        });
    }

    @Test
    void convert_whenAttributesWithNullValues_thenReturnsEntityWithDefaultValues() {
        // Arrange
        DeviceInstanceCreateInputTO input = new DeviceInstanceCreateInputTO()
            .setSource("source1")
            .setTenant("tenant1")
            .setDeviceTypeIdentifier("deviceTypeIdentifier1");
        input.setLastSeen(null)
            .setEnabled(null)
            .setDescription(null)
            .setHardwareRevision(null)
            .setFirmwareVersion(null);

        // Act
        DeviceInstanceEntity result = converter.convert(input);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isNotNull();
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getTenant()).isEqualTo("tenant1");
            softly.assertThat(result.getDeviceTypeIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getRegistrationTime()).isNotNull();
            softly.assertThat(result.getLastSeen()).isNotNull();
            softly.assertThat(result.getLastSeen()).isEqualTo(Instant.EPOCH);
            softly.assertThat(result.isEnabled()).isFalse();
            softly.assertThat(result.getDescription()).isEmpty();
            softly.assertThat(result.getHardwareRevision()).isEmpty();
            softly.assertThat(result.getFirmwareVersion()).isEmpty();
        });
    }
}
