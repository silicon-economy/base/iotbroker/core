/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Unit tests for {@link DeviceTypeEntityConverter}.
 *
 * @author M. Grzenia
 */
class DeviceTypeEntityConverterTest {

    /**
     * Class under test.
     */
    private DeviceTypeEntityConverter converter;

    @BeforeEach
    void setUp() {
        converter = new DeviceTypeEntityConverter();
    }

    @Test
    void convert() {
        // Arrange
        DeviceTypeEntity entity = new DeviceTypeEntity(
            UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"),
            "source1",
            "deviceTypeIdentifier1",
            Set.of("adapter1"),
            "Device type 1 description",
            true,
            true,
            true
        );

        // Act
        DeviceType result = converter.convert(entity);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo("5ad25934-e499-4709-9e8e-72c4d47a0d71");
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getProvidedBy()).containsExactly("adapter1");
            softly.assertThat(result.getDescription()).isEqualTo("Device type 1 description");
            softly.assertThat(result.isEnabled()).isTrue();
            softly.assertThat(result.isAutoRegisterDeviceInstances()).isTrue();
            softly.assertThat(result.isAutoEnableDeviceInstances()).isTrue();
        });
    }
}
