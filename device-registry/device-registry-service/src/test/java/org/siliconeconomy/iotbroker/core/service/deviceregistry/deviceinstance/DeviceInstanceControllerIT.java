/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.CustomResultMatchers.responseBody;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for {@link DeviceInstanceController}.
 *
 * @author M. Grzenia
 */
@WebMvcTest(controllers = DeviceInstanceController.class)
class DeviceInstanceControllerIT {

    /**
     * Test dependencies.
     */
    @MockBean
    private DeviceInstanceService service;

    /**
     * Test environment.
     */
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void fetchDeviceInstances_whenValidInput_thenReturns200AndDeviceInstances() throws Exception {
        // Arrange
        List<DeviceInstance> deviceInstances
            = List.of(deviceInstanceWithDefaults("id1", "source1", "tenant1", "deviceTypeIdentifier1"));
        when(service.findAll(any(), any(), any())).thenReturn(deviceInstances);

        // Act & Assert
        mockMvc.perform(get("/deviceInstances"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(deviceInstances));

        // Verify
        verify(service)
            .findAll(null, null, PageRequest.of(0, 10, Sort.Direction.ASC, "source", "tenant"));
    }

    @Test
    void fetchDeviceInstances_whenInvalidInput_thenReturns400() throws Exception {
        // Act & Assert
        mockMvc.perform(get("/deviceInstances").param("pageIndex", "-1"))
            .andExpect(status().isBadRequest());

        mockMvc.perform(get("/deviceInstances").param("pageSize", "-1"))
            .andExpect(status().isBadRequest());

        mockMvc.perform(get("/deviceInstances").param("sortBySource", "***"))
            .andExpect(status().isBadRequest());

        mockMvc.perform(get("/deviceInstances").param("sortByTenant", "***"))
            .andExpect(status().isBadRequest());
    }

    @Test
    void registerDeviceInstance_whenValidInput_thenReturns201AndCreatedDeviceInstance() throws Exception {
        // Arrange
        DeviceInstanceCreateInputTO input
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstance deviceInstance
            = deviceInstanceWithDefaults("id1", "source1", "tenant1", "deviceTypeIdentifier1");
        when(service.create(any())).thenReturn(deviceInstance);

        // Act & Assert
        mockMvc.perform(
                post("/deviceInstances")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isCreated())
            .andExpect(responseBody().containsObjectAsJson(deviceInstance));

        // Verify
        verify(service).create(input);
    }

    @Test
    void registerDeviceInstance_whenDeviceInstanceAlreadyExists_thenReturns409() throws Exception {
        // Arrange
        DeviceInstanceCreateInputTO input
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        when(service.create(any())).thenThrow(new DeviceInstanceAlreadyExistsException("", ""));

        // Act & Assert
        mockMvc.perform(
                post("/deviceInstances")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isConflict());
    }

    @Test
    void registerDeviceInstance_whenUnknownDeviceType_thenReturns404() throws Exception {
        // Arrange
        DeviceInstanceCreateInputTO input
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        when(service.create(any())).thenThrow(new DeviceTypeNotFoundException(""));

        // Act & Assert
        mockMvc.perform(
                post("/deviceInstances")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isNotFound());
    }

    @Test
    void registerDeviceInstance_whenInvalidInput_thenReturns400AndValidationErrorResponse() throws Exception {
        // Arrange
        DeviceInstanceCreateInputTO input = createInputWithDefaults(null, null, null);

        // Act & Assert
        mockMvc.perform(
                post("/deviceInstances")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isBadRequest())
            .andExpect(responseBody().containsValidationError("source", "must not be null nor empty"))
            .andExpect(responseBody().containsValidationError("tenant", "must not be null nor empty"))
            .andExpect(responseBody().containsValidationError("deviceTypeIdentifier", "must not be null nor empty"));
    }

    @Test
    void fetchDeviceInstancesCount_whenCalled_thenReturns200AndCount() throws Exception {
        // Arrange
        when(service.count()).thenReturn(7L);

        // Act & Assert
        mockMvc.perform(get("/deviceInstances/count"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(7L));

        // Verify
        verify(service).count();
    }

    @Test
    void fetchDeviceInstanceSources_whenCalled_thenReturns200AndSources() throws Exception {
        // Arrange
        when(service.findAllSources()).thenReturn(Set.of("source1", "source2"));

        // Act & Assert
        mockMvc.perform(get("/deviceInstances/sources"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(Set.of("source1", "source2")));

        // Verify
        verify(service).findAllSources();
    }

    @Test
    void fetchDeviceInstance_whenKnownDeviceInstance_thenReturns200AndDeviceInstance() throws Exception {
        // Arrange
        DeviceInstance deviceInstance
            = deviceInstanceWithDefaults("id1", "source1", "tenant1", "deviceTypeIdentifier1");
        when(service.findById(anyString())).thenReturn(deviceInstance);

        // Act & Assert
        mockMvc.perform(get("/deviceInstances/{id}", "id1"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(deviceInstance));

        // Verify
        verify(service).findById("id1");
    }

    @Test
    void fetchDeviceInstance_whenUnknownDeviceInstance_thenReturns404() throws Exception {
        // Arrange
        when(service.findById(anyString())).thenThrow(new DeviceInstanceNotFoundException(""));

        // Act & Assert
        mockMvc.perform(get("/deviceInstances/{id}", "id1"))
            .andExpect(status().isNotFound());

        // Verify
        verify(service).findById("id1");
    }

    @Test
    void updateDeviceInstance_whenValidInput_thenReturns200AndUpdatedDeviceInstance() throws Exception {
        // Arrange
        DeviceInstanceUpdateInputTO input = updateInputWithDefaults("deviceTypeIdentifier1");
        DeviceInstance deviceInstance
            = deviceInstanceWithDefaults("id1", "source1", "tenant1", "deviceTypeIdentifier1");
        when(service.update(anyString(), any())).thenReturn(deviceInstance);

        // Act & Assert
        mockMvc.perform(
                put("/deviceInstances/{id}", "id1")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(deviceInstance));

        // Verify
        verify(service).update("id1", input);
    }

    @Test
    void updateDeviceInstance_whenUnknownDeviceInstance_thenReturns404() throws Exception {
        // Arrange
        DeviceInstanceUpdateInputTO input = updateInputWithDefaults("deviceTypeIdentifier1");
        when(service.update(anyString(), any())).thenThrow(new DeviceInstanceNotFoundException(""));

        // Act & Assert
        mockMvc.perform(
                put("/deviceInstances/{id}", "id1")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isNotFound());

        // Verify
        verify(service).update("id1", input);
    }

    @Test
    void updateDeviceInstance_whenUnknownDeviceType_thenReturns404() throws Exception {
        // Arrange
        DeviceInstanceUpdateInputTO input = updateInputWithDefaults("deviceTypeIdentifier1");
        when(service.update(anyString(), any())).thenThrow(new DeviceTypeNotFoundException(""));

        // Act & Assert
        mockMvc.perform(
                put("/deviceInstances/{id}", "id1")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isNotFound());

        // Verify
        verify(service).update("id1", input);
    }

    @Test
    void deleteDeviceInstance_whenCalled_thenReturns200() throws Exception {
        // Act & Assert
        mockMvc.perform(delete("/deviceInstances/{id}", "id1"))
            .andExpect(status().isOk());

        // Verify
        verify(service).deleteById("id1");
    }
}
