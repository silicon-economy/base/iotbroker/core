/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.config.ModelMapperConfig;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceService;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeService;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceAlreadyExistsException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.DeviceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.model.device.Device;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link DeviceService}.
 *
 * @author M. Grzenia
 */
@SpringBootTest(classes = {DeviceService.class, ModelMapperConfig.class})
class DeviceServiceTest {

    /**
     * Class under test.
     */
    @Autowired
    @InjectMocks
    private DeviceService service;

    /**
     * Test dependencies.
     */
    @MockBean
    private DeviceInstanceRepository deviceInstanceRepository;
    @MockBean
    private DeviceInstanceService deviceInstanceService;
    @MockBean
    private DeviceTypeRepository deviceTypeRepository;
    @MockBean
    private DeviceTypeService deviceTypeService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void findAll() {
        // Arrange
        when(deviceInstanceService.findAll(eq(null), eq(null), any(Pageable.class)))
            .thenReturn(List.of(createDummyDeviceInstance("123")));

        // Act
        List<Device> result = service.findAll(0, 10, Sort.Direction.ASC, Sort.Direction.DESC);

        // Assert & Verify
        verify(deviceInstanceService).findAll(
            null,
            null,
            PageRequest.of(
                0,
                10,
                Sort.by(Sort.Direction.ASC, "source").and(Sort.by(Sort.Direction.DESC, "tenant"))
            )
        );
        assertThat(result)
            .hasSize(1)
            .contains(new Device("somesource", "sometenant")
                .withType("")
                .withModel("")
                .withDescription("Some description")
                .withHardwareRevision("Some hardware revision")
                .withFirmwareVersion("Some firmware version")
            );
    }

    @Test
    void count() {
        // Arrange
        when(deviceInstanceService.count()).thenReturn(13L);

        // Act
        long count = service.count();

        // Assert & Verify
        verify(deviceInstanceService).count();
        assertThat(count).isEqualTo(13);
    }

    @Test
    void findAllSources() {
        // Act
        service.findAllSources();

        // Assert & Verify
        verify(deviceInstanceService).findAllSources();
    }

    @Test
    void findAllBySource() {
        // Arrange
        when(deviceInstanceService.findAll(anyString(), eq(null), any(Pageable.class)))
            .thenReturn(List.of(createDummyDeviceInstance("123")));

        // Act
        List<Device> result = service.findAllBySource("somesource", 0, 7, Sort.Direction.ASC);

        // Assert & Verify
        verify(deviceInstanceService).findAll(
            "somesource",
            null,
            PageRequest.of(
                0,
                7,
                Sort.by(Sort.Direction.ASC, "tenant")
            )
        );
        assertThat(result)
            .hasSize(1)
            .contains(new Device("somesource", "sometenant")
                .withType("")
                .withModel("")
                .withDescription("Some description")
                .withHardwareRevision("Some hardware revision")
                .withFirmwareVersion("Some firmware version")
            );
    }

    @Test
    void findBySourceAndTenant() {
        // Arrange
        when(deviceInstanceService.findAll(anyString(), anyString(), any(Pageable.class)))
            .thenReturn(List.of(createDummyDeviceInstance("123")));

        // Act
        Device result = service.findBySourceAndTenant("somesource", "sometenant");

        // Assert & Verify
        verify(deviceInstanceService).findAll("somesource", "sometenant", Pageable.unpaged());
        assertThat(result)
            .isEqualTo(new Device("somesource", "sometenant")
                .withType("")
                .withModel("")
                .withDescription("Some description")
                .withHardwareRevision("Some hardware revision")
                .withFirmwareVersion("Some firmware version")
            );
    }

    @Test
    void findBySourceAndTenant_deviceNotFound() {
        // Arrange
        when(deviceInstanceService.findAll(anyString(), anyString(), any(Pageable.class)))
            .thenReturn(Collections.emptyList());

        // Act & Assert
        assertThatThrownBy(() -> service.findBySourceAndTenant("somesource", "sometenant"))
            .isInstanceOf(DeviceNotFoundException.class);
    }

    @Test
    void add() {
        // Arrange
        when(deviceInstanceRepository.existsBySourceAndTenant(anyString(), anyString()))
            .thenReturn(false);
        when(deviceInstanceService.create(any(DeviceInstanceCreateInputTO.class)))
            .thenReturn(createDummyDeviceInstance("1"));

        // Act
        Device result = service.add(createDummyDevice());

        // Assert & Verify
        verify(deviceInstanceService).create(any());
        assertThat(result).isEqualTo(createDummyDevice());
    }

    @Test
    void add_deviceAlreadyExists() {
        // Arrange
        when(deviceInstanceRepository.existsBySourceAndTenant(anyString(), anyString())).thenReturn(true);
        Device device = createDummyDevice();

        // Act && Assert
        assertThatThrownBy(() -> service.add(device))
            .isInstanceOf(DeviceAlreadyExistsException.class);
    }

    @Test
    void update() {
        // Arrange
        Device updateDevice = new Device("somesource", "sometenant")
            .withType("")
            .withModel("")
            .withDescription("Some new description")
            .withHardwareRevision("Some new hardware revision")
            .withFirmwareVersion("Some new firmware version");
        DeviceInstanceEntity existingEntity = createDummyDeviceInstanceEntity(UUID.randomUUID());
        DeviceInstance updatedDevice = new DeviceInstance(
            existingEntity.getId().toString(),
            "somesource",
            "sometenant",
            "somesource_Some new firmware version",
            Instant.EPOCH,
            Instant.EPOCH,
            false,
            "Some new description",
            "Some new hardware revision",
            "Some new firmware version"
        );
        when(deviceInstanceRepository.findBySourceAndTenant(anyString(), anyString()))
            .thenReturn(Optional.of(existingEntity));
        when(deviceInstanceService.update(anyString(), any())).thenReturn(updatedDevice);

        // Act
        Device result = service.update(updateDevice);

        // Assert & Verify
        verify(deviceInstanceService).update(anyString(), any());
        assertThat(result).isEqualTo(updateDevice);
    }

    @Test
    void update_deviceNotFound() {
        // Arrange
        when(deviceInstanceRepository.findBySourceAndTenant(anyString(), anyString()))
            .thenReturn(Optional.empty());
        Device device = createDummyDevice();

        // Act && Assert
        assertThatThrownBy(() -> service.update(device))
            .isInstanceOf(DeviceNotFoundException.class);
    }

    @Test
    void deleteBySourceAndTenant() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        when(deviceInstanceRepository.findBySourceAndTenant(anyString(), anyString()))
            .thenReturn(Optional.of(createDummyDeviceInstanceEntity(uuid)));

        // Act
        service.deleteBySourceAndTenant("somesource", "sometenant");

        // Assert & Verify
        verify(deviceInstanceRepository).deleteById(uuid);
    }

    private DeviceEntity createDummyDeviceEntity(Long id) {
        DeviceEntity entity = new DeviceEntity();
        entity.setId(id);
        entity.setSource("somesource");
        entity.setTenant("sometenant");
        entity.setType("Some type");
        entity.setModel("Some model");
        entity.setDescription("Some description");
        entity.setHardwareRevision("Some hardware revision");
        entity.setFirmwareVersion("Some firmware version");
        return entity;
    }

    private Device createDummyDevice() {
        return new Device("somesource", "sometenant")
            .withType("")
            .withModel("")
            .withDescription("Some description")
            .withHardwareRevision("Some hardware revision")
            .withFirmwareVersion("Some firmware version");
    }

    private DeviceInstanceEntity createDummyDeviceInstanceEntity(UUID id) {
        return new DeviceInstanceEntity(
            id,
            "somesource",
            "sometenant",
            "somesource_Some firmware version",
            Instant.EPOCH,
            Instant.EPOCH,
            false,
            "Some description",
            "Some hardware revision",
            "Some firmware version"
        );
    }

    private DeviceInstance createDummyDeviceInstance(String id) {
        return new DeviceInstance(
            id,
            "somesource",
            "sometenant",
            "somedevicetypeidentifier",
            Instant.EPOCH,
            Instant.EPOCH,
            false,
            "Some description",
            "Some hardware revision",
            "Some firmware version"
        );
    }
}
