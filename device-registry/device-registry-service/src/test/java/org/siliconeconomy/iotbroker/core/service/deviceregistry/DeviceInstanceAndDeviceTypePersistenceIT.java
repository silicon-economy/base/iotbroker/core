/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp.DeviceRegistryEventPublisher;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.config.ModelMapperConfig;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceService;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeService;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Pageable;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.createInputWithDefaults;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.updateInputWithDefaults;

/**
 * Integration tests related to the persistence of data involving {@link DeviceInstanceRepository}
 * and {@link DeviceInstanceService}, and {@link DeviceTypeRepository} and
 * {@link DeviceTypeService}.
 *
 * @author M. Grzenia
 */
@DataJpaTest
@Import({DeviceInstanceService.class, DeviceTypeService.class, ModelMapperConfig.class})
class DeviceInstanceAndDeviceTypePersistenceIT {

    /**
     * Classes under test.
     */
    @Autowired
    private DeviceTypeRepository deviceTypeRepository;
    @Autowired
    private DeviceInstanceService deviceInstanceService;
    @Autowired
    private DeviceInstanceRepository deviceInstanceRepository;
    @Autowired
    private DeviceTypeService deviceTypeService;
    /**
     * Test dependencies.
     */
    @MockBean
    private DeviceRegistryEventPublisher deviceRegistryEventPublisher;

    @Test
    void shouldRegisterDeviceInstanceAndCorrespondingDeviceType() {
        // Assert that the databases are empty.
        assertThat(deviceTypeRepository.count()).isZero();
        assertThat(deviceInstanceRepository.count()).isZero();

        // Register a device type.
        DeviceTypeCreateInputTO deviceTypeInput
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        DeviceType registeredDeviceType = deviceTypeService.create(deviceTypeInput);

        // Assert that there now is a device type.
        assertThat(deviceTypeRepository.count()).isOne();
        assertThat(deviceTypeService.findById(registeredDeviceType.getId()))
            .extracting(DeviceType::getSource, DeviceType::getIdentifier)
            .containsExactly("source1", "deviceTypeIdentifier1");

        // Register a device instance.
        DeviceInstanceCreateInputTO deviceInstanceInput
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstance registeredDeviceInstance = deviceInstanceService.create(deviceInstanceInput);

        // Assert that there now is a device instance.
        assertThat(deviceInstanceRepository.count()).isOne();
        assertThat(deviceInstanceService.findById(registeredDeviceInstance.getId()))
            .extracting(
                DeviceInstance::getSource,
                DeviceInstance::getTenant,
                DeviceInstance::getDeviceTypeIdentifier
            )
            .containsExactly("source1", "tenant1", "deviceTypeIdentifier1");
    }

    @Test
    void shouldUpdateDeviceInstanceRegistration() {
        // Assert that the databases are empty.
        assertThat(deviceTypeRepository.count()).isZero();
        assertThat(deviceInstanceRepository.count()).isZero();

        // Register a device type.
        DeviceTypeCreateInputTO deviceTypeInput
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        deviceTypeService.create(deviceTypeInput);

        // Register a device instance.
        DeviceInstanceCreateInputTO deviceInstanceCreateInput
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstance registeredDeviceInstance
            = deviceInstanceService.create(deviceInstanceCreateInput);

        // Update the device instance.
        DeviceInstanceUpdateInputTO deviceInstanceUpdateInput
            = updateInputWithDefaults("deviceTypeIdentifier1");
        deviceInstanceUpdateInput.setDescription("A new instance description.")
            .setEnabled(true)
            .setFirmwareVersion("v2.1.0");
        deviceInstanceService.update(registeredDeviceInstance.getId(), deviceInstanceUpdateInput);

        // Assert that there now is an updated device instance.
        assertThat(deviceInstanceRepository.count()).isOne();
        assertThat(deviceInstanceService.findById(registeredDeviceInstance.getId()))
            .extracting(
                DeviceInstance::getSource,
                DeviceInstance::getTenant,
                DeviceInstance::getDeviceTypeIdentifier,
                DeviceInstance::getDescription,
                DeviceInstance::isEnabled,
                DeviceInstance::getFirmwareVersion
            )
            .containsExactly(
                "source1",
                "tenant1",
                "deviceTypeIdentifier1",
                "A new instance description.",
                true,
                "v2.1.0"
            );
    }

    @Test
    void shouldUpdateDeviceTypeRegistration() {
        // Assert that the database is empty.
        assertThat(deviceTypeRepository.count()).isZero();

        // Register a device type.
        DeviceTypeCreateInputTO deviceTypeCreateInput
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        DeviceType registeredDeviceType = deviceTypeService.create(deviceTypeCreateInput);

        // Update the device type.
        DeviceTypeUpdateInputTO deviceTypeUpdateInput = updateInputWithDefaults();
        deviceTypeUpdateInput.setDescription("A new type description.")
            .setAutoRegisterDeviceInstances(true)
            .setProvidedBy(Set.of("adapter1, adapter2"));
        deviceTypeService.update(registeredDeviceType.getId(), deviceTypeUpdateInput);

        // Assert that there now is an updated device type.
        assertThat(deviceTypeRepository.count()).isOne();
        assertThat(deviceTypeService.findById(registeredDeviceType.getId()))
            .extracting(
                DeviceType::getSource,
                DeviceType::getIdentifier,
                DeviceType::getDescription,
                DeviceType::isAutoRegisterDeviceInstances,
                DeviceType::getProvidedBy
            )
            .containsExactly(
                "source1",
                "deviceTypeIdentifier1",
                "A new type description.",
                true,
                Set.of("adapter1, adapter2")
            );
    }

    @Test
    void shouldDeleteDeviceInstanceAndDeviceTypeRegistration() {
        // Assert that the databases are empty.
        assertThat(deviceTypeRepository.count()).isZero();
        assertThat(deviceInstanceRepository.count()).isZero();

        // Register a device type.
        DeviceTypeCreateInputTO deviceTypeInput
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        DeviceType registeredDeviceType = deviceTypeService.create(deviceTypeInput);

        // Register a device instance.
        DeviceInstanceCreateInputTO deviceInstanceInput
            = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstance registeredDeviceInstance = deviceInstanceService.create(deviceInstanceInput);

        // Assert that there now is a device type and a device instance.
        assertThat(deviceTypeRepository.count()).isOne();
        assertThat(deviceInstanceRepository.count()).isOne();

        // Delete the device instance and the device type.
        deviceInstanceService.deleteById(registeredDeviceInstance.getId());
        deviceTypeService.deleteById(registeredDeviceType.getId());

        // Assert that the databases are empty, again.
        assertThat(deviceTypeRepository.count()).isZero();
        assertThat(deviceInstanceRepository.count()).isZero();
    }

    @Test
    void shouldFindTheRequestedDeviceInstancesAndDeviceTypes() {
        // Assert that the databases are empty.
        assertThat(deviceTypeRepository.count()).isZero();
        assertThat(deviceInstanceRepository.count()).isZero();

        // Register device types and device instances.
        deviceTypeService.create(createInputWithDefaults("source1", "deviceTypeIdentifier1"));
        deviceTypeService.create(createInputWithDefaults("source2", "deviceTypeIdentifier2"));
        deviceTypeService.create(createInputWithDefaults("source3", "deviceTypeIdentifier1"));
        deviceInstanceService.create(createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1"));
        deviceInstanceService.create(createInputWithDefaults("source2", "tenant2", "deviceTypeIdentifier2"));

        // Assert that the correct device types are found.
        assertThat(deviceTypeRepository.count()).isEqualTo(3);
        assertThat(deviceTypeService.findAll("source2", "deviceTypeIdentifier2", Pageable.unpaged()))
            .hasSize(1)
            .extracting("source", "identifier")
            .containsExactlyInAnyOrder(tuple("source2", "deviceTypeIdentifier2"));
        assertThat(deviceTypeService.findAll("source1", null, Pageable.unpaged()))
            .hasSize(1)
            .extracting("source", "identifier")
            .containsExactlyInAnyOrder(tuple("source1", "deviceTypeIdentifier1"));
        assertThat(deviceTypeService.findAll(null, "deviceTypeIdentifier1", Pageable.unpaged()))
            .hasSize(2)
            .extracting("source", "identifier")
            .containsExactlyInAnyOrder(
                tuple("source1", "deviceTypeIdentifier1"),
                tuple("source3", "deviceTypeIdentifier1")
            );
        assertThat(deviceTypeService.findAll(null, null, Pageable.unpaged()))
            .hasSize(3)
            .extracting("source", "identifier")
            .containsExactlyInAnyOrder(
                tuple("source1", "deviceTypeIdentifier1"),
                tuple("source2", "deviceTypeIdentifier2"),
                tuple("source3", "deviceTypeIdentifier1")
            );

        // Assert that the correct device instances are found.
        assertThat(deviceInstanceRepository.count()).isEqualTo(2);
        assertThat(deviceInstanceService.findAll("source2", "tenant2", Pageable.unpaged()))
            .hasSize(1)
            .extracting("source", "tenant")
            .containsExactlyInAnyOrder(tuple("source2", "tenant2"));
        assertThat(deviceInstanceService.findAll("source1", null, Pageable.unpaged()))
            .hasSize(1)
            .extracting("source", "tenant")
            .containsExactlyInAnyOrder(tuple("source1", "tenant1"));
        assertThat(deviceInstanceService.findAll(null, "tenant2", Pageable.unpaged()))
            .hasSize(1)
            .extracting("source", "tenant")
            .containsExactlyInAnyOrder(tuple("source2", "tenant2"));
        assertThat(deviceInstanceService.findAll(null, null, Pageable.unpaged()))
            .hasSize(2)
            .extracting("source", "tenant")
            .containsExactlyInAnyOrder(
                tuple("source1", "tenant1"),
                tuple("source2", "tenant2")
            );
    }

    @Test
    void shouldFindAllDeviceInstanceAndDeviceTypeSources() {
        // Assert that the databases are empty.
        assertThat(deviceTypeRepository.count()).isZero();
        assertThat(deviceInstanceRepository.count()).isZero();

        // Register device types and device instances.
        deviceTypeService.create(createInputWithDefaults("source1", "deviceTypeIdentifier1"));
        deviceTypeService.create(createInputWithDefaults("source2", "deviceTypeIdentifier1"));
        deviceTypeService.create(createInputWithDefaults("source2", "deviceTypeIdentifier2"));
        deviceTypeService.create(createInputWithDefaults("source3", "deviceTypeIdentifier1"));
        deviceInstanceService.create(createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1"));
        deviceInstanceService.create(createInputWithDefaults("source1", "tenant2", "deviceTypeIdentifier1"));
        deviceInstanceService.create(createInputWithDefaults("source2", "tenant2", "deviceTypeIdentifier2"));

        // Assert that all sources are found.
        assertThat(deviceTypeRepository.count()).isEqualTo(4);
        assertThat(deviceTypeService.findAllSources())
            .containsExactlyInAnyOrder("source1", "source2", "source3");
        assertThat(deviceInstanceRepository.count()).isEqualTo(3);
        assertThat(deviceInstanceService.findAllSources())
            .containsExactlyInAnyOrder("source1", "source2");
    }
}
