/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.MessageAssert;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.utils.amqp.DeviceManagementExchangeConfiguration;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Message;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.deviceInstanceWithDefaults;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.deviceTypeWithDefaults;

/**
 * Unit tests for {@link DeviceRegistryEventPublisher}.
 *
 * @author M. Grzenia
 */
class DeviceRegistryEventPublisherTest {

    /**
     * Class under test.
     */
    private DeviceRegistryEventPublisher deviceRegistryEventPublisher;
    /**
     * Test dependencies.
     */
    private AmqpAdmin amqpAdmin;
    private AmqpTemplate amqpTemplate;

    @BeforeEach
    void setUp() {
        amqpAdmin = mock(AmqpAdmin.class);
        amqpTemplate = mock(AmqpTemplate.class);
        deviceRegistryEventPublisher = new DeviceRegistryEventPublisher(amqpAdmin, amqpTemplate);
    }

    @Test
    void initialize_shouldDeclareExchange() {
        // Act
        deviceRegistryEventPublisher.initialize();

        // Assert & Verify
        ArgumentCaptor<Exchange> exchangeCaptor = ArgumentCaptor.forClass(Exchange.class);
        verify(amqpAdmin).declareExchange(exchangeCaptor.capture());
        assertThat(exchangeCaptor.getValue())
            .extracting(Exchange::getName, Exchange::getType, Exchange::isDurable)
            .containsExactly(
                DeviceManagementExchangeConfiguration.NAME,
                DeviceManagementExchangeConfiguration.TYPE,
                DeviceManagementExchangeConfiguration.DURABLE
            );
    }

    @Test
    void publishDeviceInstanceUpdate_whenNewAndOldInstanceNull_thenThrowsException() {
        // Act & Assert
        assertThatThrownBy(() -> deviceRegistryEventPublisher.publishDeviceInstanceUpdate(null, null))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void publishDeviceInstanceUpdate_whenOldInstanceNull_thenPublishesCreatedEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstance newInstance
            = deviceInstanceWithDefaults(uuid.toString(), "source1", "tenant1", "deviceTypeIdentifier1");

        // Act
        deviceRegistryEventPublisher.publishDeviceInstanceUpdate(newInstance, null);

        // Assert & Verify
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(amqpTemplate).send(
            eq(DeviceManagementExchangeConfiguration.NAME),
            eq(DeviceManagementExchangeConfiguration.formatDeviceInstanceUpdateRoutingKey("deviceTypeIdentifier1")),
            messageCaptor.capture()
        );
        verifyNoMoreInteractions(amqpTemplate);
        MessageAssert.assertThat(messageCaptor.getValue())
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceInstanceUpdate(newInstance, null, DeviceInstanceUpdate.Type.CREATED)
            );
    }

    @Test
    void publishDeviceInstanceUpdate_whenNewInstanceNull_thenPublishesDeletedEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstance oldInstance
            = deviceInstanceWithDefaults(uuid.toString(), "source1", "tenant1", "deviceTypeIdentifier1");

        // Act
        deviceRegistryEventPublisher.publishDeviceInstanceUpdate(null, oldInstance);

        // Assert & Verify
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(amqpTemplate).send(
            eq(DeviceManagementExchangeConfiguration.NAME),
            eq(DeviceManagementExchangeConfiguration.formatDeviceInstanceUpdateRoutingKey("deviceTypeIdentifier1")),
            messageCaptor.capture()
        );
        verifyNoMoreInteractions(amqpTemplate);
        MessageAssert.assertThat(messageCaptor.getValue())
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceInstanceUpdate(null, oldInstance, DeviceInstanceUpdate.Type.DELETED)
            );
    }

    @Test
    void publishDeviceInstanceUpdate_whenDeviceTypeUnchanged_thenPublishesModifiedEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstance oldInstance
            = deviceInstanceWithDefaults(uuid.toString(), "source1", "tenant1", "deviceTypeIdentifier1")
            .withDescription("Old description");
        DeviceInstance newInstance
            = deviceInstanceWithDefaults(uuid.toString(), "source1", "tenant1", "deviceTypeIdentifier1")
            .withDescription("New description");

        // Act
        deviceRegistryEventPublisher.publishDeviceInstanceUpdate(newInstance, oldInstance);

        // Assert & Verify
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(amqpTemplate).send(
            eq(DeviceManagementExchangeConfiguration.NAME),
            eq(DeviceManagementExchangeConfiguration.formatDeviceInstanceUpdateRoutingKey("deviceTypeIdentifier1")),
            messageCaptor.capture()
        );
        verifyNoMoreInteractions(amqpTemplate);
        MessageAssert.assertThat(messageCaptor.getValue())
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceInstanceUpdate(newInstance, oldInstance, DeviceInstanceUpdate.Type.MODIFIED)
            );
    }

    @Test
    void publishDeviceInstanceUpdate_whenDeviceTypeChanged_thenPublishesDeletedAndCreatedEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceInstance oldInstance
            = deviceInstanceWithDefaults(uuid.toString(), "source1", "tenant1", "deviceTypeIdentifier1");
        DeviceInstance newInstance
            = deviceInstanceWithDefaults(uuid.toString(), "source1", "tenant1", "deviceTypeIdentifier2");

        // Act
        deviceRegistryEventPublisher.publishDeviceInstanceUpdate(newInstance, oldInstance);

        // Assert & Verify
        ArgumentCaptor<String> routingKeyCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(amqpTemplate, times(2)).send(
            eq(DeviceManagementExchangeConfiguration.NAME),
            routingKeyCaptor.capture(),
            messageCaptor.capture()
        );
        assertThat(routingKeyCaptor.getAllValues())
            .hasSize(2)
            .containsExactly(
                DeviceManagementExchangeConfiguration.formatDeviceInstanceUpdateRoutingKey("deviceTypeIdentifier1"),
                DeviceManagementExchangeConfiguration.formatDeviceInstanceUpdateRoutingKey("deviceTypeIdentifier2")
            );
        assertThat(messageCaptor.getAllValues()).hasSize(2);
        MessageAssert.assertThat(messageCaptor.getAllValues().get(0))
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceInstanceUpdate(null, oldInstance, DeviceInstanceUpdate.Type.DELETED)
            );
        MessageAssert.assertThat(messageCaptor.getAllValues().get(1))
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceInstanceUpdate(newInstance, null, DeviceInstanceUpdate.Type.CREATED)
            );
        verifyNoMoreInteractions(amqpTemplate);
    }

    @Test
    void publishDeviceTypeUpdate_whenNewAndOldInstanceNull_thenThrowsException() {
        // Act & Assert
        assertThatThrownBy(() -> deviceRegistryEventPublisher.publishDeviceInstanceUpdate(null, null))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void publishDeviceTypeUpdate_whenOldTypeNull_thenPublishesCreatedEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceType newType
            = deviceTypeWithDefaults(uuid.toString(), "source1", "deviceTypeIdentifier1");

        // Act
        deviceRegistryEventPublisher.publishDeviceTypeUpdate(newType, null);

        // Assert & Verify
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(amqpTemplate).send(
            eq(DeviceManagementExchangeConfiguration.NAME),
            eq(DeviceManagementExchangeConfiguration.formatDeviceTypeUpdateRoutingKey("deviceTypeIdentifier1")),
            messageCaptor.capture()
        );
        verifyNoMoreInteractions(amqpTemplate);
        MessageAssert.assertThat(messageCaptor.getValue())
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceTypeUpdate(newType, null, DeviceTypeUpdate.Type.CREATED)
            );
    }

    @Test
    void publishDeviceTypeUpdate_whenNewTypeNull_thenPublishesDeletedEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceType oldType
            = deviceTypeWithDefaults(uuid.toString(), "source1", "deviceTypeIdentifier1");

        // Act
        deviceRegistryEventPublisher.publishDeviceTypeUpdate(null, oldType);

        // Assert & Verify
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(amqpTemplate).send(
            eq(DeviceManagementExchangeConfiguration.NAME),
            eq(DeviceManagementExchangeConfiguration.formatDeviceTypeUpdateRoutingKey("deviceTypeIdentifier1")),
            messageCaptor.capture()
        );
        verifyNoMoreInteractions(amqpTemplate);
        MessageAssert.assertThat(messageCaptor.getValue())
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceTypeUpdate(null, oldType, DeviceTypeUpdate.Type.DELETED)
            );
    }

    @Test
    void publishDeviceTypeUpdate_whenNewAndOldTypeNotNull_thenPublishesModifiedEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceType oldType
            = deviceTypeWithDefaults(uuid.toString(), "source1", "deviceTypeIdentifier1")
            .withDescription("Old description");
        DeviceType newType
            = deviceTypeWithDefaults(uuid.toString(), "source1", "deviceTypeIdentifier1")
            .withDescription("New description");

        // Act
        deviceRegistryEventPublisher.publishDeviceTypeUpdate(newType, oldType);

        // Assert & Verify
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(amqpTemplate).send(
            eq(DeviceManagementExchangeConfiguration.NAME),
            eq(DeviceManagementExchangeConfiguration.formatDeviceTypeUpdateRoutingKey("deviceTypeIdentifier1")),
            messageCaptor.capture()
        );
        verifyNoMoreInteractions(amqpTemplate);
        MessageAssert.assertThat(messageCaptor.getValue())
            .hasUtf8ContentEncoding()
            .hasJsonContentType()
            .hasBodyContainingObjectAsJson(
                new DeviceTypeUpdate(newType, oldType, DeviceTypeUpdate.Type.MODIFIED)
            );
    }
}
