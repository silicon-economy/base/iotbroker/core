/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceAlreadyExistsException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.DeviceInput;
import org.siliconeconomy.iotbroker.model.device.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link DeviceController}.
 *
 * @author M. Grzenia
 */
@SpringBootTest(classes = {DeviceController.class})
class DeviceControllerTest {

    /**
     * Class under test.
     */
    @Autowired
    @InjectMocks
    private DeviceController controller;

    /**
     * Test dependencies.
     */
    @MockBean
    private DeviceService service;

    /**
     * Test environment.
     */
    private MockMvc mockMvc;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders
            .standaloneSetup(controller)
            .build();
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getDevices() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/devices")
                .param("pageIndex", "0")
                .param("pageSize", "10")
                .param("sortBySource", "ASC")
                .param("sortByTenant", "DESC")
            )
            .andReturn();

        // Assert & Verify
        verify(service).findAll(0, 10, Sort.Direction.ASC, Sort.Direction.DESC);
    }

    @Test
    void getDevicesCount() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/devices/count")
            )
            .andReturn();

        // Assert & Verify
        verify(service).count();
    }

    @Test
    void getDeviceSources() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/devices/sources")
            )
            .andReturn();

        // Assert & Verify
        verify(service).findAllSources();
    }

    @Test
    void getDevicesBySource() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/devices/sources/{source}/tenants", "somesource")
                .param("pageIndex", "0")
                .param("pageSize", "7")
                .param("sortByTenant", "ASC")
            )
            .andReturn();

        // Assert & Verify
        verify(service).findAllBySource("somesource", 0, 7, Sort.Direction.ASC);
    }

    @Test
    void getDevice() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/devices/sources/{source}/tenants/{tenant}", "somesource", "sometenant")
            )
            .andReturn();

        // Assert & Verify
        verify(service).findBySourceAndTenant("somesource", "sometenant");
    }

    @Test
    void getDevice_deviceNotFound() throws Exception {
        // Arrange
        when(service.findBySourceAndTenant(anyString(), anyString()))
            .thenThrow(new DeviceNotFoundException("somesource", "sometenant"));

        // Act & Assert
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/devices/sources/{source}/tenants/{tenant}", "somesource", "sometenant")
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void registerDevice() throws Exception {
        // Arrange
        DeviceInput input = new DeviceInput();
        input.setType("Some type");
        input.setModel("Some model");
        input.setDescription("Some description");
        input.setHardwareRevision("Some hardware revision");
        input.setFirmwareVersion("Some firmware version");

        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .post("/devices/sources/{source}/tenants/{tenant}", "somesource", "sometenant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input))
            )
            .andReturn();

        // Assert & Verify
        Device expected = new Device("somesource", "sometenant")
            .withType("Some type")
            .withModel("Some model")
            .withDescription("Some description")
            .withHardwareRevision("Some hardware revision")
            .withFirmwareVersion("Some firmware version");
        verify(service).add(expected);
    }

    @Test
    void registerDevice_deviceAlreadyExists() throws Exception {
        // Arrange
        when(service.add(any(Device.class)))
            .thenThrow(new DeviceAlreadyExistsException("somesource", "sometenant"));
        DeviceInput input = new DeviceInput();
        input.setType("Some type");
        input.setModel("Some model");
        input.setDescription("Some description");
        input.setHardwareRevision("Some hardware revision");
        input.setFirmwareVersion("Some firmware version");

        // Act & Assert
        mockMvc
            .perform(MockMvcRequestBuilders
                .post("/devices/sources/{source}/tenants/{tenant}", "somesource", "sometenant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(MockMvcResultMatchers.status().isConflict());
    }

    @Test
    void updateDevice() throws Exception {
        // Arrange
        DeviceInput input = new DeviceInput();
        input.setType("Some type");
        input.setModel("Some model");
        input.setDescription("Some description");
        input.setHardwareRevision("Some hardware revision");
        input.setFirmwareVersion("Some firmware version");

        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .put("/devices/sources/{source}/tenants/{tenant}", "somesource", "sometenant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input))
            )
            .andReturn();

        // Assert & Verify
        Device expected = new Device("somesource", "sometenant")
            .withType("Some type")
            .withModel("Some model")
            .withDescription("Some description")
            .withHardwareRevision("Some hardware revision")
            .withFirmwareVersion("Some firmware version");
        verify(service).update(expected);
    }

    @Test
    void updateDevice_deviceNotFound() throws Exception {
        // Arrange
        when(service.update(any(Device.class)))
            .thenThrow(new DeviceNotFoundException("somesource", "sometenant"));
        DeviceInput input = new DeviceInput();
        input.setType("Some type");
        input.setModel("Some model");
        input.setDescription("Some description");
        input.setHardwareRevision("Some hardware revision");
        input.setFirmwareVersion("Some firmware version");

        // Act & Assert
        mockMvc
            .perform(MockMvcRequestBuilders
                .put("/devices/sources/{source}/tenants/{tenant}", "somesource", "sometenant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void deleteDevice() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .delete("/devices/sources/{source}/tenants/{tenant}", "somesource", "sometenant")
            )
            .andReturn();

        // Assert & Verify
        verify(service).deleteBySourceAndTenant("somesource", "sometenant");
    }
}
