package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp.DeviceRegistryEventPublisher;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.config.ModelMapperConfig;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.*;

/**
 * Unit tests for {@link DeviceTypeService}.
 *
 * @author M. Grzenia
 */
class DeviceTypeServiceTest {

    /**
     * Class under test.
     */
    private DeviceTypeService service;
    /**
     * Test dependencies.
     */
    private DeviceTypeRepository deviceTypeRepository;
    private DeviceInstanceRepository deviceInstanceRepository;
    private DeviceRegistryEventPublisher deviceRegistryEventPublisher;

    @BeforeEach
    void setUp() {
        deviceTypeRepository = mock(DeviceTypeRepository.class);
        deviceInstanceRepository = mock(DeviceInstanceRepository.class);
        deviceRegistryEventPublisher = mock(DeviceRegistryEventPublisher.class);
        ModelMapper modelMapper = new ModelMapperConfig().modelMapper();
        service = new DeviceTypeService(
            deviceTypeRepository,
            deviceInstanceRepository,
            deviceRegistryEventPublisher,
            modelMapper
        );
    }

    @Test
    void create_whenNewDeviceType_thenCallsSaveOnRepositoryAndPublishesUpdateEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceTypeCreateInputTO input
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        DeviceTypeEntity createdEntity
            = deviceTypeEntityWithDefaults(uuid, "source1", "deviceTypeIdentifier1");
        when(deviceTypeRepository.existsBySourceAndIdentifier(anyString(), anyString()))
            .thenReturn(false);
        when(deviceTypeRepository.save(any(DeviceTypeEntity.class)))
            .thenReturn(createdEntity);

        // Act
        service.create(input);

        // Verify
        verify(deviceTypeRepository).save(any());
        verify(deviceRegistryEventPublisher).publishDeviceTypeUpdate(isNotNull(), isNull());
    }

    @Test
    void create_whenDeviceTypeAlreadyExists_thenThrowsException() {
        // Arrange
        DeviceTypeCreateInputTO input
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        when(deviceTypeRepository.existsBySourceAndIdentifier(anyString(), anyString()))
            .thenReturn(true);

        // Act
        assertThatThrownBy(() -> service.create(input))
            .isInstanceOf(DeviceTypeAlreadyExistsException.class);

        // Verify
        verify(deviceTypeRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceTypeUpdate(any(), any());
    }

    @Test
    void findById_whenValidId_thenCallsFindByIdOnRepository() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceTypeEntity existingEntity
            = deviceTypeEntityWithDefaults(uuid, "source1", "deviceTypeIdentifier1");
        when(deviceTypeRepository.findById(any())).thenReturn(Optional.of(existingEntity));

        // Act
        service.findById(uuid.toString());

        // Verify
        verify(deviceTypeRepository).findById(uuid);
    }

    @Test
    void findById_whenInvalidId_thenThrowsException() {
        // Act & Assert
        assertThatThrownBy(() -> service.findById("not-a-uuid-string"))
            .isInstanceOf(DeviceTypeNotFoundException.class);

        // Verify
        verify(deviceTypeRepository, never()).findById(any());
    }

    @Test
    void findById_whenUnknownDeviceInstance_thenThrowsException() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString();
        when(deviceTypeRepository.findById(any())).thenReturn(Optional.empty());

        // Act
        assertThatThrownBy(() -> service.findById(uuidString))
            .isInstanceOf(DeviceTypeNotFoundException.class);

        // Verify
        verify(deviceTypeRepository).findById(uuid);
    }

    @Test
    void update_whenValidIdAndInput_thenCallsSaveOnRepositoryAndPublishesUpdateEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceTypeEntity existingEntity
            = deviceTypeEntityWithDefaults(uuid, "source1", "deviceTypeIdentifier1")
            .withDescription("Old description");
        DeviceTypeUpdateInputTO input = updateInputWithDefaults();
        input.setDescription("New description");
        DeviceTypeEntity updatedEntity
            = deviceTypeEntityWithDefaults(uuid, "source1", "deviceTypeIdentifier1")
            .withDescription("New description");
        when(deviceTypeRepository.findById(uuid)).thenReturn(Optional.of(existingEntity));
        when(deviceTypeRepository.save(any(DeviceTypeEntity.class)))
            .thenReturn(updatedEntity);

        // Act
        service.update(uuid.toString(), input);

        // Verify
        verify(deviceTypeRepository).save(any());
        verify(deviceRegistryEventPublisher).publishDeviceTypeUpdate(isNotNull(), isNotNull());
    }

    @Test
    void update_whenInvalidId_thenThrowsException() {
        // Arrange
        DeviceTypeUpdateInputTO input = updateInputWithDefaults();

        // Act & Assert
        assertThatThrownBy(() -> service.update("not-a-uuid-string", input))
            .isInstanceOf(DeviceTypeNotFoundException.class);

        // Verify
        verify(deviceTypeRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceTypeUpdate(any(), any());
    }

    @Test
    void update_whenUnknownDeviceType_thenThrowsException() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString();
        DeviceTypeUpdateInputTO input = updateInputWithDefaults();
        when(deviceTypeRepository.findById(uuid)).thenReturn(Optional.empty());

        // Act & Assert
        assertThatThrownBy(() -> service.update(uuidString, input))
            .isInstanceOf(DeviceTypeNotFoundException.class);

        // Verify
        verify(deviceTypeRepository, never()).save(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceTypeUpdate(any(), any());
    }

    @Test
    void deleteById_whenKnownAndStaleDeviceType_thenCallsDeleteByIdOnRepositoryAndPublishesUpdateEvent() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        DeviceTypeEntity existingEntity
            = deviceTypeEntityWithDefaults(uuid, "source1", "deviceTypeIdentifier1");
        when(deviceTypeRepository.findById(uuid)).thenReturn(Optional.of(existingEntity));
        when(deviceInstanceRepository.existsBySourceAndDeviceTypeIdentifier("source1", "deviceTypeIdentifier1"))
            .thenReturn(false);

        // Act
        service.deleteById(uuid.toString());

        // Verify
        verify(deviceTypeRepository).deleteById(uuid);
        verify(deviceRegistryEventPublisher).publishDeviceTypeUpdate(isNull(), isNotNull());
    }

    @Test
    void deleteById_whenInvalidId_thenDoesNothing() {
        // Act
        service.deleteById("not-a-uuid-string");

        // Verify
        verify(deviceTypeRepository, never()).deleteById(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceTypeUpdate(any(), any());
    }

    @Test
    void deleteById_whenUnknownDeviceType_thenDoesNothing() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        when(deviceTypeRepository.existsById(uuid)).thenReturn(false);

        // Act
        service.deleteById(uuid.toString());

        // Verify
        verify(deviceTypeRepository, never()).deleteById(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceTypeUpdate(any(), any());
    }

    @Test
    void deleteById_whenDeviceTypeSillReferencedByDeviceInstances_thenThrowsException() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString();
        DeviceTypeEntity existingEntity
            = deviceTypeEntityWithDefaults(uuid, "source1", "deviceTypeIdentifier1");
        when(deviceTypeRepository.findById(uuid)).thenReturn(Optional.of(existingEntity));
        when(deviceInstanceRepository.existsBySourceAndDeviceTypeIdentifier("source1", "deviceTypeIdentifier1"))
            .thenReturn(true);

        // Act
        assertThatThrownBy(() -> service.deleteById(uuidString))
            .isInstanceOf(DeviceTypeStillReferencedException.class);

        // Verify
        verify(deviceTypeRepository, never()).deleteById(any());
        verify(deviceRegistryEventPublisher, never()).publishDeviceTypeUpdate(any(), any());
    }

    @Test
    void findAll_whenNoFilterProvided_thenCallsFindAllOnRepository() {
        // Arrange
        Pageable pageable = Pageable.unpaged();
        when(deviceTypeRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());

        // Act
        service.findAll(null, null, pageable);

        // Verify
        verify(deviceTypeRepository).findAll(pageable);
    }

    @Test
    void findAll_whenSourceFilterProvided_thenCallsFindAllBySourceOnRepository() {
        // Arrange
        Pageable pageable = Pageable.unpaged();
        when(deviceTypeRepository.findAllBySource(anyString(), any(Pageable.class)))
            .thenReturn(Page.empty());

        // Act
        service.findAll("source1", null, pageable);

        // Verify
        verify(deviceTypeRepository).findAllBySource("source1", pageable);
    }

    @Test
    void findAll_whenIdentifierFilterProvided_thenCallsFindAllByIdentifierOnRepository() {
        // Arrange
        Pageable pageable = Pageable.unpaged();
        when(deviceTypeRepository.findAllByIdentifier(anyString(), any(Pageable.class)))
            .thenReturn(Page.empty());

        // Act
        service.findAll(null, "deviceTypeIdentifier1", pageable);

        // Verify
        verify(deviceTypeRepository).findAllByIdentifier("deviceTypeIdentifier1", pageable);
    }

    @Test
    void findAll_whenSourceAndIdentifierFilterProvided_thenCallsFindBySourceAndIdentifierOnRepository() {
        // Arrange
        when(deviceTypeRepository.findBySourceAndIdentifier(anyString(), anyString()))
            .thenReturn(Optional.empty());

        // Act
        service.findAll("source1", "deviceTypeIdentifier1", Pageable.unpaged());

        // Verify
        verify(deviceTypeRepository).findBySourceAndIdentifier("source1", "deviceTypeIdentifier1");
    }

    @Test
    void count_whenCalled_thenCallsCountOnRepository() {
        // Act
        service.count();

        // Verify
        verify(deviceTypeRepository).count();
    }

    @Test
    void findAllSources_whenCalled_thenCallsFindAllSourcesOnRepository() {
        // Act
        service.findAllSources();

        // Verify
        verify(deviceTypeRepository).findAllSources();
    }
}
