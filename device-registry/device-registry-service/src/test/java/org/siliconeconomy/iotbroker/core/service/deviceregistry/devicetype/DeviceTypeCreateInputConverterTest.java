/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;

import java.util.Set;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Unit tests for {@link DeviceTypeCreateInputConverter}.
 *
 * @author M. Grzenia
 */
class DeviceTypeCreateInputConverterTest {

    /**
     * Class under test.
     */
    private DeviceTypeCreateInputConverter converter;

    @BeforeEach
    void setUp() {
        converter = new DeviceTypeCreateInputConverter();
    }

    @Test
    void convert_whenAttributesWithNonNullValues_thenReturnsEntityWithAppliedValues() {
        // Arrange
        DeviceTypeCreateInputTO input = new DeviceTypeCreateInputTO()
            .setSource("source1")
            .setIdentifier("deviceTypeIdentifier1");
        input.setProvidedBy(Set.of("adapter1"))
            .setDescription("Device type 1 description")
            .setEnabled(true)
            .setAutoRegisterDeviceInstances(true)
            .setAutoEnableDeviceInstances(false);

        // Act
        DeviceTypeEntity result = converter.convert(input);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isNotNull();
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getProvidedBy()).containsExactly("adapter1");
            softly.assertThat(result.getDescription()).isEqualTo("Device type 1 description");
            softly.assertThat(result.isEnabled()).isTrue();
            softly.assertThat(result.isAutoRegisterDeviceInstances()).isTrue();
            softly.assertThat(result.isAutoEnableDeviceInstances()).isFalse();
        });
    }

    @Test
    void convert_whenAttributesWithNullValues_thenReturnsEntityWithDefaultValues() {
        // Arrange
        DeviceTypeCreateInputTO input = new DeviceTypeCreateInputTO()
            .setSource("source1")
            .setIdentifier("deviceTypeIdentifier1");
        input.setProvidedBy(null)
            .setDescription(null)
            .setEnabled(null)
            .setAutoRegisterDeviceInstances(null)
            .setAutoEnableDeviceInstances(null);

        // Act
        DeviceTypeEntity result = converter.convert(input);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isNotNull();
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getProvidedBy()).isEmpty();
            softly.assertThat(result.getDescription()).isEmpty();
            softly.assertThat(result.isEnabled()).isFalse();
            softly.assertThat(result.isAutoRegisterDeviceInstances()).isFalse();
            softly.assertThat(result.isAutoEnableDeviceInstances()).isFalse();
        });
    }
}
