/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;

import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Unit tests for {@link DeviceTypeUpdateInputConverter}.
 *
 * @author M. Grzenia
 */
class DeviceTypeUpdateInputConverterTest {

    @Test
    void convert_whenAttributesWithNonNullValues_thenReturnsEntityWithUpdatedValues() {
        // Arrange
        DeviceTypeEntity entity = new DeviceTypeEntity(
            UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"),
            "source1",
            "deviceTypeIdentifier1",
            Set.of("adapter1"),
            "Device type 1 description",
            true,
            true,
            true
        );
        DeviceTypeUpdateInputTO update = new DeviceTypeUpdateInputTO();
        update.setProvidedBy(Set.of("adapter1", "adapter2"))
            .setDescription("New device type 1 description")
            .setEnabled(false)
            .setAutoRegisterDeviceInstances(false)
            .setAutoEnableDeviceInstances(false);

        // Act
        DeviceTypeEntity result = DeviceTypeUpdateInputConverter.convert(entity, update);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo(UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"));
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getProvidedBy()).containsExactlyInAnyOrder("adapter1", "adapter2");
            softly.assertThat(result.getDescription()).isEqualTo("New device type 1 description");
            softly.assertThat(result.isEnabled()).isFalse();
            softly.assertThat(result.isAutoRegisterDeviceInstances()).isFalse();
            softly.assertThat(result.isAutoEnableDeviceInstances()).isFalse();
        });
    }

    @Test
    void convert_whenAttributesWithNullValues_thenReturnsEntityWithOriginalValues() {
        // Arrange
        DeviceTypeEntity entity = new DeviceTypeEntity(
            UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"),
            "source1",
            "deviceTypeIdentifier1",
            Set.of("adapter1"),
            "Device type 1 description",
            true,
            true,
            true
        );
        DeviceTypeUpdateInputTO update = new DeviceTypeUpdateInputTO();
        update.setProvidedBy(null)
            .setDescription(null)
            .setEnabled(null)
            .setAutoRegisterDeviceInstances(null)
            .setAutoEnableDeviceInstances(null);

        // Act
        DeviceTypeEntity result = DeviceTypeUpdateInputConverter.convert(entity, update);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo(UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"));
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getProvidedBy()).containsExactly("adapter1");
            softly.assertThat(result.getDescription()).isEqualTo("Device type 1 description");
            softly.assertThat(result.isEnabled()).isTrue();
            softly.assertThat(result.isAutoRegisterDeviceInstances()).isTrue();
            softly.assertThat(result.isAutoEnableDeviceInstances()).isTrue();
        });
    }
}
