/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for {@link DeviceInstanceEntityConverter}.
 *
 * @author M. Grzenia
 */
class DeviceInstanceEntityConverterTest {

    /**
     * Class under test.
     */
    private DeviceInstanceEntityConverter converter;

    @BeforeEach
    void setUp() {
        converter = new DeviceInstanceEntityConverter();
    }

    @Test
    void convert() {
        // Arrange
        DeviceInstanceEntity entity = new DeviceInstanceEntity(
            UUID.fromString("5ad25934-e499-4709-9e8e-72c4d47a0d71"),
            "source1",
            "tenant1",
            "deviceTypeIdentifier1",
            Instant.ofEpochMilli(1646122484120L),
            Instant.ofEpochMilli(1646122485000L),
            true,
            "Device instance 1 description",
            "v3.2.7",
            "v6.3.1"
        );

        // Act
        DeviceInstance result = converter.convert(entity);

        // Assert
        assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo("5ad25934-e499-4709-9e8e-72c4d47a0d71");
            softly.assertThat(result.getSource()).isEqualTo("source1");
            softly.assertThat(result.getTenant()).isEqualTo("tenant1");
            softly.assertThat(result.getDeviceTypeIdentifier()).isEqualTo("deviceTypeIdentifier1");
            softly.assertThat(result.getRegistrationTime()).isNotNull();
            softly.assertThat(result.getRegistrationTime().toEpochMilli()).isEqualTo(1646122484120L);
            softly.assertThat(result.getLastSeen()).isNotNull();
            softly.assertThat(result.getLastSeen().toEpochMilli()).isEqualTo(1646122485000L);
            softly.assertThat(result.isEnabled()).isTrue();
            softly.assertThat(result.getDescription()).isEqualTo("Device instance 1 description");
            softly.assertThat(result.getHardwareRevision()).isEqualTo("v3.2.7");
            softly.assertThat(result.getFirmwareVersion()).isEqualTo("v6.3.1");
        });
    }
}
