/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static java.util.Objects.requireNonNull;

/**
 * Defines some assertions related to {@link Message}s.
 *
 * @author M. Grzenia
 */
public class MessageAssert extends AbstractAssert<MessageAssert, Message> {

    private final ObjectMapper objectMapper;

    private MessageAssert(Message message) {
        super(message, MessageAssert.class);

        objectMapper = new ObjectMapper()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .registerModule(new JavaTimeModule());
    }

    public static MessageAssert assertThat(Message message) {
        requireNonNull(message, "message");
        return new MessageAssert(message);
    }

    public MessageAssert hasBodyContainingObjectAsJson(Object object) {
        isNotNull();
        try {
            String objectAsJsonString = objectMapper.writeValueAsString(object);
            Assertions.assertThat(new String(actual.getBody(), StandardCharsets.UTF_8))
                .isEqualToIgnoringWhitespace(objectAsJsonString);
        } catch (IOException e) {
            failWithMessage("Failed to write object as string.\n%s", e);
        }
        return this;
    }

    public MessageAssert hasUtf8ContentEncoding() {
        isNotNull();
        Assertions.assertThat(actual.getMessageProperties().getContentEncoding())
            .isEqualTo(StandardCharsets.UTF_8.toString());
        return this;
    }

    public MessageAssert hasJsonContentType() {
        isNotNull();
        Assertions.assertThat(actual.getMessageProperties().getContentType())
            .isEqualTo(MessageProperties.CONTENT_TYPE_JSON);
        return this;
    }
}
