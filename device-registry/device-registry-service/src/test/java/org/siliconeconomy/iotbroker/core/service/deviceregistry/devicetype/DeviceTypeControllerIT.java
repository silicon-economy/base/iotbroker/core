/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.CustomResultMatchers.responseBody;
import static org.siliconeconomy.iotbroker.core.service.deviceregistry.TestDataFactory.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for {@link DeviceTypeController}.
 *
 * @author M. Grzenia
 */
@WebMvcTest(controllers = DeviceTypeController.class)
class DeviceTypeControllerIT {

    /**
     * Test dependencies.
     */
    @MockBean
    private DeviceTypeService service;

    /**
     * Test environment.
     */
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void fetchDeviceTypes_whenValidInput_thenReturns200AndDeviceTypes() throws Exception {
        // Arrange
        List<DeviceType> deviceTypes
            = List.of(deviceTypeWithDefaults("id1", "source1", "deviceTypeIdentifier1"));
        when(service.findAll(any(), any(), any())).thenReturn(deviceTypes);

        // Act & Assert
        mockMvc.perform(get("/deviceTypes"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(deviceTypes));

        // Verify
        verify(service)
            .findAll(null, null, PageRequest.of(0, 10, Sort.Direction.ASC, "source", "identifier"));
    }

    @Test
    void fetchDeviceTypes_whenInvalidInput_thenReturns400() throws Exception {
        // Act & Assert
        mockMvc.perform(get("/deviceTypes").param("pageIndex", "-1"))
            .andExpect(status().isBadRequest());

        mockMvc.perform(get("/deviceTypes").param("pageSize", "-1"))
            .andExpect(status().isBadRequest());

        mockMvc.perform(get("/deviceTypes").param("sortBySource", "***"))
            .andExpect(status().isBadRequest());

        mockMvc.perform(get("/deviceTypes").param("sortByIdentifier", "***"))
            .andExpect(status().isBadRequest());
    }

    @Test
    void registerDeviceInstance_whenValidInput_thenReturns201AndCreatedDeviceType() throws Exception {
        // Arrange
        DeviceTypeCreateInputTO input
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        DeviceType deviceType
            = deviceTypeWithDefaults("id1", "source1", "deviceTypeIdentifier1");
        when(service.create(any())).thenReturn(deviceType);

        // Act & Assert
        mockMvc.perform(
                post("/deviceTypes")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isCreated())
            .andExpect(responseBody().containsObjectAsJson(deviceType));

        // Verify
        verify(service).create(input);
    }

    @Test
    void registerDeviceType_whenDeviceTypeAlreadyExists_thenReturns409() throws Exception {
        // Arrange
        DeviceTypeCreateInputTO input
            = createInputWithDefaults("source1", "deviceTypeIdentifier1");
        when(service.create(any())).thenThrow(new DeviceTypeAlreadyExistsException("", ""));

        // Act & Assert
        mockMvc.perform(
                post("/deviceTypes")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isConflict());
    }

    @Test
    void registerDeviceType_whenInvalidInput_thenReturns400AndValidationErrorResponse() throws Exception {
        // Arrange
        DeviceTypeCreateInputTO input = createInputWithDefaults(null, null);

        // Act & Assert
        mockMvc.perform(
                post("/deviceTypes")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isBadRequest())
            .andExpect(responseBody().containsValidationError("source", "must not be null nor empty"))
            .andExpect(responseBody().containsValidationError("identifier", "must not be null nor empty"));
    }

    @Test
    void fetchDeviceTypesCount_whenCalled_thenReturns200AndCount() throws Exception {
        // Arrange
        when(service.count()).thenReturn(11L);

        // Act & Assert
        mockMvc.perform(get("/deviceTypes/count"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(11L));

        // Verify
        verify(service).count();
    }

    @Test
    void fetchDeviceTypeSources_whenCalled_thenReturns200AndSources() throws Exception {
        // Arrange
        when(service.findAllSources()).thenReturn(Set.of("source1", "source2"));

        // Act & Assert
        mockMvc.perform(get("/deviceTypes/sources"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(Set.of("source1", "source2")));

        // Verify
        verify(service).findAllSources();
    }

    @Test
    void fetchDeviceType_whenKnownDeviceType_thenReturns200AndDeviceType() throws Exception {
        // Arrange
        DeviceType deviceType
            = deviceTypeWithDefaults("id1", "source1", "deviceTypeIdentifier1");
        when(service.findById(anyString())).thenReturn(deviceType);

        // Act & Assert
        mockMvc.perform(get("/deviceTypes/{id}", "id1"))
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(deviceType));

        // Verify
        verify(service).findById("id1");
    }

    @Test
    void fetchDeviceType_whenUnknownDeviceType_thenReturns404() throws Exception {
        // Arrange
        when(service.findById(anyString())).thenThrow(new DeviceTypeNotFoundException(""));

        // Act & Assert
        mockMvc.perform(get("/deviceTypes/{id}", "id1"))
            .andExpect(status().isNotFound());

        // Verify
        verify(service).findById("id1");
    }

    @Test
    void updateDeviceType_whenValidInput_thenReturns200AndUpdatedDeviceType() throws Exception {
        // Arrange
        DeviceTypeUpdateInputTO input = updateInputWithDefaults();
        DeviceType deviceType
            = deviceTypeWithDefaults("id1", "source1", "deviceTypeIdentifier1");
        when(service.update(anyString(), any())).thenReturn(deviceType);

        // Act & Assert
        mockMvc.perform(
                put("/deviceTypes/{id}", "id1")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isOk())
            .andExpect(responseBody().containsObjectAsJson(deviceType));

        // Verify
        verify(service).update("id1", input);
    }

    @Test
    void updateDeviceType_whenUnknownDeviceType_thenReturns404() throws Exception {
        // Arrange
        DeviceTypeUpdateInputTO input = updateInputWithDefaults();
        when(service.update(anyString(), any())).thenThrow(new DeviceTypeNotFoundException(""));

        // Act & Assert
        mockMvc.perform(
                put("/deviceTypes/{id}", "id1")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(input))
            )
            .andExpect(status().isNotFound());

        // Verify
        verify(service).update("id1", input);
    }

    @Test
    void deleteDeviceType_whenUnusedOrUnknownDeviceType_thenReturns200() throws Exception {
        // Act & Assert
        mockMvc.perform(delete("/deviceTypes/{id}", "id1"))
            .andExpect(status().isOk());

        // Verify
        verify(service).deleteById("id1");
    }

    @Test
    void deleteDeviceType_whenDeviceTypeStillReferenced_thenReturns409() throws Exception {
        // Arrange
        doThrow(new DeviceTypeStillReferencedException("", ""))
            .when(service).deleteById(anyString());

        // Act & Assert
        mockMvc.perform(delete("/deviceTypes/{id}", "id1"))
            .andExpect(status().isConflict());

        // Verify
        verify(service).deleteById("id1");
    }
}
