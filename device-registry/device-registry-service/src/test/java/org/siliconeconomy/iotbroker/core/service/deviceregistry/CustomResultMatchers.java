/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.ValidationError;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.ValidationErrorResponse;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.bind.annotation.RestController;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Defines some {@link ResultMatcher}s to help with testing {@link RestController}s.
 *
 * @author M. Grzenia
 */
public class CustomResultMatchers {

    private final ObjectMapper objectMapper;

    private CustomResultMatchers() {
        // We don't have access to the object mapper instance provided by Spring (which is already
        // configured properly). Therefore, configure the object mapper to allow handling of
        // Java 8 date/time types.
        objectMapper = new ObjectMapper()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .registerModule(new JavaTimeModule());
    }

    public ResultMatcher containsObjectAsJson(Object expectedObject) {
        return mvcResult -> {
            String responseJson = mvcResult.getResponse().getContentAsString();
            String expectedJson = objectMapper.writeValueAsString(expectedObject);
            assertThat(responseJson).isEqualToIgnoringWhitespace(expectedJson);
        };
    }

    public ResultMatcher containsValidationError(String expectedProperty, String expectedMessage) {
        return mvcResult -> {
            String responseJson = mvcResult.getResponse().getContentAsString();
            ValidationErrorResponse errorResponse = objectMapper.readValue(responseJson, ValidationErrorResponse.class);
            ValidationError expectedValidationError = new ValidationError(expectedProperty, expectedMessage);

            assertThat(errorResponse.getValidationErrors())
                .withFailMessage(
                    "Expected error message with property '%s' and message '%s'.",
                    expectedValidationError.getProperty(),
                    expectedValidationError.getMessage()
                )
                .contains(expectedValidationError);
        };
    }

    public static CustomResultMatchers responseBody() {
        return new CustomResultMatchers();
    }
}
