/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.model.device.Device;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This DTO is used to interact with the database.
 * <p>
 * It is basically a copy of the {@link Device} class, but additionally provides an ID field for
 * the database entries and defines the table in which the entities are persisted.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstanceEntity} instead.
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString(callSuper = true)
@Entity
@Table(name = "devices")
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceEntity extends DeviceDto {

    /**
     * The unique ID of the entity in the database.
     */
    @Id
    @GeneratedValue
    private Long id;
    /**
     * The source that the device represents. (E.g. as a source of data/messages.)
     */
    @NonNull
    private String source;
    /**
     * A unique identifier for the device. (E.g. the device ID. Only unique in the context of the
     * device's source.)
     */
    @NonNull
    private String tenant;
}
