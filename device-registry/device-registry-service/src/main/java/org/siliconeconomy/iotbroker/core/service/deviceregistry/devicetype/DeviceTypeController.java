/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.RequestExceptionHandler;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * A REST controller providing endpoints related to the management of {@link DeviceType}s.
 *
 * @author M. Grzenia
 */
@RestController
@RequestMapping("/deviceTypes")
public class DeviceTypeController {

    /**
     * Provides methods for managing {@link DeviceType}s.
     */
    private final DeviceTypeService service;

    @Autowired
    public DeviceTypeController(DeviceTypeService service) {
        this.service = service;
    }

    /**
     * Returns a list of registered devices types.
     * <p>
     * Which elements are included in the list depends on the filter parameters, which can be
     * specified optionally, as well as on the pagination parameters.
     * <p>
     * If the provided pagination parameters are invalid, the request will be handled by
     * {@link RequestExceptionHandler#handleIllegalArgumentException(IllegalArgumentException)}.
     *
     * @param source           A filter to retrieve only information about device types associated
     *                         with a specific source.
     * @param identifier       A filter to retrieve only information about device types with a
     *                         specific identifier.
     * @param pageIndex        The page index to retrieve device instances for.
     * @param pageSize         The size of a single page.
     * @param sortBySource     The direction in which the retrieved device types are to be sorted
     *                         according to their source.
     * @param sortByIdentifier The direction in which the retrieved device types are to be sorted
     *                         according to their identifier.
     * @return A list of {@link DeviceType}s.
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<DeviceType> fetchDeviceTypes(
        @RequestParam(required = false) String source,
        @RequestParam(required = false) String identifier,
        @RequestParam(defaultValue = "0") int pageIndex,
        @RequestParam(defaultValue = "10") int pageSize,
        @RequestParam(defaultValue = "ASC") String sortBySource,
        @RequestParam(defaultValue = "ASC") String sortByIdentifier) {
        Pageable pageable = PageRequest.of(
            pageIndex,
            pageSize,
            Sort.by(Sort.Direction.fromString(sortBySource), "source")
                .and(Sort.by(Sort.Direction.fromString(sortByIdentifier), "identifier"))
        );

        return service.findAll(source, identifier, pageable);
    }

    /**
     * Registers a device type using the given input data.
     * <p>
     * If a device type with the source and identifier contained in the given input data already
     * exists, the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceTypeAlreadyExistsException(DeviceTypeAlreadyExistsException)}.
     * <p>
     * If validation of the given input fails, the request will be handled by
     * {@link RequestExceptionHandler#handleMethodArgumentNotValidException(MethodArgumentNotValidException)}
     *
     * @param input The input to use for registering a new device type.
     * @return The registered {@link DeviceType}.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DeviceType registerDeviceType(@Valid @RequestBody DeviceTypeCreateInputTO input) {
        return service.create(input);
    }

    /**
     * Returns the total number of registered device types.
     *
     * @return The total number of registered device types.
     */
    @GetMapping("/count")
    @ResponseStatus(HttpStatus.OK)
    public long fetchDeviceTypesCount() {
        return service.count();
    }

    /**
     * Returns a set of all registered sources of device types.
     *
     * @return A set of all registered sources of device types.
     */
    @GetMapping("/sources")
    @ResponseStatus(HttpStatus.OK)
    public Set<String> fetchDeviceTypeSources() {
        return service.findAllSources();
    }


    /**
     * Returns the device type with the given ID.
     * <p>
     * If a device type with the given ID doesn't exist, the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceTypeNotFoundException(DeviceTypeNotFoundException)}.
     *
     * @param id The ID of the device type.
     * @return The {@link DeviceType}.
     */
    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public DeviceType fetchDeviceType(@PathVariable String id) {
        return service.findById(id);
    }


    /**
     * Updates the device type with the given ID using the given input data.
     * <p>
     * If a device type with the given ID doesn't exist, the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceTypeNotFoundException(DeviceTypeNotFoundException)}.
     *
     * @param id    The ID of the device type.
     * @param input The input to use for updating the device type.
     * @return The updated {@link DeviceType}.
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DeviceType updateDeviceType(@PathVariable String id,
                                       @RequestBody DeviceTypeUpdateInputTO input) {
        return service.update(id, input);
    }

    /**
     * Deletes the device type with the given ID.
     *
     * @param id The ID of the device type.
     */
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteDeviceType(@PathVariable String id) {
        service.deleteById(id);
    }
}
