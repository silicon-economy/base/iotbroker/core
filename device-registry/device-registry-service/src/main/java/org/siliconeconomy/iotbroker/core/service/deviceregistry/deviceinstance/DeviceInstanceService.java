/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.modelmapper.ModelMapper;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp.DeviceRegistryEventPublisher;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Implements the business logic for managing {@link DeviceInstance}s.
 *
 * @author M. Grzenia
 */
@Service
public class DeviceInstanceService {

    /**
     * Provides access to the {@link DeviceInstanceEntity} database.
     */
    private final DeviceInstanceRepository deviceInstanceRepository;
    /**
     * Provides access to the {@link DeviceTypeEntity} database.
     */
    private final DeviceTypeRepository deviceTypeRepository;
    /**
     * Used to publish update events for {@link DeviceInstance}s.
     */
    private final DeviceRegistryEventPublisher deviceRegistryEventPublisher;
    /**
     * Used for mapping between different business objects.
     */
    private final ModelMapper modelMapper;

    @Autowired
    public DeviceInstanceService(DeviceInstanceRepository deviceInstanceRepository,
                                 DeviceTypeRepository deviceTypeRepository,
                                 DeviceRegistryEventPublisher deviceRegistryEventPublisher,
                                 ModelMapper modelMapper) {
        this.deviceInstanceRepository = deviceInstanceRepository;
        this.deviceTypeRepository = deviceTypeRepository;
        this.deviceRegistryEventPublisher = deviceRegistryEventPublisher;
        this.modelMapper = modelMapper;
    }

    /**
     * Creates a new {@link DeviceInstance} using the given {@link DeviceInstanceCreateInputTO} by
     * adding a corresponding {@link DeviceInstanceEntity} to the database.
     *
     * @param input The input to use for creating a new device instance.
     * @return The created {@link DeviceInstance}.
     * @throws DeviceInstanceAlreadyExistsException If a device instance with the same source and
     *                                              tenant already exists.
     * @throws DeviceTypeNotFoundException          If the device type referenced in the given input
     *                                              doesn't exist.
     */
    public DeviceInstance create(DeviceInstanceCreateInputTO input) {
        if (deviceInstanceRepository.existsBySourceAndTenant(input.getSource(), input.getTenant())) {
            throw new DeviceInstanceAlreadyExistsException(input.getSource(), input.getTenant());
        }

        if (!deviceTypeRepository.existsBySourceAndIdentifier(input.getSource(), input.getDeviceTypeIdentifier())) {
            throw new DeviceTypeNotFoundException(input.getSource(), input.getDeviceTypeIdentifier());
        }

        // Map the transfer object to the entity to persist. This is where the entity gets its ID.
        DeviceInstanceEntity entity = modelMapper.map(input, DeviceInstanceEntity.class);

        DeviceInstanceEntity persistedEntity = deviceInstanceRepository.save(entity);

        // Publish an update event.
        DeviceInstance newInstance = modelMapper.map(persistedEntity, DeviceInstance.class);
        deviceRegistryEventPublisher.publishDeviceInstanceUpdate(newInstance, null);

        return newInstance;
    }

    /**
     * Returns the device instance with the given ID.
     *
     * @param id The ID of the device instance.
     * @return The {@link DeviceInstance}.
     * @throws DeviceInstanceNotFoundException If a device instance with the given ID could not be
     *                                         found.
     */
    public DeviceInstance findById(String id) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            // The service persists entities with UUIDs as their ID. Therefore, there are no
            // entities with IDs other than UUIDs.
            throw new DeviceInstanceNotFoundException(id);
        }

        DeviceInstanceEntity entity = deviceInstanceRepository.findById(uuid)
            .orElseThrow(() -> new DeviceInstanceNotFoundException(id));

        return modelMapper.map(entity, DeviceInstance.class);
    }

    /**
     * Updates the device instance with the given ID using the given input data.
     *
     * @param id    The ID of the device instance.
     * @param input The input to use for updating the device instance.
     * @return The updated {@link DeviceInstance}.
     * @throws DeviceInstanceNotFoundException If a device instance with the given source and tenant
     *                                         could not be found.
     * @throws DeviceTypeNotFoundException     If the device type referenced in the given input
     *                                         doesn't exist.
     */
    public DeviceInstance update(String id, DeviceInstanceUpdateInputTO input) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            // The service persists entities with UUIDs as their ID. Therefore, there are no
            // entities with IDs other than UUIDs.
            throw new DeviceInstanceNotFoundException(id);
        }

        DeviceInstanceEntity existingEntity = deviceInstanceRepository
            .findById(uuid)
            .orElseThrow(() -> new DeviceInstanceNotFoundException(id));

        // When a device type identifier is specified, the value in the existing entity will be
        // overwritten, and we must ensure that the new device type exists.
        if (input.getDeviceTypeIdentifier() != null
            && !deviceTypeRepository.existsBySourceAndIdentifier(existingEntity.getSource(), input.getDeviceTypeIdentifier())) {
            throw new DeviceTypeNotFoundException(existingEntity.getSource(), input.getDeviceTypeIdentifier());
        }

        // We are working with Hibernate and managed entities. This means that after the save
        // operation we lose access to the state of the entity before the update (and only the
        // reference to the updated managed entity remains). Therefore, we need to map the entity
        // BEFORE we perform the save operation.
        DeviceInstance oldInstance = modelMapper.map(existingEntity, DeviceInstance.class);

        // Update the existing entity.
        DeviceInstanceEntity updatedEntity
            = DeviceInstanceUpdateInputConverter.convert(existingEntity, input);

        DeviceInstanceEntity persistedEntity = deviceInstanceRepository.save(updatedEntity);

        // Publish an update event.
        DeviceInstance newInstance = modelMapper.map(persistedEntity, DeviceInstance.class);
        deviceRegistryEventPublisher.publishDeviceInstanceUpdate(newInstance, oldInstance);

        return newInstance;
    }

    /**
     * Deletes the device instance with the given ID.
     *
     * @param id The ID of the device instance.
     */
    public void deleteById(String id) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            // The service persists entities with UUIDs as their ID. Therefore, there are no
            // entities with IDs other than UUIDs.
            return;
        }

        Optional<DeviceInstanceEntity> existingEntityOpt = deviceInstanceRepository.findById(uuid);
        if (existingEntityOpt.isEmpty()) {
            // This avoids an EmptyResultDataAccessException when trying to perform the delete
            // operation in case there is no entity with the given ID.
            return;
        }

        // Publish an update event.
        DeviceInstance oldInstance = modelMapper.map(existingEntityOpt.get(), DeviceInstance.class);
        deviceRegistryEventPublisher.publishDeviceInstanceUpdate(null, oldInstance);

        deviceInstanceRepository.deleteById(uuid);
    }

    /**
     * Returns a list of device instances.
     * <p>
     * Which elements are included in the list depends on the provided pagination information.
     *
     * @param source   A filter to retrieve only information about device instances associated
     *                 with a specific source.
     * @param tenant   A filter to retrieve only information about device instances with a
     *                 specific tenant.
     * @param pageable The pagination information to use when retrieving device instances from
     *                 the database.
     * @return A list of {@link DeviceInstance}s.
     */
    public List<DeviceInstance> findAll(@Nullable String source,
                                        @Nullable String tenant,
                                        Pageable pageable) {
        Page<DeviceInstanceEntity> page;
        if (source != null && tenant != null) {
            page = deviceInstanceRepository.findBySourceAndTenant(source, tenant)
                .map(entity -> new PageImpl<>(List.of(entity)))
                .orElseGet(() -> new PageImpl<>(Collections.emptyList()));
        } else if (source != null) {
            page = deviceInstanceRepository.findAllBySource(source, pageable);
        } else if (tenant != null) {
            page = deviceInstanceRepository.findAllByTenant(tenant, pageable);
        } else {
            page = deviceInstanceRepository.findAll(pageable);
        }

        return page.stream()
            .map(entity -> modelMapper.map(entity, DeviceInstance.class))
            .collect(Collectors.toList());
    }

    /**
     * Returns the total number of device instances.
     *
     * @return The total number of device instances.
     */
    public long count() {
        return deviceInstanceRepository.count();
    }

    /**
     * Returns a set of all sources of device instances.
     *
     * @return A set of all sources of device instances.
     */
    public Set<String> findAllSources() {
        return deviceInstanceRepository.findAllSources();
    }
}
