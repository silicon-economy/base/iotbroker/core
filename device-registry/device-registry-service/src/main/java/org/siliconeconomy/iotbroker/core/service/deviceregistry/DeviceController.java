/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceController;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceAlreadyExistsException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.DeviceInput;
import org.siliconeconomy.iotbroker.model.device.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * Provides REST endpoints related to the management of devices registered with the IoT Broker.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstanceController} instead.
 */
@RestController
@RequestMapping("/devices")
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceController {

    /**
     * Used to perform basic CRUD and advanced operations on the database.
     */
    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    /**
     * Returns all devices registered with the IoT Broker.
     *
     * @param pageIndex    The page index to retrieve devices for.
     * @param pageSize     The size of a single page.
     * @param sortBySource The direction in which the retrieved devices are to be sorted according
     *                     to their source.
     * @param sortByTenant The direction in which the retrieved devices are to be sorted according
     *                     to their tenant.
     * @return A list of {@link Device}s.
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Device> getDevices(
        @RequestParam int pageIndex,
        @RequestParam int pageSize,
        @RequestParam(defaultValue = "ASC") Sort.Direction sortBySource,
        @RequestParam(defaultValue = "ASC") Sort.Direction sortByTenant) {
        return deviceService.findAll(pageIndex, pageSize, sortBySource, sortByTenant);
    }

    /**
     * Returns the number of registered devices.
     *
     * @return The number of registered devices.
     */
    @GetMapping("/count")
    @ResponseStatus(HttpStatus.OK)
    public long getDevicesCount() {
        return deviceService.count();
    }

    /**
     * Returns a set of all registered device sources.
     *
     * @return A set of all registered device sources.
     */
    @GetMapping("/sources")
    @ResponseStatus(HttpStatus.OK)
    public Set<String> getDeviceSources() {
        return deviceService.findAllSources();
    }

    /**
     * Returns all registered devices that represent the given source.
     *
     * @param source       The source.
     * @param pageIndex    The page index to retrieve devices for.
     * @param pageSize     The size of a single page.
     * @param sortByTenant The direction in which the retrieved devices are to be sorted according
     *                     to their tenant.
     * @return A list of {@link Device}s.
     */
    @GetMapping("sources/{source}/tenants")
    @ResponseStatus(HttpStatus.OK)
    public List<Device> getDevicesBySource(
        @PathVariable("source") String source,
        @RequestParam int pageIndex,
        @RequestParam int pageSize,
        @RequestParam(defaultValue = "ASC") Sort.Direction sortByTenant) {
        return deviceService.findAllBySource(source, pageIndex, pageSize, sortByTenant);
    }

    /**
     * Returns the device for the given source and tenant.
     * <p>
     * If an unknown source or tenant was supplied, the request will be handled by
     * {@link #handleDeviceNotFoundException}.
     *
     * @param source The source.
     * @param tenant The tenant.
     * @return The device for the given source and tenant.
     */
    @GetMapping("sources/{source}/tenants/{tenant}")
    @ResponseStatus(HttpStatus.OK)
    public Device getDevice(@PathVariable("source") String source,
                            @PathVariable("tenant") String tenant) {
        return deviceService.findBySourceAndTenant(source, tenant);
    }

    /**
     * Registers a device with the given source, tenant and input data.
     * <p>
     * If a device with the given source and tenant already exists, the request will be handled by
     * {@link #handleDeviceExistsException}.
     *
     * @param source      The source.
     * @param tenant      The tenant.
     * @param deviceInput Input required for creating the new device.
     * @return The registered device.
     */
    @PostMapping("sources/{source}/tenants/{tenant}")
    @ResponseStatus(HttpStatus.CREATED)
    public Device registerDevice(@PathVariable("source") String source,
                                 @PathVariable("tenant") String tenant,
                                 @RequestBody DeviceInput deviceInput) {
        return deviceService.add(new Device(source, tenant)
            .withType(deviceInput.getType())
            .withModel(deviceInput.getModel())
            .withDescription(deviceInput.getDescription())
            .withHardwareRevision(deviceInput.getHardwareRevision())
            .withFirmwareVersion(deviceInput.getFirmwareVersion())
        );
    }

    /**
     * Updates the device with the given source and tenant with the given input data.
     * <p>
     * If an unknown source or tenant was supplied, the request will be handled by
     * {@link #handleDeviceNotFoundException}.
     *
     * @param source      The source.
     * @param tenant      The tenant.
     * @param deviceInput Input required for updating the device.
     * @return The updated device.
     */
    @PutMapping("sources/{source}/tenants/{tenant}")
    @ResponseStatus(HttpStatus.OK)
    public Device updateDevice(@PathVariable("source") String source,
                               @PathVariable("tenant") String tenant,
                               @RequestBody DeviceInput deviceInput) {
        return deviceService.update(new Device(source, tenant)
            .withType(deviceInput.getType())
            .withModel(deviceInput.getModel())
            .withDescription(deviceInput.getDescription())
            .withHardwareRevision(deviceInput.getHardwareRevision())
            .withFirmwareVersion(deviceInput.getFirmwareVersion())
        );
    }

    /**
     * Deletes the device with the given source and tenant.
     *
     * @param source The source.
     * @param tenant The tenant.
     */
    @DeleteMapping("sources/{source}/tenants/{tenant}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteDevice(@PathVariable("source") String source,
                             @PathVariable("tenant") String tenant) {
        deviceService.deleteBySourceAndTenant(source, tenant);
    }

    @ExceptionHandler(DeviceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleDeviceNotFoundException(Exception exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(DeviceAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleDeviceExistsException(Exception exception) {
        return exception.getMessage();
    }
}
