/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.modelmapper.AbstractConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;

import java.util.Collections;
import java.util.UUID;

/**
 * Converts instances of {@link DeviceTypeCreateInputTO} to instances of
 * {@link DeviceTypeEntity}.
 * <p>
 * The converted {@link DeviceTypeEntity} will have its ID set to a randomly generated UUID. If
 * the given {@link DeviceTypeCreateInputTO} contains {@code null} values, the corresponding
 * attributes in the converted {@link DeviceTypeEntity} will be set to appropriate default
 * values.
 *
 * @author M. Grzenia
 */
public class DeviceTypeCreateInputConverter
    extends AbstractConverter<DeviceTypeCreateInputTO, DeviceTypeEntity> {

    @Override
    protected DeviceTypeEntity convert(DeviceTypeCreateInputTO input) {
        return new DeviceTypeEntity(
            UUID.randomUUID(),
            input.getSource(),
            input.getIdentifier(),
            input.getProvidedBy() != null ? input.getProvidedBy() : Collections.emptySet(),
            input.getDescription() != null ? input.getDescription() : "",
            input.getEnabled() != null && input.getEnabled(),
            input.getAutoRegisterDeviceInstances() != null && input.getAutoRegisterDeviceInstances(),
            input.getAutoEnableDeviceInstances() != null && input.getAutoEnableDeviceInstances()
        );
    }
}
