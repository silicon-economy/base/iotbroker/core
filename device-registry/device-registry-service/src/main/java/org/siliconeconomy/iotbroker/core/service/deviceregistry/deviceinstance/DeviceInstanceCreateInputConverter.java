/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.modelmapper.AbstractConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;

import java.time.Instant;
import java.util.UUID;

/**
 * Converts instances of {@link DeviceInstanceCreateInputTO} to instances of
 * {@link DeviceInstanceEntity}.
 * <p>
 * The converted {@link DeviceInstanceEntity} will have its ID set to a randomly generated UUID. If
 * the given {@link DeviceInstanceCreateInputTO} contains {@code null} values, the corresponding
 * attributes in the converted {@link DeviceInstanceEntity} will be set to appropriate default
 * values.
 *
 * @author M. Grzenia
 */
public class DeviceInstanceCreateInputConverter
    extends AbstractConverter<DeviceInstanceCreateInputTO, DeviceInstanceEntity> {

    @Override
    protected DeviceInstanceEntity convert(DeviceInstanceCreateInputTO input) {
        return new DeviceInstanceEntity(
            UUID.randomUUID(),
            input.getSource(),
            input.getTenant(),
            input.getDeviceTypeIdentifier(),
            Instant.now(),
            input.getLastSeen() != null ? input.getLastSeen() : Instant.EPOCH,
            input.getEnabled() != null && input.getEnabled(),
            input.getDescription() != null ? input.getDescription() : "",
            input.getHardwareRevision() != null ? input.getHardwareRevision() : "",
            input.getFirmwareVersion() != null ? input.getFirmwareVersion() : ""
        );
    }
}
