/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.exception;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceNotFoundException;

/**
 * Thrown to indicate that a device with a specific source and tenant could not be found in the
 * database.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstanceNotFoundException} instead.
 */
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceNotFoundException extends RuntimeException {

    public DeviceNotFoundException(String source, String tenant) {
        super(String.format(
            "Could not find device with source '%s' and tenant '%s'.",
            source,
            tenant
        ));
    }
}
