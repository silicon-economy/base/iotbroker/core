/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceAlreadyExistsException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeAlreadyExistsException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeStillReferencedException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.ValidationError;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.ValidationErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * Handles exceptions thrown in any rest controller by providing corresponding responses for
 * different kind of exceptions.
 *
 * @author M. Grzenia
 */
@RestControllerAdvice
public class RequestExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RequestExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleUnhandledException(Exception exception, HttpServletRequest request) {
        LOG.error(
            "An unhandled exception occurred while a client accessed: {} {}",
            request.getMethod(),
            request.getRequestURI(),
            exception
        );
        return "An internal error occurred while fetching/processing data.";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleIllegalArgumentException(IllegalArgumentException exception) {
        return "Invalid parameter value(s): " + exception.getMessage();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ValidationErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        var response = new ValidationErrorResponse();
        for (FieldError error : exception.getFieldErrors()) {
            response.getValidationErrors()
                .add(new ValidationError(error.getField(), error.getDefaultMessage()));
        }
        return response;
    }

    @ExceptionHandler(DeviceInstanceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleDeviceInstanceNotFoundException(DeviceInstanceNotFoundException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(DeviceInstanceAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleDeviceInstanceAlreadyExistsException(DeviceInstanceAlreadyExistsException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(DeviceTypeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleDeviceTypeNotFoundException(DeviceTypeNotFoundException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(DeviceTypeAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleDeviceTypeAlreadyExistsException(DeviceTypeAlreadyExistsException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(DeviceTypeStillReferencedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleDeviceTypeStillReferencedException(DeviceTypeStillReferencedException exception) {
        return exception.getMessage();
    }
}
