/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;

/**
 * Converts instances of {@link DeviceTypeUpdateInputTO} to instances of
 * {@link DeviceTypeEntity} by updating an existing instance of {@link DeviceTypeEntity}.
 * <p>
 * The converted {@link DeviceTypeEntity} first inherits all values from the existing
 * {@link DeviceTypeEntity}. Then, all non-{@code null} values from the given
 * {@link DeviceTypeUpdateInputTO} are be applied to the converted {@link DeviceTypeEntity}.
 *
 * @author M. Grzenia
 */
public class DeviceTypeUpdateInputConverter {

    private DeviceTypeUpdateInputConverter() {
    }

    public static DeviceTypeEntity convert(DeviceTypeEntity entity,
                                           DeviceTypeUpdateInputTO update) {
        DeviceTypeEntity result = entity;

        if (update.getProvidedBy() != null) {
            result = result.withProvidedBy(update.getProvidedBy());
        }
        if (update.getDescription() != null) {
            result = result.withDescription(update.getDescription());
        }
        if (update.getEnabled() != null) {
            result = result.withEnabled(update.getEnabled());
        }
        if (update.getAutoRegisterDeviceInstances() != null) {
            result = result.withAutoRegisterDeviceInstances(update.getAutoRegisterDeviceInstances());
        }
        if (update.getAutoEnableDeviceInstances() != null) {
            result = result.withAutoEnableDeviceInstances(update.getAutoEnableDeviceInstances());
        }

        return result;
    }
}
