/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.utils.amqp.DeviceManagementExchangeConfiguration;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;

/**
 * Used to publish events related to the device registry service via AMQP.
 * <p>
 * Events related to the device registry service can be e.g.:
 * <ul>
 *     <li>Updates to {@link DeviceInstance}s represented by {@link DeviceInstanceUpdate}s</li>
 *     <li>Updates to {@link DeviceType}s represented by {@link DeviceTypeUpdate}s</li>
 * </ul>
 * Details on the configuration of the AMQP exchange that events are published to are defined in
 * {@link DeviceManagementExchangeConfiguration}.
 *
 * @author M. Grzenia
 */
@Component
public class DeviceRegistryEventPublisher {

    /**
     * Used for mapping the events to publish to their JSON representation.
     */
    private final ObjectMapper objectMapper = new ObjectMapper();
    /**
     * The AMQP admin instance to use for declaring exchanges.
     */
    private final AmqpAdmin amqpAdmin;
    /**
     * The AMQP template to use for publishing messages.
     */
    private final AmqpTemplate amqpTemplate;

    @Autowired
    public DeviceRegistryEventPublisher(AmqpAdmin amqpAdmin, AmqpTemplate amqpTemplate) {
        this.amqpAdmin = amqpAdmin;
        this.amqpTemplate = amqpTemplate;

        // Configure the object mapper for device instance updates and device type updates.
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .registerModule(new JavaTimeModule());
    }

    @PostConstruct
    public void initialize() {
        amqpAdmin.declareExchange(
            ExchangeBuilder
                .topicExchange(DeviceManagementExchangeConfiguration.NAME)
                .durable(DeviceManagementExchangeConfiguration.DURABLE)
                .build()
        );
    }

    /**
     * Publishes a {@link DeviceInstanceUpdate} message for the given {@link DeviceInstance}s.
     * <p>
     * Depending on whether only one or both {@link DeviceInstance}s are provided, this method will
     * generate the appropriate {@link DeviceInstanceUpdate} instance. The following rules apply:
     * <ul>
     *     <li> If only {@code newState} is provided, a {@link DeviceInstanceUpdate} with
     *     type {@link DeviceInstanceUpdate.Type#CREATED} will be generated.</li>
     *     <li> If only {@code oldState} is provided, a {@link DeviceInstanceUpdate} with
     *     type {@link DeviceInstanceUpdate.Type#DELETED} will be generated.</li>
     *     <li> If both {@link DeviceInstance}s are provided, a {@link DeviceInstanceUpdate} with
     *     type {@link DeviceInstanceUpdate.Type#MODIFIED} will be generated.</li>
     * </ul>
     * In any case, at least one {@link DeviceInstance} must be provided.
     *
     * @param newState The new state of the {@link DeviceInstance}.
     * @param oldState The old state of the {@link DeviceInstance}.
     */
    public void publishDeviceInstanceUpdate(DeviceInstance newState, DeviceInstance oldState) {
        if (newState == null && oldState == null) {
            throw new IllegalArgumentException(
                "newState and oldState are both null. At least one non-null value required."
            );
        }

        if (oldState == null) {
            publishDeviceInstanceUpdate(
                newState,
                null,
                DeviceInstanceUpdate.Type.CREATED
            );
        } else if (newState == null) {
            publishDeviceInstanceUpdate(
                null,
                oldState,
                DeviceInstanceUpdate.Type.DELETED
            );
        } else {
            if (oldState.getDeviceTypeIdentifier().equals(newState.getDeviceTypeIdentifier())) {
                publishDeviceInstanceUpdate(
                    newState,
                    oldState,
                    DeviceInstanceUpdate.Type.MODIFIED
                );
            } else {
                // Since messages are published to topics that are based on the device type
                // identifier, listeners may never notice that a device is 'gone' when the device
                // type identifier changes. We therefore treat this case as if a device has been
                // re-registered.
                publishDeviceInstanceUpdate(
                    null,
                    oldState,
                    DeviceInstanceUpdate.Type.DELETED
                );
                publishDeviceInstanceUpdate(
                    newState,
                    null,
                    DeviceInstanceUpdate.Type.CREATED
                );
            }
        }
    }

    /**
     * Publishes a {@link DeviceTypeUpdate} message for the given {@link DeviceType}s.
     * <p>
     * Depending on whether only one or both {@link DeviceType}s are provided, this method will
     * generate the appropriate {@link DeviceTypeUpdate} instance. The following rules apply:
     * <ul>
     *     <li> If only {@code newState} is provided, a {@link DeviceTypeUpdate} with
     *     type {@link DeviceTypeUpdate.Type#CREATED} will be generated.</li>
     *     <li> If only {@code oldState} is provided, a {@link DeviceTypeUpdate} with
     *     type {@link DeviceTypeUpdate.Type#DELETED} will be generated.</li>
     *     <li> If both {@link DeviceType}s are provided, a {@link DeviceTypeUpdate} with
     *     type {@link DeviceTypeUpdate.Type#MODIFIED} will be generated.</li>
     * </ul>
     * In any case, at least one {@link DeviceType} must be provided.
     *
     * @param newState The new state of the {@link DeviceType}.
     * @param oldState The old state of the {@link DeviceType}.
     */
    public void publishDeviceTypeUpdate(DeviceType newState, DeviceType oldState) {
        if (newState == null && oldState == null) {
            throw new IllegalArgumentException(
                "newState and oldState are both null. At least one non-null value required."
            );
        }

        if (oldState == null) {
            publishDeviceTypeUpdate(
                newState,
                null,
                DeviceTypeUpdate.Type.CREATED
            );
        } else if (newState == null) {
            publishDeviceTypeUpdate(
                null,
                oldState,
                DeviceTypeUpdate.Type.DELETED
            );
        } else {
            publishDeviceTypeUpdate(
                newState,
                oldState,
                DeviceTypeUpdate.Type.MODIFIED
            );
        }
    }

    private void publishDeviceInstanceUpdate(DeviceInstance newState,
                                             DeviceInstance oldState,
                                             DeviceInstanceUpdate.Type type) {
        String deviceTypeIdentifier = extractDeviceTypeIdentifier(newState, oldState);
        amqpTemplate.send(
            DeviceManagementExchangeConfiguration.NAME,
            DeviceManagementExchangeConfiguration
                .formatDeviceInstanceUpdateRoutingKey(deviceTypeIdentifier),
            buildMessage(new DeviceInstanceUpdate(newState, oldState, type))
        );
    }

    private void publishDeviceTypeUpdate(DeviceType newState,
                                         DeviceType oldState,
                                         DeviceTypeUpdate.Type type) {
        String deviceTypeIdentifier = extractDeviceTypeIdentifier(newState, oldState);
        amqpTemplate.send(
            DeviceManagementExchangeConfiguration.NAME,
            DeviceManagementExchangeConfiguration
                .formatDeviceTypeUpdateRoutingKey(deviceTypeIdentifier),
            buildMessage(new DeviceTypeUpdate(newState, oldState, type))
        );
    }

    private String extractDeviceTypeIdentifier(DeviceInstance newState, DeviceInstance oldState) {
        if (newState != null) {
            return newState.getDeviceTypeIdentifier();
        } else {
            return oldState.getDeviceTypeIdentifier();
        }
    }

    private String extractDeviceTypeIdentifier(DeviceType newState, DeviceType oldState) {
        if (newState != null) {
            return newState.getIdentifier();
        } else {
            return oldState.getIdentifier();
        }
    }

    private Message buildMessage(Object object) {
        try {
            return MessageBuilder
                .withBody(objectMapper.writeValueAsString(object).getBytes(StandardCharsets.UTF_8))
                .setContentEncoding(StandardCharsets.UTF_8.toString())
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .build();
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Error while serializing data to JSON.", e);
        }
    }
}
