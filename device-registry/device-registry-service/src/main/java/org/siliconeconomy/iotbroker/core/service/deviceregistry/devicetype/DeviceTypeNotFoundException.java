/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

/**
 * Thrown to indicate that a device type with a specific ID or a specific source and identifier
 * could not be found in the database.
 *
 * @author M. Grzenia
 */
public class DeviceTypeNotFoundException extends RuntimeException {

    public DeviceTypeNotFoundException(String id) {
        super(String.format("Could not find device type with ID '%s'.", id));
    }

    public DeviceTypeNotFoundException(String source, String identifier) {
        super(String.format(
            "Could not find device type with source '%s' and identifier '%s'.",
            source,
            identifier
        ));
    }
}
