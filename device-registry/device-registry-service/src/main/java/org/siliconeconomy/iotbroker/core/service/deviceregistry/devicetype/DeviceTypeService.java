/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.modelmapper.ModelMapper;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp.DeviceRegistryEventPublisher;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Implements the business logic for managing {@link DeviceType}s.
 *
 * @author M. Grzenia
 */
@Service
public class DeviceTypeService {

    /**
     * Provides access to the {@link DeviceTypeEntity} database.
     */
    private final DeviceTypeRepository deviceTypeRepository;
    /**
     * Provides access to the {@link DeviceInstanceEntity} database.
     */
    private final DeviceInstanceRepository deviceInstanceRepository;
    /**
     * Used to publish update events for {@link DeviceType}s.
     */
    private final DeviceRegistryEventPublisher deviceRegistryEventPublisher;
    /**
     * Used for mapping between different business objects.
     */
    private final ModelMapper modelMapper;

    @Autowired
    public DeviceTypeService(DeviceTypeRepository deviceTypeRepository,
                             DeviceInstanceRepository deviceInstanceRepository,
                             DeviceRegistryEventPublisher deviceRegistryEventPublisher,
                             ModelMapper modelMapper) {
        this.deviceTypeRepository = deviceTypeRepository;
        this.deviceInstanceRepository = deviceInstanceRepository;
        this.deviceRegistryEventPublisher = deviceRegistryEventPublisher;
        this.modelMapper = modelMapper;
    }

    /**
     * Creates a new {@link DeviceType} using the given {@link DeviceTypeCreateInputTO} by
     * adding a corresponding {@link DeviceTypeEntity} to the database.
     *
     * @param input The input to use for creating a new device type.
     * @return The created {@link DeviceType}.
     * @throws DeviceTypeAlreadyExistsException If a device type with the same source and
     *                                          identifier already exists.
     */
    public DeviceType create(DeviceTypeCreateInputTO input) {
        if (deviceTypeRepository.existsBySourceAndIdentifier(input.getSource(), input.getIdentifier())) {
            throw new DeviceTypeAlreadyExistsException(input.getSource(), input.getIdentifier());
        }

        // Map the transfer object to the entity to persist. This is where the entity gets its ID.
        DeviceTypeEntity entity = modelMapper.map(input, DeviceTypeEntity.class);

        DeviceTypeEntity persistedEntity = deviceTypeRepository.save(entity);

        // Publish an update event.
        DeviceType newType = modelMapper.map(persistedEntity, DeviceType.class);
        deviceRegistryEventPublisher.publishDeviceTypeUpdate(newType, null);

        return newType;
    }

    /**
     * Returns the device type with the given ID.
     *
     * @param id The ID of the device type.
     * @return The {@link DeviceType}.
     * @throws DeviceTypeNotFoundException If a device type with the given ID could not be found.
     */
    public DeviceType findById(String id) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            // The service persists entities with UUIDs as their ID. Therefore, there are no
            // entities with IDs other than UUIDs.
            throw new DeviceTypeNotFoundException(id);
        }

        DeviceTypeEntity entity = deviceTypeRepository.findById(uuid)
            .orElseThrow(() -> new DeviceTypeNotFoundException(id));

        return modelMapper.map(entity, DeviceType.class);
    }

    /**
     * Updates the device type with the given ID using the given input data.
     *
     * @param id    The ID of the device type.
     * @param input The input to use for updating the device type.
     * @return The updated {@link DeviceType}.
     * @throws DeviceTypeNotFoundException If a device type with the given source and identifier
     *                                     could not be found.
     */
    public DeviceType update(String id, DeviceTypeUpdateInputTO input) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            // The service persists entities with UUIDs as their ID. Therefore, there are no
            // entities with IDs other than UUIDs.
            throw new DeviceTypeNotFoundException(id);
        }

        DeviceTypeEntity existingEntity = deviceTypeRepository
            .findById(uuid)
            .orElseThrow(() -> new DeviceTypeNotFoundException(id));

        // We are working with Hibernate and managed entities. This means that after the save
        // operation we lose access to the state of the entity before the update (and only the
        // reference to the updated managed entity remains). Therefore, we need to map the entity
        // BEFORE we perform the save operation.
        DeviceType oldType = modelMapper.map(existingEntity, DeviceType.class);

        // Update the existing entity.
        DeviceTypeEntity updatedEntity
            = DeviceTypeUpdateInputConverter.convert(existingEntity, input);

        DeviceTypeEntity persistedEntity = deviceTypeRepository.save(updatedEntity);

        // Publish an update event.
        DeviceType newType = modelMapper.map(persistedEntity, DeviceType.class);
        deviceRegistryEventPublisher.publishDeviceTypeUpdate(newType, oldType);

        return newType;
    }


    /**
     * Deletes the device type with the given ID.
     *
     * @param id The ID of the device type.
     */
    public void deleteById(String id) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            // The service persists entities with UUIDs as their ID. Therefore, there are no
            // entities with IDs other than UUIDs.
            return;
        }

        Optional<DeviceTypeEntity> existingEntityOpt = deviceTypeRepository.findById(uuid);
        if (existingEntityOpt.isEmpty()) {
            // This avoids an EmptyResultDataAccessException when trying to perform the delete
            // operation in case there is no entity with the given ID.
            return;
        }

        DeviceTypeEntity existingEntity = existingEntityOpt.get();
        if (isReferencedByDeviceInstances(existingEntity)) {
            throw new DeviceTypeStillReferencedException(existingEntity.getSource(), existingEntity.getIdentifier());
        }

        // Publish an update event.
        DeviceType oldType = modelMapper.map(existingEntity, DeviceType.class);
        deviceRegistryEventPublisher.publishDeviceTypeUpdate(null, oldType);

        deviceTypeRepository.deleteById(uuid);
    }

    /**
     * Returns a list of device types.
     * <p>
     * Which elements are included in the list depends on the provided pagination information.
     *
     * @param source     A filter to retrieve only information about device types associated with a
     *                   specific source.
     * @param identifier A filter to retrieve only information about device types with a specific
     *                   identifier.
     * @param pageable   The pagination information to use when retrieving device types from the
     *                   database.
     * @return A list of {@link DeviceType}s.
     */
    public List<DeviceType> findAll(@Nullable String source,
                                    @Nullable String identifier,
                                    Pageable pageable) {
        Page<DeviceTypeEntity> page;
        if (source != null && identifier != null) {
            page = deviceTypeRepository.findBySourceAndIdentifier(source, identifier)
                .map(entity -> new PageImpl<>(List.of(entity)))
                .orElseGet(() -> new PageImpl<>(Collections.emptyList()));
        } else if (source != null) {
            page = deviceTypeRepository.findAllBySource(source, pageable);
        } else if (identifier != null) {
            page = deviceTypeRepository.findAllByIdentifier(identifier, pageable);
        } else {
            page = deviceTypeRepository.findAll(pageable);
        }

        return page.stream()
            .map(entity -> modelMapper.map(entity, DeviceType.class))
            .collect(Collectors.toList());
    }

    /**
     * Returns the total number of device types.
     *
     * @return The total number of device types.
     */
    public long count() {
        return deviceTypeRepository.count();
    }

    /**
     * Returns a set of all sources of device types.
     *
     * @return A set of all sources of device types.
     */
    public Set<String> findAllSources() {
        return deviceTypeRepository.findAllSources();
    }

    private boolean isReferencedByDeviceInstances(DeviceTypeEntity entity) {
        return deviceInstanceRepository.existsBySourceAndDeviceTypeIdentifier(
            entity.getSource(),
            entity.getIdentifier()
        );
    }
}
