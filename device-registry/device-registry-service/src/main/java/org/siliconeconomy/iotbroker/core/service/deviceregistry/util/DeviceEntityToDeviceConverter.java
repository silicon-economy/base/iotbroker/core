/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.util;

import org.modelmapper.AbstractConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.DeviceController;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.DeviceService;
import org.siliconeconomy.iotbroker.model.device.Device;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.DeviceEntity;

/**
 * Converts {@link DeviceEntity}s to {@link Device}s.
 *
 * @author M. Grzenia
 * @deprecated Will be removed with {@link DeviceController} and {@link DeviceService}.
 */
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceEntityToDeviceConverter extends AbstractConverter<DeviceEntity, Device> {

    @Override
    protected Device convert(DeviceEntity deviceEntity) {
        return new Device(deviceEntity.getSource(), deviceEntity.getTenant())
            .withType(deviceEntity.getType())
            .withModel(deviceEntity.getModel())
            .withDescription(deviceEntity.getDescription())
            .withHardwareRevision(deviceEntity.getHardwareRevision())
            .withFirmwareVersion(deviceEntity.getFirmwareVersion());
    }
}
