/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

/**
 * Thrown to indicate that a device instance with a specific ID could not be found in the database.
 *
 * @author M. Grzenia
 */
public class DeviceInstanceNotFoundException extends RuntimeException {

    public DeviceInstanceNotFoundException(String id) {
        super(String.format("Could not find device instance with ID '%s'.", id));
    }
}
