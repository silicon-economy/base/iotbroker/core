/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * Configuration of Spring MVC.
 * Enables CORS for the endpoints provided by the device registry service and adds the a
 * configurable set of URLs to the list of allowed origins.
 *
 * @author M. Grzenia
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${cors.allowedOrigin.urls}")
    private List<String> allowedOrigins;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/devices/**")
            .allowedOrigins(allowedOrigins.toArray(new String[]{}))
            .allowedMethods("*");
        registry.addMapping("/deviceInstances/**")
            .allowedOrigins(allowedOrigins.toArray(new String[]{}))
            .allowedMethods("*");
        registry.addMapping("/deviceTypes/**")
            .allowedOrigins(allowedOrigins.toArray(new String[]{}))
            .allowedMethods("*");
    }
}
