/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;

/**
 * Converts instances of {@link DeviceInstanceUpdateInputTO} to instances of
 * {@link DeviceInstanceEntity} by updating an existing instance of {@link DeviceInstanceEntity}.
 * <p>
 * The converted {@link DeviceInstanceEntity} first inherits all values from the existing
 * {@link DeviceInstanceEntity}. Then, all non-{@code null} values from the given
 * {@link DeviceInstanceUpdateInputTO} are be applied to the converted
 * {@link DeviceInstanceEntity}.
 *
 * @author M. Grzenia
 */
public class DeviceInstanceUpdateInputConverter {

    private DeviceInstanceUpdateInputConverter() {
    }

    public static DeviceInstanceEntity convert(DeviceInstanceEntity entity,
                                               DeviceInstanceUpdateInputTO update) {
        DeviceInstanceEntity result = entity;

        if (update.getDeviceTypeIdentifier() != null) {
            result = result.withDeviceTypeIdentifier(update.getDeviceTypeIdentifier());
        }
        if (update.getLastSeen() != null) {
            result = result.withLastSeen(update.getLastSeen());
        }
        if (update.getEnabled() != null) {
            result = result.withEnabled(update.getEnabled());
        }
        if (update.getDescription() != null) {
            result = result.withDescription(update.getDescription());
        }
        if (update.getHardwareRevision() != null) {
            result = result.withHardwareRevision(update.getHardwareRevision());
        }
        if (update.getFirmwareVersion() != null) {
            result = result.withFirmwareVersion(update.getFirmwareVersion());
        }

        return result;
    }
}
