/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import org.modelmapper.ModelMapper;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceService;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeService;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceAlreadyExistsException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.exception.DeviceNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.DeviceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.model.device.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides methods for performing database operations for the {@link DeviceEntity}.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstanceService} instead.
 */
@Service
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceService {

    /**
     * Classes handling the database access.
     */
    private final DeviceInstanceRepository deviceInstanceRepository;
    private final DeviceInstanceService deviceInstanceService;
    private final DeviceTypeRepository deviceTypeRepository;
    private final DeviceTypeService deviceTypeService;
    /**
     * The model mapper instance to use.
     */
    private final ModelMapper modelMapper;

    @Autowired
    public DeviceService(DeviceInstanceRepository deviceInstanceRepository,
                         DeviceInstanceService deviceInstanceService,
                         DeviceTypeRepository deviceTypeRepository,
                         DeviceTypeService deviceTypeService,
                         ModelMapper modelMapper) {
        this.deviceInstanceRepository = deviceInstanceRepository;
        this.deviceInstanceService = deviceInstanceService;
        this.deviceTypeRepository = deviceTypeRepository;
        this.deviceTypeService = deviceTypeService;
        this.modelMapper = modelMapper;
    }

    /**
     * Returns all devices.
     *
     * @param pageIndex    The page index to retrieve devices for.
     * @param pageSize     The size of a single page.
     * @param sortBySource The direction in which the retrieved devices are to be sorted according
     *                     to their source.
     * @param sortByTenant The direction in which the retrieved devices are to be sorted according
     *                     to their tenant.
     * @return A list of {@link Device}s.
     */
    public List<Device> findAll(int pageIndex,
                                int pageSize,
                                Sort.Direction sortBySource,
                                Sort.Direction sortByTenant) {
        Pageable pageable = PageRequest.of(
            pageIndex,
            pageSize,
            Sort.by(sortBySource, "source").and(Sort.by(sortByTenant, "tenant"))
        );
        return deviceInstanceService.findAll(null, null, pageable).stream()
            .map(entity -> modelMapper.map(entity, Device.class))
            .collect(Collectors.toList());
    }

    /**
     * Returns the number of devices.
     *
     * @return The number of devices.
     */
    public long count() {
        return deviceInstanceService.count();
    }

    /**
     * Returns all device sources.
     *
     * @return A set of device sources.
     */
    public Set<String> findAllSources() {
        return deviceInstanceService.findAllSources();
    }

    /**
     * Returns all devices that represent the given source.
     *
     * @param source       The source.
     * @param pageIndex    The page index to retrieve devices for.
     * @param pageSize     The size of a single page.
     * @param sortByTenant The direction in which the retrieved devices are to be sorted according
     *                     to their tenant.
     * @return A list of {@link Device}s.
     */
    public List<Device> findAllBySource(String source,
                                        int pageIndex,
                                        int pageSize,
                                        Sort.Direction sortByTenant) {
        Pageable pageable = PageRequest.of(
            pageIndex,
            pageSize,
            Sort.by(sortByTenant, "tenant")
        );
        return deviceInstanceService.findAll(source, null, pageable).stream()
            .map(entity -> modelMapper.map(entity, Device.class))
            .collect(Collectors.toList());
    }

    /**
     * Returns the device with the given source and tenant.
     *
     * @param source The source.
     * @param tenant The tenant.
     * @return The device with the given source and tenant.
     * @throws DeviceNotFoundException If a device with the given source and tenant could not be
     *                                 found.
     */
    public Device findBySourceAndTenant(String source, String tenant) {
        return deviceInstanceService.findAll(source, tenant, Pageable.unpaged())
            .stream()
            .findAny()
            .map(deviceInstance -> modelMapper.map(deviceInstance, Device.class))
            .orElseThrow(() -> new DeviceNotFoundException(source, tenant));
    }

    /**
     * Adds the given device to the database.
     *
     * @param device The device to add.
     * @return The added device.
     * @throws DeviceAlreadyExistsException If a device with the same source and tenant already
     *                                      exists.
     */
    public Device add(Device device) {
        if (deviceInstanceRepository.existsBySourceAndTenant(device.getSource(), device.getTenant())) {
            throw new DeviceAlreadyExistsException(device.getSource(), device.getTenant());
        }

        // Ensure that a proper device type exists.
        String deviceTypeIdentifier = generateDeviceTypeIdentifier(device);
        if (!deviceTypeRepository.existsBySourceAndIdentifier(device.getSource(), deviceTypeIdentifier)) {
            deviceTypeService.create(generateDeviceTypeCreateInput(device));
        }

        var deviceInstance = deviceInstanceService.create(generateDeviceInstanceCreateInput(device));

        return modelMapper.map(deviceInstance, Device.class);
    }

    /**
     * Updates the given device in the database.
     *
     * @param device The device to update.
     * @return The updated device.
     * @throws DeviceNotFoundException If a device with the given source and tenant could not be
     *                                 found.
     */
    public Device update(Device device) {
        DeviceInstanceEntity existingEntity = deviceInstanceRepository
            .findBySourceAndTenant(device.getSource(), device.getTenant())
            .orElseThrow(() -> new DeviceNotFoundException(device.getSource(), device.getTenant()));

        // In case the generated device type identifier changed, ensure that a proper device type
        // exists.
        String deviceTypeIdentifier = generateDeviceTypeIdentifier(device);
        if (!deviceTypeRepository.existsBySourceAndIdentifier(device.getSource(), deviceTypeIdentifier)) {
            deviceTypeService.create(generateDeviceTypeCreateInput(device));
        }

        var deviceInstance = deviceInstanceService
            .update(existingEntity.getId().toString(), generateDeviceInstanceUpdateInput(device));

        return modelMapper.map(deviceInstance, Device.class);
    }

    /**
     * Deletes the device with the given source and tenant.
     *
     * @param source The source.
     * @param tenant The tenant.
     */
    @Transactional
    public void deleteBySourceAndTenant(String source, String tenant) {
        Optional<DeviceInstanceEntity> entityOpt =
            deviceInstanceRepository.findBySourceAndTenant(source, tenant);
        if (entityOpt.isEmpty()) {
            return;
        }
        deviceInstanceRepository.deleteById(entityOpt.get().getId());
    }

    private String generateDeviceTypeIdentifier(Device device) {
        return String.format("%s_%s", device.getSource(), device.getFirmwareVersion());
    }

    private DeviceTypeCreateInputTO generateDeviceTypeCreateInput(Device device) {
        DeviceTypeCreateInputTO result = new DeviceTypeCreateInputTO()
            .setSource(device.getSource())
            .setIdentifier(generateDeviceTypeIdentifier(device));
        result.setDescription("Auto-generated device type for clients using the old DRS API.");
        return result;
    }

    private DeviceInstanceCreateInputTO generateDeviceInstanceCreateInput(Device device) {
        DeviceInstanceCreateInputTO result = new DeviceInstanceCreateInputTO()
            .setSource(device.getSource())
            .setTenant(device.getTenant())
            .setDeviceTypeIdentifier(generateDeviceTypeIdentifier(device));
        result.setDescription(device.getDescription())
            .setHardwareRevision(device.getHardwareRevision())
            .setFirmwareVersion(device.getFirmwareVersion());
        return result;
    }

    private DeviceInstanceUpdateInputTO generateDeviceInstanceUpdateInput(Device device) {
        DeviceInstanceUpdateInputTO result = new DeviceInstanceUpdateInputTO()
            .setDeviceTypeIdentifier(generateDeviceTypeIdentifier(device));
        result.setDescription(device.getDescription())
            .setHardwareRevision(device.getHardwareRevision())
            .setFirmwareVersion(device.getFirmwareVersion());
        return result;
    }
}
