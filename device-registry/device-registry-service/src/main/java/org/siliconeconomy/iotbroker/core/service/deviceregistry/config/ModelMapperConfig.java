/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.config;

import org.modelmapper.ModelMapper;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceCreateInputConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceEntityConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeCreateInputConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeEntityConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.util.DeviceEntityToDeviceConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.util.DeviceInstanceToDeviceConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration of the {@link ModelMapper} bean to use.
 *
 * @author M. Grzenia
 */
@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        var modelMapper = new ModelMapper();
        modelMapper.addConverter(new DeviceEntityToDeviceConverter());
        modelMapper.addConverter(new DeviceInstanceToDeviceConverter());
        modelMapper.addConverter(new DeviceInstanceCreateInputConverter());
        modelMapper.addConverter(new DeviceInstanceEntityConverter());
        modelMapper.addConverter(new DeviceTypeCreateInputConverter());
        modelMapper.addConverter(new DeviceTypeEntityConverter());
        return modelMapper;
    }
}
