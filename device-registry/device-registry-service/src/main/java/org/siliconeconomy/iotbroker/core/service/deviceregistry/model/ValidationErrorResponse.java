/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * A response for reporting validation errors to clients of the Device Registry Service's REST API.
 *
 * @author M. Grzenia
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ValidationErrorResponse {

    /**
     * A list of all validation errors that occurred during a request.
     */
    private List<ValidationError> validationErrors = new ArrayList<>();
}
