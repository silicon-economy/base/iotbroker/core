/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.persistence.MappedSuperclass;

/**
 * This DTO defines the base information about a device that is shared between various DTOs used in
 * this project to represent a device.
 *
 * @author M. Grzenia
 */
@Setter
@Getter
@EqualsAndHashCode
@ToString
@MappedSuperclass
@Deprecated
public abstract class DeviceDto {

    /**
     * A descriptive text for the device type. (E.g. Button, Tracker, etc.)
     * Optional: May be an empty string.
     */
    @NonNull
    private String type;
    /**
     * A descriptive text for the device model. (E.g. the device's code name.)
     * Optional: May be an empty string.
     */
    @NonNull
    private String model;
    /**
     * A description for the device.
     * Optional: May be an empty string.
     */
    @NonNull
    private String description;
    /**
     * The device's hardware revision.
     * Optional: May be an empty string.
     */
    @NonNull
    private String hardwareRevision;
    /**
     * The device's firmware version.
     * Optional: May be an empty string.
     */
    @NonNull
    private String firmwareVersion;

    protected DeviceDto() {
        this.type = "";
        this.model = "";
        this.description = "";
        this.hardwareRevision = "";
        this.firmwareVersion = "";
    }
}

