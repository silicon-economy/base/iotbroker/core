/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.DeviceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

/**
 * Interface used for database access for the {@link DeviceEntity}.
 * Will be generated and implemented by Spring at runtime and will then contain methods for all
 * CRUD operations.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstanceRepository} instead.
 */
@Repository
@Deprecated(since = "1.1.0", forRemoval = true)
public interface DeviceRepository extends JpaRepository<DeviceEntity, String> {

    /**
     * Returns all device sources in the database.
     *
     * @return A set of all device sources.
     */
    @Query(value = "select distinct source from DeviceEntity")
    Set<String> findAllSources();

    /**
     * Returns a {@link Page} of {@link DeviceEntity}s with the given source.
     *
     * @param source   The source.
     * @param pageable The pagination information.
     * @return A {@link Page} of {@link DeviceEntity}s.
     */
    Page<DeviceEntity> findAllBySource(String source, Pageable pageable);

    /**
     * Returns whether a {@link DeviceEntity} with the given source and tenant exists.
     *
     * @param source The source.
     * @param tenant The tenant.
     * @return {@code true}, if a {@link DeviceEntity} with the given source and tenant exists,
     * otherwise {@code false}.
     */
    boolean existsBySourceAndTenant(String source, String tenant);

    /**
     * Retrieves a {@link DeviceEntity} by its source and tenant.
     *
     * @param source The source.
     * @param tenant The tenant.
     * @return The {@link DeviceEntity} with the given source or tenant or {@link Optional#empty()}
     * if none is found.
     */
    Optional<DeviceEntity> findBySourceAndTenant(String source, String tenant);

    /**
     * Deletes the {@link DeviceEntity} with the given source and tenant.
     *
     * @param source The source.
     * @param tenant The tenant.
     */
    void deleteBySourceAndTenant(String source, String tenant);
}
