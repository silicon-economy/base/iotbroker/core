/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;

/**
 * This DTO is used to transfer device data for registering new and updating already registered
 * devices.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstanceCreateInputTO} and {@link DeviceInstanceUpdateInputTO}
 * instead.
 */
@Setter
@Getter
@EqualsAndHashCode
@ToString(callSuper = true)
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceInput extends DeviceDto {
}
