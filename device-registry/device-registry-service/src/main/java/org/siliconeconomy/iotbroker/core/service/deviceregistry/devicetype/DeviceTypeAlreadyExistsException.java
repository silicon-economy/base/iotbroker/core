/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

/**
 * Thrown to indicate that a device type with a specific source and identifier already exists in the
 * database.
 *
 * @author M. Grzenia
 */
public class DeviceTypeAlreadyExistsException extends RuntimeException {

    public DeviceTypeAlreadyExistsException(String source, String identifier) {
        super(String.format(
            "A device type with source '%s' and identifier '%s' already exists.",
            source,
            identifier
        ));
    }
}
