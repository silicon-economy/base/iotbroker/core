/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.With;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * A representation of a {@link DeviceType} for interaction with the database.
 * <p>
 * Where possible, documentation of private fields is omitted, since in most cases it is identical
 * to the corresponding documentation in {@link DeviceType}.
 *
 * @author M. Grzenia
 */
@AllArgsConstructor
@Getter
@With
@ToString
@Entity
@Table(name = "deviceTypes")
public class DeviceTypeEntity {

    /**
     * The unique identifier for the entity.
     * <p>
     * Unlike the ID field in {@link DeviceType}, which is a string, {@link UUID} is used as the
     * type here (and in the actual database).
     */
    @Id
    private final UUID id;
    @NonNull
    private final String source;
    @NonNull
    private final String identifier;
    @ElementCollection
    @CollectionTable(name = "deviceType_providedBy", joinColumns = @JoinColumn(name = "deviceType_id"))
    @NonNull
    private final Set<String> providedBy;
    @NonNull
    private final String description;
    private final boolean enabled;
    private final boolean autoRegisterDeviceInstances;
    private final boolean autoEnableDeviceInstances;

    /**
     * Creates a new instance.
     * <p>
     * This no-arg constructor is required for Spring Data JPA when retrieving data from the
     * database.
     */
    public DeviceTypeEntity() {
        this(null, "", "", new HashSet<>(), "", false, false, false);
    }
}
