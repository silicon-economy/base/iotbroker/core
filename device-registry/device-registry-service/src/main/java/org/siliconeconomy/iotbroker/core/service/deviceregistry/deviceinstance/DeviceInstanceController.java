/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.RequestExceptionHandler;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeNotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * A REST controller providing endpoints related to the management of {@link DeviceInstance}s.
 *
 * @author M. Grzenia
 */
@RestController
@RequestMapping("/deviceInstances")
public class DeviceInstanceController {

    /**
     * Provides methods for managing {@link DeviceInstance}s.
     */
    private final DeviceInstanceService service;

    @Autowired
    public DeviceInstanceController(DeviceInstanceService service) {
        this.service = service;
    }

    /**
     * Returns a list of registered devices instances.
     * <p>
     * Which elements are included in the list depends on the filter parameters, which can be
     * specified optionally, as well as on the pagination parameters.
     * <p>
     * If the provided pagination parameters are invalid, the request will be handled by
     * {@link RequestExceptionHandler#handleIllegalArgumentException(IllegalArgumentException)}.
     *
     * @param source       A filter to retrieve only information about device instances associated
     *                     with a specific source.
     * @param tenant       A filter to retrieve only information about device instances with a
     *                     specific tenant.
     * @param pageIndex    The page index to retrieve device instances for.
     * @param pageSize     The size of a single page.
     * @param sortBySource The direction in which the retrieved device instances are to be sorted
     *                     according to their source.
     * @param sortByTenant The direction in which the retrieved device instances are to be sorted
     *                     according to their tenant.
     * @return A list of {@link DeviceInstance}s.
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<DeviceInstance> fetchDeviceInstances(
        @RequestParam(required = false) String source,
        @RequestParam(required = false) String tenant,
        @RequestParam(defaultValue = "0") int pageIndex,
        @RequestParam(defaultValue = "10") int pageSize,
        @RequestParam(defaultValue = "ASC") String sortBySource,
        @RequestParam(defaultValue = "ASC") String sortByTenant) {
        Pageable pageable = PageRequest.of(
            pageIndex,
            pageSize,
            Sort.by(Sort.Direction.fromString(sortBySource), "source")
                .and(Sort.by(Sort.Direction.fromString(sortByTenant), "tenant"))
        );

        return service.findAll(source, tenant, pageable);
    }

    /**
     * Registers a device instance using the given input data.
     * <p>
     * If a device instance with the source and tenant contained in the given input data already
     * exists, the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceInstanceAlreadyExistsException(DeviceInstanceAlreadyExistsException)}.
     * <p>
     * If the device type identifier contained in the given input references an unknown device type,
     * the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceTypeNotFoundException(DeviceTypeNotFoundException)}.
     * <p>
     * If validation of the given input fails, the request will be handled by
     * {@link RequestExceptionHandler#handleMethodArgumentNotValidException(MethodArgumentNotValidException)}
     *
     * @param input The input to use for registering a new device instance.
     * @return The registered {@link DeviceInstance}.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DeviceInstance registerDeviceInstance(@Valid @RequestBody DeviceInstanceCreateInputTO input) {
        return service.create(input);
    }

    /**
     * Returns the total number of registered device instances.
     *
     * @return The total number of registered device instances.
     */
    @GetMapping("/count")
    @ResponseStatus(HttpStatus.OK)
    public long fetchDeviceInstancesCount() {
        return service.count();
    }

    /**
     * Returns a set of all registered sources of device instances.
     *
     * @return A set of all registered sources of device instances.
     */
    @GetMapping("/sources")
    @ResponseStatus(HttpStatus.OK)
    public Set<String> fetchDeviceInstanceSources() {
        return service.findAllSources();
    }

    /**
     * Returns the device instance with the given ID.
     * <p>
     * If a device instance with the given ID doesn't exist, the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceInstanceNotFoundException(DeviceInstanceNotFoundException)}.
     *
     * @param id The ID of the device instance.
     * @return The {@link DeviceInstance}.
     */
    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public DeviceInstance fetchDeviceInstance(@PathVariable String id) {
        return service.findById(id);
    }

    /**
     * Updates the device instance with the given ID using the given input data.
     * <p>
     * If a device instance with the given ID doesn't exist, the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceInstanceNotFoundException(DeviceInstanceNotFoundException)}.
     * <p>
     * If the device type identifier contained in the given input references an unknown device type,
     * the request will be handled by
     * {@link RequestExceptionHandler#handleDeviceTypeNotFoundException(DeviceTypeNotFoundException)}.
     *
     * @param id    The ID of the device instance.
     * @param input The input to use for updating the device instance.
     * @return The updated {@link DeviceInstance}.
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DeviceInstance updateDeviceInstance(@PathVariable String id,
                                               @RequestBody DeviceInstanceUpdateInputTO input) {
        return service.update(id, input);
    }

    /**
     * Deletes the device instance with the given ID.
     *
     * @param id The ID of the device instance.
     */
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteDeviceInstance(@PathVariable String id) {
        service.deleteById(id);
    }
}
