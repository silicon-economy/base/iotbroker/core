/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

import org.modelmapper.AbstractConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

/**
 * Converts instances of {@link DeviceInstanceEntity} to instances of {@link DeviceInstance}.
 *
 * @author M. Grzenia
 */
public class DeviceInstanceEntityConverter
    extends AbstractConverter<DeviceInstanceEntity, DeviceInstance> {

    @Override
    protected DeviceInstance convert(DeviceInstanceEntity entity) {
        return new DeviceInstance(
            entity.getId().toString(),
            entity.getSource(),
            entity.getTenant(),
            entity.getDeviceTypeIdentifier(),
            entity.getRegistrationTime(),
            entity.getLastSeen(),
            entity.isEnabled(),
            entity.getDescription(),
            entity.getHardwareRevision(),
            entity.getFirmwareVersion()
        );
    }
}
