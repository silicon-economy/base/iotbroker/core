/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.exception;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceAlreadyExistsException;

/**
 * Thrown to indicate that a device with a specific source and tenant already exists in the
 * database.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstanceAlreadyExistsException} instead.
 */
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceAlreadyExistsException extends RuntimeException {

    public DeviceAlreadyExistsException(String source, String tenant) {
        super(String.format(
            "Device with source '%s' and tenant '%s' already exists.",
            source,
            tenant
        ));
    }
}
