/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.util;

import org.modelmapper.AbstractConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.DeviceController;
import org.siliconeconomy.iotbroker.model.device.Device;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

/**
 * Converts {@link DeviceInstance}s to {@link Device}s.
 *
 * @author M. Grzenia
 * @deprecated This is a temporary converter to provide some sort of backwards compatibility when
 * working with the REST API provided by {@link DeviceController} and will be removed with the
 * aforementioned controller.
 */
@Deprecated(since = "1.1.0", forRemoval = true)
public class DeviceInstanceToDeviceConverter extends AbstractConverter<DeviceInstance, Device> {

    @Override
    protected Device convert(DeviceInstance deviceInstance) {
        return new Device(deviceInstance.getSource(), deviceInstance.getTenant())
            .withType("")
            .withModel("")
            .withDescription(deviceInstance.getDescription())
            .withHardwareRevision(deviceInstance.getHardwareRevision())
            .withFirmwareVersion(deviceInstance.getFirmwareVersion());
    }
}
