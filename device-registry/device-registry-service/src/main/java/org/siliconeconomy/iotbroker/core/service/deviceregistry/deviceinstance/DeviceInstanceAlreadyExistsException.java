/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance;

/**
 * Thrown to indicate that a device instance with a specific source and tenant already exists in the
 * database.
 *
 * @author M. Grzenia
 */
public class DeviceInstanceAlreadyExistsException extends RuntimeException {

    public DeviceInstanceAlreadyExistsException(String source, String tenant) {
        super(String.format(
            "A device instance with source '%s' and tenant '%s' already exists.",
            source,
            tenant
        ));
    }
}
