/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance;

import lombok.*;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.UUID;

/**
 * A representation of a {@link DeviceInstance} for interaction with the database.
 * <p>
 * Where possible, documentation of private fields is omitted, since in most cases it is identical
 * to the corresponding documentation in {@link DeviceInstance}.
 *
 * @author M. Grzenia
 */
@AllArgsConstructor
@Getter
@With
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "deviceInstances")
public class DeviceInstanceEntity {

    /**
     * The unique identifier for the entity.
     * <p>
     * Unlike the ID field in {@link DeviceInstance}, which is a string, {@link UUID} is used as the
     * type here (and in the actual database).
     */
    @Id
    private final UUID id;
    @NonNull
    private final String source;
    @NonNull
    private final String tenant;
    @NonNull
    private final String deviceTypeIdentifier;
    @NonNull
    private final Instant registrationTime;
    @NonNull
    private final Instant lastSeen;
    private final boolean enabled;
    @NonNull
    private final String description;
    @NonNull
    private final String hardwareRevision;
    @NonNull
    private final String firmwareVersion;

    /**
     * Creates a new instance.
     * <p>
     * This no-arg constructor is required for Spring Data JPA when retrieving data from the
     * database.
     */
    public DeviceInstanceEntity() {
        this(null, "", "", "", Instant.now(), Instant.EPOCH, false, "", "", "");
    }
}
