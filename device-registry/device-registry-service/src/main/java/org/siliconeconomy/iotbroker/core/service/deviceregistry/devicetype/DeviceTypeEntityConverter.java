/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

import org.modelmapper.AbstractConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Set;

/**
 * Converts instances of {@link DeviceTypeEntity} to instances of {@link DeviceType}.
 *
 * @author M. Grzenia
 */
public class DeviceTypeEntityConverter
    extends AbstractConverter<DeviceTypeEntity, DeviceType> {

    @Override
    protected DeviceType convert(DeviceTypeEntity entity) {
        return new DeviceType(
            entity.getId().toString(),
            entity.getSource(),
            entity.getIdentifier(),
            Set.copyOf(entity.getProvidedBy()),
            entity.getDescription(),
            entity.isEnabled(),
            entity.isAutoRegisterDeviceInstances(),
            entity.isAutoEnableDeviceInstances()
        );
    }
}
