/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.integration;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.deviceinstance.DeviceInstanceEntity;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.model.devicetype.DeviceTypeEntity;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.time.Instant;
import java.util.Collections;
import java.util.UUID;

/**
 * A factory that provides convenient methods for creating test data/objects.
 *
 * @author M. Grzenia
 */
public class TestDataFactory {

  private TestDataFactory() {
  }

  public static DeviceTypeCreateInputTO createInputWithDefaults(String source,
                                                                String identifier) {
    DeviceTypeCreateInputTO input = new DeviceTypeCreateInputTO()
        .setSource(source)
        .setIdentifier(identifier);
    input.setProvidedBy(Collections.emptySet())
        .setDescription("description")
        .setEnabled(false)
        .setAutoRegisterDeviceInstances(false)
        .setAutoEnableDeviceInstances(false);
    return input;
  }

  public static DeviceTypeUpdateInputTO updateInputWithDefaults() {
    DeviceTypeUpdateInputTO input = new DeviceTypeUpdateInputTO();
    input.setProvidedBy(Collections.emptySet())
        .setDescription("description")
        .setEnabled(false)
        .setAutoRegisterDeviceInstances(false)
        .setAutoEnableDeviceInstances(false);
    return input;
  }

  public static DeviceType deviceTypeWithDefaults(String id,
                                                  String source,
                                                  String identifier) {
    return new DeviceType(
        id,
        source,
        identifier,
        Collections.emptySet(),
        "description",
        false,
        false,
        false
    );
  }

  public static DeviceTypeEntity deviceTypeEntityWithDefaults(UUID id,
                                                              String source,
                                                              String identifier) {
    return new DeviceTypeEntity(
        id,
        source,
        identifier,
        Collections.emptySet(),
        "description",
        false,
        false,
        false
    );
  }

  public static DeviceInstanceCreateInputTO createInputWithDefaults(String source,
                                                                    String tenant,
                                                                    String deviceTypeIdentifier) {
    DeviceInstanceCreateInputTO input = new DeviceInstanceCreateInputTO()
        .setSource(source)
        .setTenant(tenant)
        .setDeviceTypeIdentifier(deviceTypeIdentifier);
    input.setLastSeen(Instant.EPOCH)
        .setEnabled(false)
        .setDescription("description")
        .setHardwareRevision("hardwareRevision")
        .setFirmwareVersion("firmwareVersion");
    return input;
  }

  public static DeviceInstanceUpdateInputTO updateInputWithDefaults(String deviceTypeIdentifier) {
    DeviceInstanceUpdateInputTO input = new DeviceInstanceUpdateInputTO()
        .setDeviceTypeIdentifier(deviceTypeIdentifier);
    input.setLastSeen(Instant.EPOCH)
        .setEnabled(false)
        .setDescription("description")
        .setHardwareRevision("hardwareRevision")
        .setFirmwareVersion("firmwareVersion");
    return input;
  }

  public static DeviceInstance deviceInstanceWithDefaults(String id,
                                                          String source,
                                                          String tenant,
                                                          String deviceTypeIdentifier) {
    return new DeviceInstance(
        id,
        source,
        tenant,
        deviceTypeIdentifier,
        Instant.EPOCH,
        Instant.EPOCH,
        false,
        "description",
        "hardwareRevision",
        "firmwareVersion"
    );
  }

  public static DeviceInstanceEntity deviceInstanceEntityWithDefaults(UUID id,
                                                                      String source,
                                                                      String tenant,
                                                                      String deviceTypeIdentifier) {
    return new DeviceInstanceEntity(
        id,
        source,
        tenant,
        deviceTypeIdentifier,
        Instant.EPOCH,
        Instant.EPOCH,
        false,
        "description",
        "hardwareRevision",
        "firmwareVersion"
    );
  }
}
