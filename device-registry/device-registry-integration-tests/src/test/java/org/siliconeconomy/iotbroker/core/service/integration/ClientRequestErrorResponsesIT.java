/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.integration;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.DeviceRegistryServiceApplication;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp.DeviceRegistryEventPublisher;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClientBuilder;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.BadRequestException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.ConflictException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions.NotFoundException;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit.RetrofitDeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.config.ModelMapperConfig;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.siliconeconomy.iotbroker.core.service.integration.TestDataFactory.createInputWithDefaults;
import static org.siliconeconomy.iotbroker.core.service.integration.TestDataFactory.updateInputWithDefaults;

/**
 * Integration tests related to the details of the web requests and responses of the
 * {@link RetrofitDeviceRegistryServiceClient}.
 * <p>
 * Especially testing unhappy paths and errors.
 *
 * @author C. Pionzewski
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"cors.allowedOrigin.urls=*"})
@ContextConfiguration(classes = DeviceRegistryServiceApplication.class)
@Import({ModelMapperConfig.class})
class ClientRequestErrorResponsesIT {

  @LocalServerPort
  private int port;

  private DeviceRegistryServiceClient client;
  @Autowired
  private DeviceTypeRepository deviceTypeRepository;
  @Autowired
  private DeviceInstanceRepository deviceInstanceRepository;

  /**
   * Test dependencies.
   * This field will be autowired into other services by Spring Boot.
   */
  @MockBean
  private DeviceRegistryEventPublisher deviceRegistryEventPublisher;

  @BeforeEach
  void createClient() {
    client = new DeviceRegistryServiceClientBuilder()
        .baseUrl("http://localhost:" + port)
        .build();
  }

  @AfterEach()
  void resetRepositories() {
    deviceTypeRepository.deleteAll();
    deviceInstanceRepository.deleteAll();
  }

  @Test
  void fetchDeviceType_whenDeviceTypeDoesNotExist_thenThrowNotFoundException() {
    assertThrows(NotFoundException.class, () -> client.fetchDeviceType("42"));
  }

  @Test
  void updateDeviceType_whenDeviceTypeDoesNotExist_thenThrowNotFoundException() {
    DeviceTypeUpdateInputTO deviceTypeUpdateInput = updateInputWithDefaults();
    assertThrows(NotFoundException.class,
                 () -> client.updateDeviceType("42", deviceTypeUpdateInput));
  }

  @Test
  void fetchDeviceInstance_whenDeviceInstanceDoesNotExist_thenThrowNotFoundException() {
    assertThrows(NotFoundException.class, () -> client.fetchDeviceInstance("42"));
  }

  @Test
  void updateDeviceInstance_whenDeviceInstanceDoesNotExist_thenThrowNotFoundException() {
    DeviceInstanceUpdateInputTO deviceInstanceUpdateInput
        = updateInputWithDefaults("deviceTypeIdentifier1");
    assertThrows(NotFoundException.class,
                 () -> client.updateDeviceInstance("42", deviceInstanceUpdateInput));
  }

  @Test
  void fetchDeviceTypes_whenMalformedArguments_thenThrowBadRequestException() {
    // Check valid request goes through.
    assertThat(client.fetchDeviceTypes(null,
                                       null,
                                       null,
                                       null,
                                       null,
                                       null).size()).isZero();

    // Check for errors.
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceTypes(null,
                                null,
                                -9999,
                                null,
                                null,
                                null));
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceTypes(null,
                                null,
                                null,
                                -1,
                                null,
                                null));
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceTypes(null,
                                null,
                                0,
                                -10,
                                null,
                                null));
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceTypes(null,
                                null,
                                10,
                                -1,
                                null,
                                null));
  }

  @Test
  void fetchDeviceInstances_whenMalformedArguments_thenThrowBadRequestException() {
    // Check valid requests go through.
    assertThat(client.fetchDeviceInstances(null,
                                           null,
                                           null,
                                           null,
                                           null,
                                           null).size()).isZero();

    // Check for errors.
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceInstances(null,
                                    null,
                                    -9999,
                                    null,
                                    null,
                                    null));
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceInstances(null,
                                    null,
                                    null,
                                    -1,
                                    null,
                                    null));
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceInstances(null,
                                    null,
                                    0,
                                    -10,
                                    null,
                                    null));
    assertThrows(BadRequestException.class, () ->
        client.fetchDeviceInstances(null,
                                    null,
                                    10,
                                    -1,
                                    null,
                                    null));
  }

  @Test
  void registerDeviceType_whenAlreadyExists_thenThrowConflictException() {
    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeInput
        = TestDataFactory.createInputWithDefaults("source1", "deviceTypeIdentifier1");
    client.createDeviceType(deviceTypeInput);

    // Check that it exists.
    assertThat(client.fetchDeviceTypesCount()).isOne();

    // Register the same device type and check that it throws the correct error.
    assertThrows(ConflictException.class, () -> client.createDeviceType(deviceTypeInput));
  }

  @Test
  void registerDeviceInstance_whenAlreadyExists_thenThrowConflictException() {
    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeInput
        = TestDataFactory.createInputWithDefaults("source1", "deviceTypeIdentifier1");
    client.createDeviceType(deviceTypeInput);

    // Register a device instance.
    DeviceInstanceCreateInputTO deviceInstanceInput
        = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
    client.createDeviceInstance(deviceInstanceInput);

    // Check that it exists.
    assertThat(client.fetchDeviceTypesCount()).isOne();
    assertThat(client.fetchDeviceInstancesCount()).isOne();

    // Register the same device instance and check that it throws the correct error.
    assertThrows(ConflictException.class, () -> client.createDeviceInstance(deviceInstanceInput));
  }

  @Test
  void registerDeviceInstance_whenCorrespondingDeviceTypeDoesNotExit_thenThrowNotFoundException() {
    // Register a device instance without creating its device type first.
    DeviceInstanceCreateInputTO deviceInstanceInput
        = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");

    // Check that the correct exception is thrown.
    assertThrows(NotFoundException.class, () -> client.createDeviceInstance(deviceInstanceInput));
  }

  @Test
  void registerDeviceType_whenInputIsInvalid_thenThrowBadRequestException() {
    // Create invalid device type inputs.
    DeviceTypeCreateInputTO deviceTypeInput
        = TestDataFactory.createInputWithDefaults("", "1");
    DeviceTypeCreateInputTO deviceTypeInput2
        = TestDataFactory.createInputWithDefaults("1", "");
    DeviceTypeCreateInputTO deviceTypeInput3
        = TestDataFactory.createInputWithDefaults("", "");

    // Check for each the exception is thrown.
    assertThrows(BadRequestException.class, () -> client.createDeviceType(deviceTypeInput));
    assertThrows(BadRequestException.class, () -> client.createDeviceType(deviceTypeInput2));
    assertThrows(BadRequestException.class, () -> client.createDeviceType(deviceTypeInput3));
  }

  @Test
  void registerDeviceInstance_whenInputIsInvalid_thenThrowBadRequestException() {
    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeInput
        = TestDataFactory.createInputWithDefaults("source1", "deviceTypeIdentifier1");
    client.createDeviceType(deviceTypeInput);

    // Create invalid device instance inputs.
    DeviceInstanceCreateInputTO deviceInstanceInput
        = createInputWithDefaults("", "", "");
    DeviceInstanceCreateInputTO deviceInstanceInput2
        = createInputWithDefaults("1", "", "");
    DeviceInstanceCreateInputTO deviceInstanceInput3
        = createInputWithDefaults("", "1", "");
    DeviceInstanceCreateInputTO deviceInstanceInput4
        = createInputWithDefaults("", "", "deviceTypeIdentifier1");

    // Check for each the exception is thrown.
    assertThrows(BadRequestException.class,
                 () -> client.createDeviceInstance(deviceInstanceInput));
    assertThrows(BadRequestException.class,
                 () -> client.createDeviceInstance(deviceInstanceInput2));
    assertThrows(BadRequestException.class,
                 () -> client.createDeviceInstance(deviceInstanceInput3));
    assertThrows(BadRequestException.class,
                 () -> client.createDeviceInstance(deviceInstanceInput4));
  }
}
