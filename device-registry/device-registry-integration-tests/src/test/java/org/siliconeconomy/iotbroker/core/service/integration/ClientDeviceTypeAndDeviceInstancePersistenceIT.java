/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.integration;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.DeviceRegistryServiceApplication;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.amqp.DeviceRegistryEventPublisher;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClientBuilder;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.retrofit.RetrofitDeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.config.ModelMapperConfig;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceRepository;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeRepository;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.siliconeconomy.iotbroker.core.service.integration.TestDataFactory.createInputWithDefaults;
import static org.siliconeconomy.iotbroker.core.service.integration.TestDataFactory.updateInputWithDefaults;

/**
 * Integration tests related to the persistence of data involving the
 * {@link RetrofitDeviceRegistryServiceClient}, {@link DeviceInstanceRepository} and
 * {@link DeviceTypeRepository}.
 * <p>
 * Closely related to the DeviceInstanceAndDeviceTypePersistence integration tests within the
 * Device Registry Service.
 *
 * @author C. Pionzewski
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"cors.allowedOrigin.urls=*"})
@ContextConfiguration(classes = DeviceRegistryServiceApplication.class)
@Import({ModelMapperConfig.class})
class ClientDeviceTypeAndDeviceInstancePersistenceIT {

  @LocalServerPort
  private int port;

  private DeviceRegistryServiceClient client;
  @Autowired
  private DeviceTypeRepository deviceTypeRepository;
  @Autowired
  private DeviceInstanceRepository deviceInstanceRepository;

  /**
   * Test dependencies.
   * This field will be autowired into other services by Spring Boot.
   */
  @MockBean
  private DeviceRegistryEventPublisher deviceRegistryEventPublisher;

  @BeforeEach
  void createClient() {
    client = new DeviceRegistryServiceClientBuilder()
        .baseUrl("http://localhost:" + port)
        .build();
  }

  @AfterEach()
  void resetRepositories() {
    deviceTypeRepository.deleteAll();
    deviceInstanceRepository.deleteAll();
  }

  @Test
  void shouldAccessDeviceTypeService() {
    assertThat(deviceTypeRepository.count()).isZero();
    long result = client.fetchDeviceTypesCount();
    assertThat(result).isZero();
  }

  @Test
  void shouldAccessDeviceInstanceService() {
    assertThat(deviceInstanceRepository.count()).isZero();
    long result = client.fetchDeviceInstancesCount();
    assertThat(result).isZero();
  }

  @Test
  void shouldRegisterDeviceType() {
    // Assert that the database is empty.
    assertThat(client.fetchDeviceTypesCount()).isZero();

    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeInput
        = TestDataFactory.createInputWithDefaults("source1", "deviceTypeIdentifier1");
    DeviceType registeredDeviceType = client.createDeviceType(deviceTypeInput);

    // Assert that the device type has been created.
    assertThat(client.fetchDeviceTypesCount()).isOne();
    assertThat(client.fetchDeviceType(registeredDeviceType.getId()))
        .extracting(DeviceType::getSource, DeviceType::getIdentifier)
        .containsExactly("source1", "deviceTypeIdentifier1");
  }

  @Test
  void shouldRegisterDeviceInstanceAndCorrespondingDeviceType() {
    // Assert that the databases are empty.
    assertThat(client.fetchDeviceInstancesCount()).isZero();
    assertThat(client.fetchDeviceTypesCount()).isZero();

    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeInput
        = TestDataFactory.createInputWithDefaults("source1", "deviceTypeIdentifier1");
    client.createDeviceType(deviceTypeInput);

    // Register a device instance.
    DeviceInstanceCreateInputTO deviceInstanceInput
        = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
    DeviceInstance registeredDeviceInstance = client.createDeviceInstance(deviceInstanceInput);

    // Assert that there now is a device instance.
    assertThat(client.fetchDeviceInstancesCount()).isOne();
    assertThat(client.fetchDeviceInstance(registeredDeviceInstance.getId()))
        .extracting(
            DeviceInstance::getSource,
            DeviceInstance::getTenant,
            DeviceInstance::getDeviceTypeIdentifier
        )
        .containsExactly("source1", "tenant1", "deviceTypeIdentifier1");
  }

  @Test
  void shouldUpdateDeviceInstanceRegistration() {
    // Assert that the databases are empty.
    assertThat(client.fetchDeviceInstancesCount()).isZero();
    assertThat(client.fetchDeviceTypesCount()).isZero();

    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeInput = createInputWithDefaults("source1",
                                                                      "deviceTypeIdentifier1");
    client.createDeviceType(deviceTypeInput);

    // Register a device instance.
    DeviceInstanceCreateInputTO deviceInstanceCreateInput
        = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
    DeviceInstance registeredDeviceInstance = client.createDeviceInstance(deviceInstanceCreateInput);

    // Update the device instance.
    DeviceInstanceUpdateInputTO deviceInstanceUpdateInput
        = updateInputWithDefaults("deviceTypeIdentifier1");
    deviceInstanceUpdateInput.setDescription("A new instance description.")
        .setEnabled(true)
        .setFirmwareVersion("v2.1.0");
    client.updateDeviceInstance(registeredDeviceInstance.getId(), deviceInstanceUpdateInput);

    // Assert that there is now an updated device instance.
    assertThat(client.fetchDeviceInstancesCount()).isOne();
    assertThat(client.fetchDeviceInstance(registeredDeviceInstance.getId()))
        .extracting(
            DeviceInstance::getSource,
            DeviceInstance::getTenant,
            DeviceInstance::getDeviceTypeIdentifier,
            DeviceInstance::getDescription,
            DeviceInstance::isEnabled,
            DeviceInstance::getFirmwareVersion
        )
        .containsExactly(
            "source1",
            "tenant1",
            "deviceTypeIdentifier1",
            "A new instance description.",
            true,
            "v2.1.0"
        );
  }

  @Test
  void shouldUpdateDeviceTypeRegistration() {
    // Assert that the database is empty.
    assertThat(client.fetchDeviceTypesCount()).isZero();

    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeCreateInput
        = createInputWithDefaults("source1", "deviceTypeIdentifier1");
    DeviceType registeredDeviceType = client.createDeviceType(deviceTypeCreateInput);

    // Update the device type.
    DeviceTypeUpdateInputTO deviceTypeUpdateInput = updateInputWithDefaults();
    deviceTypeUpdateInput.setDescription("A new type description.")
        .setAutoRegisterDeviceInstances(true)
        .setProvidedBy(Set.of("adapter1, adapter2"));
    client.updateDeviceType(registeredDeviceType.getId(), deviceTypeUpdateInput);

    // Assert that there is now an updated device type.
    assertThat(client.fetchDeviceTypesCount()).isOne();
    assertThat(client.fetchDeviceType(registeredDeviceType.getId()))
        .extracting(
            DeviceType::getSource,
            DeviceType::getIdentifier,
            DeviceType::getDescription,
            DeviceType::isAutoRegisterDeviceInstances,
            DeviceType::getProvidedBy
        )
        .containsExactly(
            "source1",
            "deviceTypeIdentifier1",
            "A new type description.",
            true,
            Set.of("adapter1, adapter2")
        );
  }

  @Test
  void shouldDeleteDeviceInstanceAndDeviceTypeRegistration() {
    // Assert that the databases are empty.
    assertThat(client.fetchDeviceInstancesCount()).isZero();
    assertThat(client.fetchDeviceTypesCount()).isZero();

    // Register a device type.
    DeviceTypeCreateInputTO deviceTypeInput
        = createInputWithDefaults("source1", "deviceTypeIdentifier1");
    DeviceType registeredDeviceType = client.createDeviceType(deviceTypeInput);

    // Register a device instance.
    DeviceInstanceCreateInputTO deviceInstanceInput
        = createInputWithDefaults("source1", "tenant1", "deviceTypeIdentifier1");
    DeviceInstance registeredDeviceInstance = client.createDeviceInstance(deviceInstanceInput);

    // Assert that there now is a device type and a device instance.
    assertThat(client.fetchDeviceTypesCount()).isOne();
    assertThat(client.fetchDeviceInstancesCount()).isOne();

    // Delete the device instance and the device type.
    client.deleteDeviceInstance(registeredDeviceInstance.getId());
    client.deleteDeviceType(registeredDeviceType.getId());

    // Assert that the databases are empty, again.
    assertThat(client.fetchDeviceTypesCount()).isZero();
    assertThat(client.fetchDeviceInstancesCount()).isZero();
  }

  @Test
  void shouldFindTheRequestedDeviceInstancesAndDeviceTypes() {
    // Assert that the databases are empty.
    assertThat(client.fetchDeviceInstancesCount()).isZero();
    assertThat(client.fetchDeviceTypesCount()).isZero();

    // Register device types and device instances.
    client.createDeviceType(createInputWithDefaults("source1", "deviceTypeIdentifier1"));
    client.createDeviceType(createInputWithDefaults("source2", "deviceTypeIdentifier2"));
    client.createDeviceType(createInputWithDefaults("source3", "deviceTypeIdentifier1"));
    client.createDeviceInstance(createInputWithDefaults("source1",
                                                        "tenant1",
                                                        "deviceTypeIdentifier1"));
    client.createDeviceInstance(createInputWithDefaults("source2",
                                                        "tenant2",
                                                        "deviceTypeIdentifier2"));

    // Assert that the correct device types are found.
    assertThat(client.fetchDeviceTypesCount()).isEqualTo(3);
    assertThat(client.fetchDeviceTypes("source2", "deviceTypeIdentifier2", null, null, null, null))
        .hasSize(1)
        .extracting("source", "identifier")
        .containsExactlyInAnyOrder(tuple("source2", "deviceTypeIdentifier2"));
    assertThat(client.fetchDeviceTypes("source1", null, null, null, null, null))
        .hasSize(1)
        .extracting("source", "identifier")
        .containsExactlyInAnyOrder(tuple("source1", "deviceTypeIdentifier1"));
    assertThat(client.fetchDeviceTypes(null, "deviceTypeIdentifier1", null, null, null, null))
        .hasSize(2)
        .extracting("source", "identifier")
        .containsExactlyInAnyOrder(
            tuple("source1", "deviceTypeIdentifier1"),
            tuple("source3", "deviceTypeIdentifier1")
        );
    assertThat(client.fetchDeviceTypes(null, null, null, null, null, null))
        .hasSize(3)
        .extracting("source", "identifier")
        .containsExactlyInAnyOrder(
            tuple("source1", "deviceTypeIdentifier1"),
            tuple("source2", "deviceTypeIdentifier2"),
            tuple("source3", "deviceTypeIdentifier1")
        );

    // Assert that the correct device instances are found.
    assertThat(client.fetchDeviceInstancesCount()).isEqualTo(2);
    assertThat(client.fetchDeviceInstances("source2", "tenant2", null, null, null, null))
        .hasSize(1)
        .extracting("source", "tenant")
        .containsExactlyInAnyOrder(tuple("source2", "tenant2"));
    assertThat(client.fetchDeviceInstances("source1", null, null, null, null, null))
        .hasSize(1)
        .extracting("source", "tenant")
        .containsExactlyInAnyOrder(tuple("source1", "tenant1"));
    assertThat(client.fetchDeviceInstances(null, "tenant2", null, null, null, null))
        .hasSize(1)
        .extracting("source", "tenant")
        .containsExactlyInAnyOrder(tuple("source2", "tenant2"));
    assertThat(client.fetchDeviceInstances(null, null, null, null, null, null))
        .hasSize(2)
        .extracting("source", "tenant")
        .containsExactlyInAnyOrder(
            tuple("source1", "tenant1"),
            tuple("source2", "tenant2")
        );
  }

  @Test
  void shouldFindAllDeviceInstanceAndDeviceTypeSources() {
    // Assert that the databases are empty.
    assertThat(client.fetchDeviceInstancesCount()).isZero();
    assertThat(client.fetchDeviceTypesCount()).isZero();

    // Register device types and device instances.
    client.createDeviceType(createInputWithDefaults("source1", "deviceTypeIdentifier1"));
    client.createDeviceType(createInputWithDefaults("source2", "deviceTypeIdentifier1"));
    client.createDeviceType(createInputWithDefaults("source2", "deviceTypeIdentifier2"));
    client.createDeviceType(createInputWithDefaults("source3", "deviceTypeIdentifier1"));
    client.createDeviceInstance(createInputWithDefaults("source1",
                                                        "tenant1",
                                                        "deviceTypeIdentifier1"));
    client.createDeviceInstance(createInputWithDefaults("source1",
                                                        "tenant2",
                                                        "deviceTypeIdentifier1"));
    client.createDeviceInstance(createInputWithDefaults("source2",
                                                        "tenant2",
                                                        "deviceTypeIdentifier2"));

    // Assert that all sources are found.
    assertThat(client.fetchDeviceTypesCount()).isEqualTo(4);
    assertThat(client.fetchDeviceTypesSources())
        .containsExactlyInAnyOrder("source1", "source2", "source3");
    assertThat(client.fetchDeviceInstancesCount()).isEqualTo(3);
    assertThat(client.fetchDeviceInstancesSources())
        .containsExactlyInAnyOrder("source1", "source2");
  }
}

