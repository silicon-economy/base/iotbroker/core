@startuml
top to bottom direction
'left to right direction
'skinparam linetype ortho
hide empty member

' Classes related to device instances
interface DeviceInstanceRepository << interface >> {
  +findAllSources(): Set<String>
  +findAllBySource(String, Pageable): Page<DeviceInstanceEntity>
  +findAllByTenant(String, Pageable): Page<DeviceInstanceEntity>
  +findBySourceAndTenant(String, String): Optional<DeviceInstanceEntity>
  +existsBySourceAndTenant(String, String): boolean
  +existsBySourceAndDeviceTypeIdentifier(String, String): boolean
}
class DeviceInstanceService {
  +create(DeviceInstanceCreateInputTO): DeviceInstance
  +findById(String): DeviceInstance
  +update(String, DeviceInstanceUpdateInputTO): DeviceInstance
  +deleteById(String): void
  +findAll(String?, String?, Pageable): List<DeviceInstance>
  +count(): long
  +findAllSources(): Set<String>
}
class DeviceInstanceController {
  +fetchDeviceInstances(String, String, int, int, String, String): List<DeviceInstance>
  +registerDeviceInstance(DeviceInstanceCreateInputTO): DeviceInstance
  +fetchDeviceInstancesCount(): long
  +fetchDeviceInstanceSources(): Set<String>
  +fetchDeviceInstance(String): DeviceInstance
  +updateDeviceInstance(String, DeviceInstanceUpdateInputTO): DeviceInstance
  +deleteDeviceInstance(String): void
}

' Classes related to device types
interface DeviceTypeRepository << interface >> {
  +findAllSources(): Set<String>
  +findAllBySource(String, Pageable): Page<DeviceTypeEntity>
  +findAllByIdentifier(String, Pageable): Page<DeviceTypeEntity>
  +findBySourceAndIdentifier(String, String): Optional<DeviceTypeEntity>
  +existsBySourceAndIdentifier(String, String): boolean
}
class DeviceTypeService {
  +create(DeviceTypeCreateInputTO): DeviceType
  +findById(String): DeviceType
  +update(String, DeviceTypeUpdateInputTO): DeviceType
  +deleteById(String): void
  +findAll(String?, String?, Pageable): List<DeviceType>
  +count(): long
  +findAllSources(): Set<String>
}
class DeviceTypeController {
  +fetchDeviceTypes(String, String, int, int, String, String): List<DeviceType>
  +registerDeviceType(DeviceTypeCreateInputTO): DeviceType
  +fetchDeviceTypesCount(): long
  +fetchDeviceTypeSources(): Set<String>
  +fetchDeviceType(String): DeviceType
  +updateDeviceType(String, DeviceTypeUpdateInputTO): DeviceType
  +deleteDeviceType(String): void
}

' Other classes
class DeviceRegistryEventPublisher {
  +initialize(): void
  +publishDeviceInstanceUpdate(DeviceInstance, DeviceInstance): void
  +publishDeviceTypeUpdate(DeviceType, DeviceType): void
}

' Relations
DeviceInstanceRepository <-- DeviceInstanceService
DeviceTypeRepository     <-- DeviceInstanceService
DeviceInstanceService    <-- DeviceInstanceController

DeviceInstanceRepository <-- DeviceTypeService
DeviceTypeRepository     <-- DeviceTypeService
DeviceTypeService        <-- DeviceTypeController

DeviceInstanceService --> DeviceRegistryEventPublisher
DeviceTypeService     --> DeviceRegistryEventPublisher

' Layout configuration
@enduml
