> This repository is no longer maintained.
> The IoT Broker and its core components (including the components in this repository) are now maintained in a single repository at: https://git.openlogisticsfoundation.org/silicon-economy/base/iotbroker/iotbroker

# IoT Broker core

The IoT Broker is a broker for IoT devices providing realtime and past data from and to devices.
It enables the integration of IoT devices, the encapsulation of device specifics (proprietary and binary protocols, data formats), the caching of communication and device data, as well as the conversion of IoT data into open formats (such as JSON or XML) as a basis for easy further processing of data.

This project provides components that are considered core components of the IoT Broker.
Core components provide basic IoT Broker functionality that is commonly used by other non-core components.
The core components include:

* Device Registry Service - Manages registration of IoT devices with the IoT Broker
* Sensor Data Persistence Service - Stores messages containing sensor data of devices published via AMQP to a database
* Sensor Data History Service - Provides access to messages containing sensor stored of devices in a database

## Documentation

The documentation for the IoT Broker as a whole is maintained in this project and contains sections describing the core components as well as non-core components.

For more details, please refer to the `documentation` directory.

## Git tags

The Git tags denominating versions used in this project do not correspond to tags for releases of individual components included in this project.
Instead, since the IoT Broker is composed of several components which are distributed across multiple repositories, the version numbers used with the tags refer to specific versions of the IoT Broker as a whole.
Thus, a tag marks a revision that belongs to the referenced version of the IoT Broker.
Therefore, the version numbers used with the tags do not necessarily match with the version numbers of the individual components.

## Continuous integration/Continuous deployment (CI/CD)

Development in this project facilitates CI/CD pipelines (e.g., for execution of builds and tests, deployment of build artifacts), tools for static code analysis, etc. currently on Fraunhofer internal infrastructure.

The corresponding deployment descriptors and pipeline scripts are provided as examples.

## Licenses of third-party dependencies

For information about licenses of third-party dependencies, please refer to the `README.md` files of the corresponding components.
