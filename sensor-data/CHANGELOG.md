# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.4] - 2022-08-29
### Fixed
- Fix the Docker build by skipping generation of `sources` and `javadoc` artifacts (which are not necessary for docker images).

## [1.1.3] - 2022-08-29
### Changed
- Adjust the project's Maven configuration to include `sources` and `javadoc` artifacts on deployment.

## [1.1.2] - 2022-08-29
### Changed
- Adjust the integration of commonly used configuration classes in the Sensor Data projects.
  The configuration classes are now explicitly imported whenever they are mandatory (depending on the requirements of the respective application).

### Fixed
- Fix property keys in `config/sensor-data-persistence-service-params.env` and `/config/sensor-data-subscription-service-params.env`, which lead to an unexpected behavior of the respective applications when running them via docker-compose.

## [1.1.1] - 2022-05-19
### Changed
- Update IoT Broker SDK to v5.5.0.

## [1.1.0] - 2022-05-03
### Added
- Implement the Sensor Data Subscription Service.
  The Sensor Data Subscription Service provides a live view of Sensor Data Messages and complements the Sensor Data History Service.
  For this, like the Sensor Data Persistence Service, the Sensor Data Subscription Service is connected to the AMQP broker and consumes messages published to the AMQP exchange designated for Sensor Data Messages.
  This first implementation supports subscriptions via STOMP over WebSocket.

## [1.0.5] - 2022-04-08
### Changed
- Adjust the OpenAPI specification of the Sensor Data History Service to correctly specify the contents of all types of locations.
  Additionally, add a missing definition for the datastream type `SimpleResultDatastream`.

## [1.0.4] - 2022-04-01
### Changed
- Update Spring Boot to v2.5.12.
  This Spring Boot version addresses CVE-2022-22965.

## [1.0.3] - 2022-01-21
### Changed
- Update Spring Boot to v2.5.9.
  This Spring Boot version includes Log4j v2.17.1, which fixes a number of vulnerabilities that have been reported in recent weeks.
  For more information on the reported vulnerabilities, see the following links:
  - CVE-2021-44228, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228
  - CVE-2021-45046, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046
  - CVE-2021-45105, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105
  - CVE-2021-44832, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832

## [1.0.2] - 2021-12-14
### Changed
- Update Log4j dependencies (especially `log4j-core`) to v2.15.0.
  A vulnerability has been reported with CVE-2021-44228 against the `log4j-core` jar and has been fixed in Log4j v2.15.0.
  Until Log4j v2.15.0 is picked up by Spring Boot, which is planned for the Spring Boot v2.5.8 & v2.6.2 releases (due Dec 23, 2021), the Log4j dependencies are overridden manually.
  For more information on CVE-2021-44228, see https://nvd.nist.gov/vuln/detail/CVE-2021-44228.

## [1.0.1] - 2021-11-17
### Added
- This CHANGELOG file to keep track of the changes in this project.

### Changed
- Adjust the style of license headers.
  Use the slash-star style to avoid dangling Javadoc comments.
  Using this style also prevents the license headers from being affected when reformatting code via the IntelliJ IDEA IDE.

## [1.0.0] - 2021-10-19
This is the first public release.

The Sensor Data Services provide functionality for working with sensor data of devices, and the Sensor Data Message format:

- The Sensor Data Persistence Service is responsible for storing messages containing sensor data of devices into a database.
- The Sensor Data History Service provides access to the Sensor Data Messages stored by the Sensor Data Persistence Service.

This release corresponds to IoT Broker v0.9.
