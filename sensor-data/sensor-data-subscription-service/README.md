# Sensor Data Subscription Service

This is the service for providing real time access to Sensor Data Message streams.

## Provided interfaces

The service provides the following types of interfaces that can be used to subscribe to different Sensor Data Message streams:

- Messaging over Websockets via [STOMP](https://stomp.github.io/).

### WebSocket

For message exchange over the WebSocket endpoints, [STOMP](https://stomp.github.io/) is used.

The URL for connecting to the service's WebSocket: `ws://<SERVICE_HOST>:<SERVICE_PORT>/websocket`

#### STOMP destinations

The following sections describe the STOMP destinations (sometimes also referred to as topics) to which the service publishes messages and client applications can subscribe.

##### [SUBSCRIBE] `/messages/sources/{source}/tenants/{tenant}`

Messages from a specific device with the given _source_ and _tenant_ are published to this destination.
Note that using the `*` wildcard is also supported.
For example, clients can subscribe to

* `/messages/sources/{source}/tenants/*` to receive messages from all devices with a specific source or
* `/messages/sources/*/tenants/*` to receive messages from all devices across all sources.

| Name | Type | Description | Accepted values |
|---|---|---|---|
| source | string | The source<sup>1</sup> that messages originated from. | _Any_ |
| tenant | string | The tenant<sup>1</sup> that messages originated from. | _Any_ |

<sup>1</sup> The Device Registry Service's REST API can be used to retrieve all available sources and tenants.

##### Payload

The payload is an array of messages according to the SensorDataMessage format<sup>2</sup> that is described in full detail in the [OpenAPI documentation](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/iotbroker/core/-/blob/main/sensor-data/sensor-data-history-service/openapi.yaml) of the Sensor Data History Service.

<sup>2</sup> Messages received via the WebSocket connection do not originate from any database, but from the AMQP.
Therefore, the _id_ field will always be empty/not filled, as this field gets filled by the Sensor Data Persistence Service.
The only purpose of the _id_ field is to have consistent domain models across all interfaces (including the REST API). \
The _originId_ field can be assigned by the adapters themselves and therefore may or may not be assigned.

**Payload example**

```json
[
  {
    "id": null,
    "originId": "sensingpuck-2413:123e4567-e89b-12d3-a456-426614174000",
    "source": "sensingpuck",
    "tenant": "2413",
    "timestamp": "2020-11-12T12:56:55Z",
    "location": {
      "name": "Tenant 1 location name",
      "description": "Tenant 1 location description",
      "encodingType": "SIMPLE",
      "details": "A comfy place."
    },
    "datastreams": [
      {
        "observedProperty": {
          "name": "Property 1",
          "description": "Property 1 description"
        },
        "observationType": "MEASUREMENT",
        "unitOfMeasurement": {
          "name": "milliwatt",
          "symbol": "mW"
        },
        "observations": [
          {
            "timestamp": "2020-11-12T12:56:55Z",
            "result": 3.142,
            "location": {
              "name": "Tenant 1 location name",
              "description": "Tenant 1 location description",
              "encodingType": "SIMPLE",
              "details": "A comfy place."
            },
            "parameters": {
              "paramKey": "paramValue"
            }
          }
        ]
      },
      {
        "observedProperty": {
          "name": "Property 1",
          "description": "Property 1 description"
        },
        "observationType": "TRUTH",
        "unitOfMeasurement": {
          "name": "milliwatt",
          "symbol": "mW"
        },
        "observations": [
          {
            "timestamp": "2020-11-12T12:56:55Z",
            "result": true,
            "location": {
              "name": "Tenant 1 location name",
              "description": "Tenant 1 location description",
              "encodingType": "SIMPLE",
              "details": "A comfy place."
            },
            "parameters": {
              "paramKey": "paramValue"
            }
          }
        ]
      }
    ]
  }
]
```

