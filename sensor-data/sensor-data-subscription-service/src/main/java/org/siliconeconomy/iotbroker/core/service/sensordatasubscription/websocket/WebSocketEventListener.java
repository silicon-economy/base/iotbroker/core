/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.*;

/**
 * Defines listeners for and handles WebSocket- and STOMP-related events.
 *
 * @author M. Grzenia
 */
// TODO: Add tests for this class once it implements some logic.
//  (E.g. when it is utilized to manage subscriptions.)
//  For now, this class is excluded from Sonar code coverage scans because it cannot be tested in
//  a meaningful way. (See the project's pom.xml.)
@Component
public class WebSocketEventListener {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(WebSocketEventListener.class);

  @EventListener
  public void onSessionConnectEvent(SessionConnectEvent event) {
    var header = StompHeaderAccessor.wrap(event.getMessage());
    LOG.debug("Client requesting connection... (sessionId={}, user={})",
              header.getSessionId(),
              event.getUser());
  }

  @EventListener
  public void onSessionConnectedEvent(SessionConnectedEvent event) {
    var header = StompHeaderAccessor.wrap(event.getMessage());
    LOG.debug("Client connected. (sessionId={}, user={})",
              header.getSessionId(),
              event.getUser());
  }

  @EventListener
  public void onSessionDisconnectEvent(SessionDisconnectEvent event) {
    var header = StompHeaderAccessor.wrap(event.getMessage());
    LOG.debug("Client disconnected. (sessionId={}, user={})",
              header.getSessionId(),
              event.getUser());
  }

  @EventListener
  public void onSessionSubscribeEvent(SessionSubscribeEvent event) {
    var header = StompHeaderAccessor.wrap(event.getMessage());
    LOG.debug("Client subscribed to '{}'. (sessionId={}, subscriptionId={}, user={})",
              header.getDestination(),
              header.getSessionId(),
              header.getSubscriptionId(),
              event.getUser());
  }

  @EventListener
  public void onSessionUnsubscribeEvent(SessionUnsubscribeEvent event) {
    var header = StompHeaderAccessor.wrap(event.getMessage());
    LOG.debug("Client unsubscribed from '{}'. (sessionId={}, subscriptionId={}, user={})",
              header.getDestination(),
              header.getSessionId(),
              header.getSubscriptionId(),
              event.getUser());
  }
}
