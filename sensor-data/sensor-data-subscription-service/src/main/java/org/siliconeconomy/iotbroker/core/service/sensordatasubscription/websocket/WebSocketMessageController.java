/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription.websocket;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.siliconeconomy.iotbroker.core.service.sensordatasubscription.config.WebSocketConfig.BROKER_PREFIX_SENSOR_DATA_MESSAGES;

/**
 * Controller for the subscription service's WebSocket interface.
 * <p>
 * The WebSocket integration uses the STOMP subprotocol to transmit {@link SensorDataMessage}s. This
 * controller takes care of publishing messages to a defined set of topics/destinations. A STOMP
 * broker then distributes the messages according to the client subscriptions.
 *
 * @author M. Grzenia
 */
@Controller
public class WebSocketMessageController {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(WebSocketMessageController.class);
  /**
   * The format of the destination/topic to send source-tenant-specific messages to.
   */
  private static final String SOURCE_TENANT_FORMAT
      = BROKER_PREFIX_SENSOR_DATA_MESSAGES + "/sources/%s/tenants/%s";
  /**
   * The template to use for sending messages via STOMP over WebSocket.
   */
  private final SimpMessagingTemplate messagingTemplate;

  @Autowired
  public WebSocketMessageController(SimpMessagingTemplate messagingTemplate) {
    this.messagingTemplate = messagingTemplate;
  }

  /**
   * Publishes the given messages by sending them to appropriate topics/destinations based on the
   * source and/or tenant from which the messages originate.
   *
   * @param messages The messages to publish.
   */
  public void publishMessages(List<SensorDataMessage> messages) {
    // The provided list might contain messages from different sources and/or tenants. Group these
    // messages so that we can publish them individually for each source-tenant-pair.
    Map<String, List<SensorDataMessage>> messagesBySource = messages.stream()
        .collect(Collectors.groupingBy(SensorDataMessage::getSource));
    for (Map.Entry<String, List<SensorDataMessage>> sourceEntry : messagesBySource.entrySet()) {
      Map<String, List<SensorDataMessage>> messagesByTenant = sourceEntry.getValue().stream()
          .collect(Collectors.groupingBy(SensorDataMessage::getTenant));
      for (Map.Entry<String, List<SensorDataMessage>> tenantEntry : messagesByTenant.entrySet()) {
        var destination = String.format(SOURCE_TENANT_FORMAT, sourceEntry.getKey(),
                                        tenantEntry.getKey());
        LOG.debug("Publishing {} message(s) to '{}'.", tenantEntry.getValue().size(), destination);
        messagingTemplate.convertAndSend(destination, tenantEntry.getValue());
      }
    }
  }
}
