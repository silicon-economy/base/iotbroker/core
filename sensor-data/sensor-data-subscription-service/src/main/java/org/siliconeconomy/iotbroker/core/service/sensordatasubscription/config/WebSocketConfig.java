/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription.config;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Configuration of the WebSocket that clients can use to subscribe to this service.
 *
 * @author L. Gebel
 * @author M. Grzenia
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig
    implements WebSocketMessageBrokerConfigurer {

  /**
   * The destination prefix that the STOMP broker uses for broadcasting {@link SensorDataMessage}s.
   */
  public static final String BROKER_PREFIX_SENSOR_DATA_MESSAGES = "/messages";
  /**
   * The origins for which cross-origin requests are allowed from a browser.
   */
  @Value("${cors.allowedOrigins}")
  private String[] allowedOrigins;

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    // Configure the endpoint(s) to which a WebSocket client needs to connect for the WebSocket
    // handshake.
    registry.addEndpoint("/websocket")
        .setAllowedOrigins(allowedOrigins);
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry config) {
    // Use the built-in message broker for subscriptions and broadcasting and route messages whose
    // destination header begins with one of the specified prefixes to the broker.
    config.enableSimpleBroker(BROKER_PREFIX_SENSOR_DATA_MESSAGES);
    // Configure prefixes to filter destinations targeting application annotated methods. This
    // results in STOMP messages whose destination header begins with the specified prefixes to be
    // routed to @MessageMapping methods in @Controller classes.
    config.setApplicationDestinationPrefixes("/app");
  }
}
