/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The application's starting point.
 * <p>
 * The Sensor Data Subscription Service provides a live view of messages containing sensor data of
 * devices and complements the Sensor Data History Service. For this, like the Sensor Data
 * Persistence Service, the Sensor Data Subscription Service connects to the AMQP broker and
 * consumes messages published to the AMQP exchange designated for {@link SensorDataMessage}s.
 * <p>
 * The service provides different interfaces that can be used to subscribe to
 * {@link SensorDataMessage} streams.
 *
 * @author M. Grzenia
 */
@SpringBootApplication
public class SensorDataSubscriptionServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(SensorDataSubscriptionServiceApplication.class, args);
  }
}
