/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription;

import org.siliconeconomy.iotbroker.core.service.sensordatasubscription.websocket.WebSocketMessageController;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.utils.amqp.SensorDataExchangeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Defines a listener for {@link SensorDataMessage}s published to the IoT Broker's AMQP broker.
 * <p>
 * Additionally, this listener notifies the different message controllers about incoming messages.
 *
 * @author M. Grzenia
 */
@Service
public class SensorDataMessageListener {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(SensorDataMessageListener.class);
  /**
   * The controller for the subscription service's WebSocket interface.
   */
  private final WebSocketMessageController webSocketMessageController;

  @Autowired
  public SensorDataMessageListener(WebSocketMessageController webSocketMessageController) {
    this.webSocketMessageController = webSocketMessageController;
  }

  /**
   * Callback for messages published to the AMQP exchange designated for {@link SensorDataMessage}s.
   * <p>
   * Notes on the listener configuration:
   * <ul>
   *   <li>To ensure that every instance of the subscription service receives all messages, a
   *   (unique) server-named queue is used for each instance.</li>
   *   <li>Additionally, non-durable and auto-delete queues are used for housekeeping purposes.</li>
   * </ul>
   * <p>
   * Note: The {@link SimpleMessageListenerContainer} that is used has batch processing of messages
   * enabled.
   *
   * @param messages The messages received via AMQP.
   */
  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(name = "", durable = "false", autoDelete = "true"),
          exchange = @Exchange(
              name = SensorDataExchangeConfiguration.NAME,
              type = SensorDataExchangeConfiguration.TYPE,
              durable = "true"
          ),
          key = SensorDataExchangeConfiguration.ROUTING_KEY
      )
  )
  public void onSensorDataMessages(List<SensorDataMessage> messages) {
    LOG.debug("Processing {} message(s).", messages.size());

    webSocketMessageController.publishMessages(messages);
  }
}
