/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link WebSocketConfig}.
 * <p>
 * Merely tests if the application context can be loaded properly and thus all beans can be
 * initialized successfully if all required configuration properties are provided.
 *
 * @author M. Grzenia
 */
@SpringBootTest(classes = {WebSocketConfig.class})
class WebSocketConfigTest {

  @Autowired
  private ApplicationContext context;

  @Test
  void contextLoads() {
    assertThat(context).isNotNull();
  }
}
