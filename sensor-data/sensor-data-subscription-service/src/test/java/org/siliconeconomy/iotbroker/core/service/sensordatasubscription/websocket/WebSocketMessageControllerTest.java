/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription.websocket;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Unit tests for {@link WebSocketMessageController}.
 *
 * @author M. Grzenia
 */
class WebSocketMessageControllerTest {

  /**
   * Class under test.
   */
  private WebSocketMessageController controller;
  /**
   * Test dependencies.
   */
  private SimpMessagingTemplate messagingTemplate;
  /**
   * Test environment.
   * <p>
   * Avoid warnings related capturing complex generic types by using the {@link Captor} annotation.
   * For consistency reasons, use the {@link Captor} annotation for other {@link ArgumentCaptor}s,
   * too.
   */
  @Captor
  private ArgumentCaptor<List<SensorDataMessage>> messagesCaptor;
  @Captor
  private ArgumentCaptor<String> destinationCaptor;
  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
    messagingTemplate = mock(SimpMessagingTemplate.class);
    controller = new WebSocketMessageController(messagingTemplate);
  }

  @AfterEach
  void tearDown()
      throws Exception {
    closeable.close();
  }

  @Test
  void publishMessages_whenSingleMessage_thenSendsOneMessages() {
    // Arrange
    List<SensorDataMessage> messages = List.of(createTestMessage("source1", "tenant1"));

    // Act
    controller.publishMessages(messages);

    // Assert & Verify
    verify(messagingTemplate, times(1))
        .convertAndSend(destinationCaptor.capture(), messagesCaptor.capture());

    assertThat(destinationCaptor.getAllValues())
        .hasSize(1)
        .containsExactly("/messages/sources/source1/tenants/tenant1");
    assertThat(messagesCaptor.getAllValues())
        .hasSize(1)
        .containsExactly(messages);
  }

  @Test
  void publishMessages_whenTwoMessagesFromSameSourceAndTenant_thenSendsOneMessage() {
    // Arrange
    SensorDataMessage message1 = createTestMessage("source1", "tenant1");
    SensorDataMessage message2 = createTestMessage("source1", "tenant1");
    List<SensorDataMessage> messages = List.of(message1, message2);

    // Act
    controller.publishMessages(messages);

    // Assert & Verify
    verify(messagingTemplate, times(1))
        .convertAndSend(destinationCaptor.capture(), messagesCaptor.capture());

    assertThat(destinationCaptor.getAllValues())
        .hasSize(1)
        .containsExactly("/messages/sources/source1/tenants/tenant1");
    assertThat(messagesCaptor.getAllValues())
        .hasSize(1)
        .containsExactly(messages);
  }

  @Test
  void publishMessages_whenTwoMessagesFromSameSourceButDifferentTenants_thenSendsTwoMessages() {
    // Arrange
    SensorDataMessage message1 = createTestMessage("source1", "tenant1");
    SensorDataMessage message2 = createTestMessage("source1", "tenant2");
    List<SensorDataMessage> messages = List.of(message1, message2);

    // Act
    controller.publishMessages(messages);

    // Assert & Verify
    verify(messagingTemplate, times(2))
        .convertAndSend(destinationCaptor.capture(), messagesCaptor.capture());

    assertThat(destinationCaptor.getAllValues()).hasSize(2);
    assertThat(messagesCaptor.getAllValues()).hasSize(2);

    Map<String, List<SensorDataMessage>> messagesByDestination
        = groupMessagesByDestinations(destinationCaptor.getAllValues(),
                                      messagesCaptor.getAllValues());
    assertThat(messagesByDestination)
        .hasSize(2)
        .contains(entry("/messages/sources/source1/tenants/tenant1", List.of(message1)),
                  entry("/messages/sources/source1/tenants/tenant2", List.of(message2)));
  }

  @Test
  void publishMessages_whenTwoMessagesFromDifferentSources_thenSendsTwoMessages() {
    // Arrange
    SensorDataMessage message1 = createTestMessage("source1", "tenant1");
    SensorDataMessage message2 = createTestMessage("source2", "tenant1");
    List<SensorDataMessage> messages = List.of(message1, message2);

    // Act
    controller.publishMessages(messages);

    // Assert & Verify
    verify(messagingTemplate, times(2))
        .convertAndSend(destinationCaptor.capture(), messagesCaptor.capture());

    assertThat(destinationCaptor.getAllValues()).hasSize(2);
    assertThat(messagesCaptor.getAllValues()).hasSize(2);

    Map<String, List<SensorDataMessage>> messagesByDestination
        = groupMessagesByDestinations(destinationCaptor.getAllValues(),
                                      messagesCaptor.getAllValues());
    assertThat(messagesByDestination)
        .hasSize(2)
        .contains(entry("/messages/sources/source1/tenants/tenant1", List.of(message1)),
                  entry("/messages/sources/source2/tenants/tenant1", List.of(message2)));
  }

  private Map<String, List<SensorDataMessage>> groupMessagesByDestinations(
      List<String> destinations, List<List<SensorDataMessage>> messages) {
    assertEquals(destinations.size(), messages.size());
    Map<String, List<SensorDataMessage>> messagesByDestination = new HashMap<>();
    for (int i = 0; i < destinationCaptor.getAllValues().size(); i++) {
      messagesByDestination.put(destinationCaptor.getAllValues().get(i),
                                messagesCaptor.getAllValues().get(i));
    }

    return messagesByDestination;
  }

  SensorDataMessage createTestMessage(String source, String tenant) {
    return new SensorDataMessage("",
                                 "",
                                 source,
                                 tenant,
                                 Instant.EPOCH,
                                 null,
                                 List.of());
  }
}
