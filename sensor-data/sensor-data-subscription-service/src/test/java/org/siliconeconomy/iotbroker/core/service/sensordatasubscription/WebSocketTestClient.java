/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.converter.SimpleMessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.lang.reflect.Type;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A test emulating a consuming WebSocket client.
 * <p>
 * This test creates a WebSocket client which connects to a configurable WebSocket URL and then
 * subscribes to a configurable set of STOMP destinations/topics. The test runs indefinitely until
 * the WebSocket connection is closed.
 * <p>
 * Reason for ignoring java:S3577: This is not a traditional test class and should only be executed
 * manually. To emphasize this, naming conventions intentionally don't apply here.
 *
 * @author M. Grzenia
 */
@SuppressWarnings("java:S3577")
class WebSocketTestClient {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(WebSocketTestClient.class);
  /**
   * The WebSocket URL to connect to.
   */
  private static final String WEBSOCKET_URL = "ws://localhost:8080/websocket";
  /**
   * The STOMP destinations/topics to subscribe to.
   */
  private static final Set<String> DESTINATIONS_TO_SUBSCRIBE = Set.of(
      "/messages/sources/*/tenants/*",
      "/messages/sources/source/tenants/tenant"
  );
  /**
   * The executor service merely used to kill some time.
   */
  private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
  /**
   * Whether the WebSocket client is connected.
   */
  private volatile boolean connected = false;

  @Test
  @Disabled("Available Sensor Data Subscription Service required. Use for manual tests only.")
  void startTestClient() {
    WebSocketClient webSocketClient = new StandardWebSocketClient();
    WebSocketStompClient stompClient = new WebSocketStompClient(webSocketClient);
    StompSessionHandler sessionHandler = new SimpleStompSessionHandler();
    stompClient.setMessageConverter(new SimpleMessageConverter());
    stompClient.connect(WEBSOCKET_URL, sessionHandler);

    waitForClientToConnect();
    assertThat(connected).isTrue();

    waitForConnectionToClose();
    assertThat(connected).isFalse();
  }

  private void waitForClientToConnect() {
    LOG.info("Waiting for the client to connect...");
    ScheduledFuture<?> future = executor.schedule(() -> {}, 5, TimeUnit.SECONDS);
    while (!future.isDone()) {
      Thread.onSpinWait();
    }
  }

  private void waitForConnectionToClose() {
    while (connected) {
      Thread.onSpinWait();
    }
    LOG.info("Connection closed.");
  }

  public class SimpleStompSessionHandler
      extends StompSessionHandlerAdapter {

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
      connected = true;
      LOG.info("Connected to the WebSocket server.");
      for (String topic : DESTINATIONS_TO_SUBSCRIBE) {
        session.subscribe(topic, this);
      }
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
      return Object.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
      LOG.info("Received message at '{}': {}",
               headers.getDestination(),
               new String((byte[]) payload));
    }

    @Override
    public void handleException(StompSession session,
                                StompCommand command,
                                StompHeaders headers,
                                byte[] payload,
                                Throwable exception) {
      LOG.error("Error while processing a STOMP frame.", exception);
      connected = false;
    }

    @Override
    public void handleTransportError(StompSession session, Throwable exception) {
      LOG.error("A low level transport error occured.", exception);
      connected = false;
    }
  }
}
