/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.sensordatasubscription.websocket.WebSocketMessageController;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

import java.time.Instant;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for {@link SensorDataMessageListener}.
 *
 * @author M. Grzenia
 */
class SensorDataMessageListenerTest {

  /**
   * Class under test.
   */
  private SensorDataMessageListener sensorDataMessageListener;
  /**
   * Test dependencies.
   */
  private WebSocketMessageController webSocketMessageController;

  @BeforeEach
  void setUp() {
    webSocketMessageController = mock(WebSocketMessageController.class);
    sensorDataMessageListener = new SensorDataMessageListener(webSocketMessageController);
  }

  @Test
  void onSensorDataMessages_whenMessagesIncoming_thenForwardsMessagesToControllers() {
    // Arrange
    List<SensorDataMessage> messages = generateTestMessages();

    // Act
    sensorDataMessageListener.onSensorDataMessages(messages);

    // Verify
    verify(webSocketMessageController).publishMessages(messages);
  }

  List<SensorDataMessage> generateTestMessages() {
    return List.of(
        new SensorDataMessage("1", "1", "source1", "tenant1", Instant.EPOCH, null, List.of()),
        new SensorDataMessage("2", "2", "source1", "tenant1", Instant.EPOCH, null, List.of()),
        new SensorDataMessage("3", "3", "source1", "tenant2", Instant.EPOCH, null, List.of())
    );
  }
}
