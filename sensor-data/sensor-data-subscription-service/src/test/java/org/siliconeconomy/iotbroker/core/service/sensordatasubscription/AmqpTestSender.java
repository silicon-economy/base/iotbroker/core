/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.config.ObjectMapperConfig;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.utils.amqp.SensorDataExchangeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A test for sending test {@link SensorDataMessage}s to an AMQP broker.
 * <p>
 * This test connects to an AMQP broker and uses an {@link AmqpTemplate} to send
 * {@link SensorDataMessage}s to the AMQP exchange designated for {@link SensorDataMessage}s.
 * <p>
 * Reason for ignoring java:S3577: This is not a traditional test class and should only be executed
 * manually. To emphasize this, naming conventions intentionally don't apply here.
 *
 * @author L. Gebel
 * @author M. Grzenia
 */
@SuppressWarnings("java:S3577")
class AmqpTestSender {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(AmqpTestSender.class);
  /**
   * The host of the RabbitMQ to connect to.
   */
  private static final String RABBITMQ_HOST = "localhost";
  /**
   * The port of the RabbitMQ to connect to.
   */
  private static final int RABBITMQ_PORT = 5672;
  /**
   * The username to use to connect to the RabbitMQ.
   */
  private static final String RABBITMQ_USERNAME = "guest";
  /**
   * The password to use to connect to the RabbitMQ.
   */
  private static final String RABBITMQ_PASSWORD = "guest";
  /**
   * The object mapper used to serialize {@link SensorDataMessage}s.
   */
  private final ObjectMapper objectMapper = new ObjectMapperConfig().objectMapper();

  @Test
  @Disabled("Available and properly configured AMQP broker required. Use for manual tests only.")
  void startTestSender() {
    AmqpTemplate amqpTemplate = setupAmqpTemplate();

    SensorDataMessage message = createDummyMessage("source", "tenant");

    boolean success = false;
    try {
      amqpTemplate.convertAndSend(
          SensorDataExchangeConfiguration.NAME,
          SensorDataExchangeConfiguration.ROUTING_KEY,
          objectMapper.writeValueAsString(message).getBytes(StandardCharsets.UTF_8)
      );
      success = true;
    } catch (Exception e) {
      LOG.error("Error while sending message.", e);
    }

    assertThat(success).isTrue();
  }

  private AmqpTemplate setupAmqpTemplate() {
    var connectionFactory = new CachingConnectionFactory();
    connectionFactory.setHost(RABBITMQ_HOST);
    connectionFactory.setPort(RABBITMQ_PORT);
    connectionFactory.setUsername(RABBITMQ_USERNAME);
    connectionFactory.setPassword(RABBITMQ_PASSWORD);
    return new RabbitTemplate(connectionFactory);
  }

  private SensorDataMessage createDummyMessage(String source, String tenant) {
    return new SensorDataMessage(UUID.randomUUID().toString(),
                                 "",
                                 source,
                                 tenant,
                                 Instant.now(),
                                 null,
                                 List.of());
  }
}
