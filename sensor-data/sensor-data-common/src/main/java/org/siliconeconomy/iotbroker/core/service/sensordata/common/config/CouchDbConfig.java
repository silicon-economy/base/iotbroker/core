/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.ObjectMapperFactory;
import org.siliconeconomy.iotbroker.utils.couchdb.PartitionedCouchDbConnector;
import org.siliconeconomy.iotbroker.utils.couchdb.PartitionedCouchDbInstance;
import org.siliconeconomy.iotbroker.utils.couchdb.StdPartitionedCouchDbConnector;
import org.siliconeconomy.iotbroker.utils.couchdb.StdPartitionedCouchDbInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.MalformedURLException;

/**
 * Spring {@link Configuration} to configure CouchDB connections and client.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Configuration
public class CouchDbConfig {

    public static final String MESSAGE_DB_DESIGN_DOC_NAME = "messages";
    /**
     * The URL to the CouchDB instance.
     */
    @Value("${couchdb.url}")
    private String url;
    /**
     * Username of the CouchDB instance.
     */
    @Value("${couchdb.username}")
    private String username;
    /**
     * Password for the user of the CouchDB instance.
     */
    @Value("${couchdb.password}")
    private String password;
    /**
     * Database name in which to store the messages.
     */
    @Value("${couchdb.databasename.messages}")
    private String messageDatabaseName;

    /**
     * Configures and provides the HTTP client used for connecting to CouchDB.
     */
    @Bean
    public HttpClient client() throws MalformedURLException {
        return new StdHttpClient.Builder()
            .url(url)
            .username(username)
            .password(password)
            // Disabling caching in order to use `initStandardDesignDocument()`
            // see: https://github.com/helun/Ektorp/issues/222#issuecomment-379267999
            .caching(false)
            .build();
    }

    /**
     * Provides the {@link CouchDbInstance} used for communication with CouchDB instance.
     */
    @Bean
    public PartitionedCouchDbInstance dbInstance(HttpClient httpClient) {
        return new StdPartitionedCouchDbInstance(httpClient);
    }

    @Bean
    public ObjectMapper viewQueryObjectMapper() {
        return new ObjectMapper()
            // When querying views, empty (JSON) objects can be used for the (start/end)key. This
            // configuration allows simple instances of java.lang.Object to be mapped to those empty
            // JSON objects.
            .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .registerModule(new JavaTimeModule());
    }

    /**
     * The {@link ObjectMapperFactory} to use with Ektorp.
     */
    @Bean
    public ObjectMapperFactory mapperFactory(ObjectMapper objectMapper) {
        return new ObjectMapperFactory() {
            @Override
            public ObjectMapper createObjectMapper() {
                return objectMapper;
            }

            @Override
            public ObjectMapper createObjectMapper(CouchDbConnector couchDbConnector) {
                return objectMapper;
            }
        };
    }

    @Bean
    public PartitionedCouchDbConnector messageDatabase(PartitionedCouchDbInstance dbInstance,
                                                       ObjectMapperFactory mapperFactory) {
        return new StdPartitionedCouchDbConnector(messageDatabaseName, dbInstance, mapperFactory);
    }
}
