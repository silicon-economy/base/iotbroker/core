/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.util;

import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

import java.util.regex.Pattern;

/**
 * A collection of utility methods for working with {@link SensorDataMessage}s and
 * {@link SensorDataMessageEntity}s.
 *
 * @author D. Ronnenberg
 */
public class SensorDataMessageUtils {

    static final int MIN_CHARS_SOURCE_PART = 1;
    static final int MAX_CHARS_SOURCE_PART = 32;
    static final int MIN_CHARS_TENANT_PART = 1;
    static final int MAX_CHARS_TENANT_PART = 64;
    static final int MIN_CHARS_MESSAGE_ID_PART = 1;
    static final int MAX_CHARS_MESSAGE_ID_PART = 64;
    /**
     * The format to use for generating IDs for {@link SensorDataMessageEntity} instances.
     * Corresponds to {@link #ENTITY_ID_PATTERN}.
     * The format is as follows: {@code <source>-<tenant>:<messageId>}.
     */
    private static final String ENTITY_ID_FORMAT = "%s-%s:%s";
    /**
     * The pattern to match IDs of {@link SensorDataMessageEntity} instances.
     * Corresponds to {@link #ENTITY_ID_FORMAT}.
     * <p>
     * The constraints in this pattern depend on the part of the ID:
     * <ul>
     *     <li>The 'source'-part (i.e. first part) must have at least {@link #MIN_CHARS_SOURCE_PART}
     *     and at most {@link #MAX_CHARS_SOURCE_PART} characters.</li>
     *     <li>The 'tenant'-part (i.e. second part) must have at least
     *     {@link #MIN_CHARS_TENANT_PART} and at most {@link #MAX_CHARS_TENANT_PART} characters.</li>
     *     <li>The 'messageId'-part (i.e. third part) must have at least
     *     {@link #MAX_CHARS_MESSAGE_ID_PART} and at most {@link #MAX_CHARS_MESSAGE_ID_PART}
     *     characters.</li>
     * </ul>
     */
    private static final Pattern ENTITY_ID_PATTERN = Pattern.compile(
        String.format("(.{%d,%d}-.{%d,%d}:)(.{%d,%d})",
            MIN_CHARS_SOURCE_PART,
            MAX_CHARS_SOURCE_PART,
            MIN_CHARS_TENANT_PART,
            MAX_CHARS_TENANT_PART,
            MIN_CHARS_MESSAGE_ID_PART,
            MAX_CHARS_MESSAGE_ID_PART
        )
    );

    private SensorDataMessageUtils() {
    }

    /**
     * Generates a unique ID that can be used for a {@link SensorDataMessageEntity} that is based
     * of the given {@link SensorDataMessage}.
     *
     * @param message The original message to generate an entity ID for.
     * @return A unique ID that can be used for a {@link SensorDataMessageEntity}.
     */
    public static String generateEntityId(SensorDataMessage message) {
        var formattedId = String.format(
            ENTITY_ID_FORMAT,
            message.getSource(),
            message.getTenant(),
            message.getId()
        );

        // check if the generated id is valid
        var entityIdMatcher = ENTITY_ID_PATTERN.matcher(formattedId);
        if (!entityIdMatcher.matches()) {
            throw new IllegalArgumentException(
                String.format("Generated ID '%s' doesn't match pattern '%s' for message: %s",
                    formattedId,
                    ENTITY_ID_PATTERN,
                    message
                )
            );
        }

        return formattedId;
    }

    /**
     * Extracts the part in the given {@link SensorDataMessageEntity}'s ID that represents the ID
     * of the original {@link SensorDataMessage} (i.e. the 'messageId'-part).
     *
     * @param entity The entity to extract the message ID from.
     * @return The extracted message ID.
     */
    public static String extractMessageId(SensorDataMessageEntity entity) {
        var entityIdMatcher = ENTITY_ID_PATTERN.matcher(entity.getId());
        if (!entityIdMatcher.matches()) {
            throw new IllegalArgumentException(
                String.format("Unexpected entity ID format '%s' (doesn't match pattern '%s')",
                    entity.getId(),
                    ENTITY_ID_PATTERN
                )
            );
        }

        return entityIdMatcher.group(2);
    }
}
