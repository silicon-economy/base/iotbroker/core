/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.*;

import java.time.Instant;
import java.util.Collection;

/**
 * This DTO is used to interact with the database.
 * <p>
 * It extends {@link SensorDataMessage} by providing additional (JSON) fields required for use with
 * CouchDB.
 * <p>
 * Note: The IDs of entities in the database adhere to a specific pattern and in some way 'extend'
 * the IDs of the original messages. Because of this, the {@link SensorDataMessage}'s {@code id}
 * field is 'reused' and (for a {@link SensorDataMessageEntity} instance) does not contain the ID of
 * the original message, but the 'extended' ID. Moreover, the serialization and deserialization of
 * the {@code id} field is overwritten to be able to use it with the database.
 *
 * @author D. Ronnenberg
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString(callSuper = true)
public class SensorDataMessageEntity extends SensorDataMessage {

    /**
     * A unique identifier for the revision of the document inside the database. Automatically set
     * by the client.
     */
    @JsonProperty("_rev")
    private String revision;

    /**
     * Creates a new instance.
     *
     * @param source      The source that the device represents.
     * @param tenant      A unique identifier for the device.
     * @param timestamp   The time the device sent the data.
     * @param location    The (optional) location of the device.
     * @param datastreams The list of datastreams.
     */
    @JsonCreator
    public SensorDataMessageEntity(
        @JsonProperty("_id") @NonNull String id,
        @JsonProperty("_rev") String revision,
        @JsonProperty("originId") @NonNull String originId,
        @JsonProperty("source") @NonNull String source,
        @JsonProperty("tenant") @NonNull String tenant,
        @JsonProperty("timestamp") @NonNull Instant timestamp,
        @JsonProperty("location") Location<? extends LocationDetails<?>> location,
        @JsonProperty("datastreams") @NonNull Collection<Datastream<? extends ObservationResult<?>>> datastreams) {
        super(id, originId, source, tenant, timestamp, location, datastreams);

        this.revision = revision;
    }

    @Override
    @JsonProperty("_id")
    public @NonNull String getId() {
        return super.getId();
    }
}
