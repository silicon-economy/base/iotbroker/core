/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for a connection to the AMQP broker.
 * <p>
 * This configuration enables support for batch processing of messages (which is disabled by
 * default).
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Configuration
public class RabbitMqBatchConfig {

  /**
   * Timeout after which a partial batch is delivered to {@link @RabbitListener}s if no new messages
   * arrive.
   */
  @Value("${rabbitmq.receiveTimeout}")
  private long receiveTimeout;
  /**
   * The maximum number of messages that should be batched. When this limit is reached, the entire
   * batch is passed to {@link @RabbitListener}s (if they are configured to process batches
   * accordingly).
   */
  @Value("${rabbitmq.batchSize}")
  private int batchSize;

  /**
   * Configures the {@link RabbitListenerContainerFactory} to enable batch processing of messages.
   *
   * @param connectionFactory The connection factory.
   * @param objectMapper      The object mapper to use for serializing/deserializing
   *                          {@link SensorDataMessage}s.
   * @return The configured bean.
   */
  @Bean
  public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
      ConnectionFactory connectionFactory,
      @Qualifier("objectMapper") ObjectMapper objectMapper) {
    // A properly configured message converter is required for deserialization of AMQP messages to
    // Sensor Data Message instances. We could define the message converter as a bean and Spring
    // would 'automagically' set it, but this way it's more explicit. Also, for methods annotated
    // with @RabbitListener, this way we don't have to explicitly specify the message converter to
    // use.
    var messageConverter = new Jackson2JsonMessageConverter(objectMapper);

    var factory = new SimpleRabbitListenerContainerFactory();
    factory.setConnectionFactory(connectionFactory);
    factory.setBatchListener(true);
    factory.setConsumerBatchEnabled(true);
    factory.setDeBatchingEnabled(true);
    factory.setBatchSize(batchSize);
    factory.setReceiveTimeout(receiveTimeout);
    factory.setMessageConverter(messageConverter);

    return factory;
  }
}
