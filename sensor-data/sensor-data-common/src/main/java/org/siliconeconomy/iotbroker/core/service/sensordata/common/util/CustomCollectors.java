/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Provides additional/custom {@link Collector} implementations that are not available in
 * {@link Collectors}.
 *
 * @author M. Grzenia
 */
public class CustomCollectors {

    private CustomCollectors() {
    }

    /**
     * Returns a {@link Collector} that accumulates elements into a {@link LinkedHashMap} whose keys
     * and values are the result of applying the provided mapping functions to the input elements.
     * <p>
     * The order of the elements in the returned map is the order in which keys were inserted into
     * it. When iterating over an ordered collection and using this collector, this means that the
     * returned map will preserve/reflect the order of the collection.
     * <p>
     * If the mapped keys contain duplicates (according to {@link Object#equals(Object)}), an
     * {@link IllegalStateException} is thrown when the collection operation is performed.
     *
     * @param keyMapper   The mapping function to produce keys.
     * @param valueMapper The mapping function to produce values.
     * @param <T>         The type of the input elements.
     * @param <K>         The output type of the key mapping function.
     * @param <V>         The output type of the value mapping function.
     * @return A {@link Collector} that accumulates elements into a {@link LinkedHashMap} whose keys
     * and values are the result of applying the provided mapping functions to the input elements.
     */
    public static <T, K, V> Collector<T, ?, Map<K, V>> toLinkedHashMap(
        Function<? super T, ? extends K> keyMapper,
        Function<? super T, ? extends V> valueMapper) {
        return Collectors.toMap(keyMapper, valueMapper, uniqueKeysMerger(), LinkedHashMap::new);
    }

    private static final <V> BinaryOperator<V> uniqueKeysMerger() {
        return (existing, replacement) -> {
            throw new IllegalStateException(
                String.format("Duplicate key. Attempted to merge values '%s' and '%s'",
                    existing,
                    replacement)
            );
        };
    }
}
