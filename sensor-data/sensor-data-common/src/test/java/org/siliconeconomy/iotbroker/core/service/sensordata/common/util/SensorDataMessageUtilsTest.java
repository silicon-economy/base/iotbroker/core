/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.util;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test cases for {@link SensorDataMessageUtils}.
 *
 * @author D. Ronnenberg
 */
class SensorDataMessageUtilsTest {

    @Test
    void generateEntityId() {
        // Arrange
        var message = new SensorDataMessage(
            "someId",
            "someOriginId",
            "someSource",
            "someTenant",
            Instant.EPOCH,
            null,
            List.of()
        );

        // Act
        var id = SensorDataMessageUtils.generateEntityId(message);

        // Assert
        assertThat(id).isEqualTo(message.getSource() + "-" + message.getTenant() + ":" + message.getId());
    }

    @Test
    void generateEntityId_invalidInput() {
        // Arrange
        var messageWithNoId = new SensorDataMessage(
            "",
            "someOriginId",
            "someSource",
            "someTenant",
            Instant.EPOCH,
            null,
            List.of()
        );
        var messageWithNoSource = new SensorDataMessage(
            "someId",
            "someOriginId",
            "",
            "someTenant",
            Instant.EPOCH,
            null,
            List.of()
        );
        var messageWithNoTenant = new SensorDataMessage(
            "someId",
            "someOriginId",
            "someSource",
            "",
            Instant.EPOCH,
            null,
            List.of()
        );
        var messageWithTooLongId = new SensorDataMessage(
            "x".repeat(SensorDataMessageUtils.MAX_CHARS_MESSAGE_ID_PART + 1),
            "someOriginId",
            "someSource",
            "someTenant",
            Instant.EPOCH,
            null,
            List.of()
        );
        var messageWithTooLongSource = new SensorDataMessage(
            "someId",
            "someOriginId",
            "x".repeat(SensorDataMessageUtils.MAX_CHARS_SOURCE_PART + 1),
            "someTenant",
            Instant.EPOCH,
            null,
            List.of()
        );
        var messageWithTooLongTenant = new SensorDataMessage(
            "someId",
            "someOriginId",
            "someSource",
            "x".repeat(SensorDataMessageUtils.MAX_CHARS_TENANT_PART + 1),
            Instant.EPOCH,
            null,
            List.of()
        );

        // Act & Assert
        // min bounds violation
        assertThatThrownBy(() -> SensorDataMessageUtils.generateEntityId(messageWithNoId))
            .isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> SensorDataMessageUtils.generateEntityId(messageWithNoSource))
            .isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> SensorDataMessageUtils.generateEntityId(messageWithNoTenant))
            .isInstanceOf(IllegalArgumentException.class);

        // max bounds violation
        assertThatThrownBy(() -> SensorDataMessageUtils.generateEntityId(messageWithTooLongId))
            .isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> SensorDataMessageUtils.generateEntityId(messageWithTooLongSource))
            .isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> SensorDataMessageUtils.generateEntityId(messageWithTooLongTenant))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void extractMessageId() {
        // Arrange
        var entity = new SensorDataMessageEntity(
            "someSource-someTenant:36a83ab4-cf8f-416a-b226-801af1080913",
            null,
            "someOriginId",
            "someSource",
            "someTenant",
            Instant.EPOCH,
            null,
            List.of()
        );

        // Act
        String result = SensorDataMessageUtils.extractMessageId(entity);

        // Assert
        assertThat(result).isEqualTo("36a83ab4-cf8f-416a-b226-801af1080913");
    }

    @Test
    void extractMessageId_invalidEntityIdPattern() {
        // Arrange
        var entity = new SensorDataMessageEntity(
            "some-invalid-pattern",
            null,
            "someOriginId",
            "someSource",
            "someTenant",
            Instant.EPOCH,
            null,
            List.of()
        );

        // Act & Assert
        assertThatThrownBy(() -> SensorDataMessageUtils.extractMessageId(entity))
            .isInstanceOf(IllegalArgumentException.class);
    }
}
