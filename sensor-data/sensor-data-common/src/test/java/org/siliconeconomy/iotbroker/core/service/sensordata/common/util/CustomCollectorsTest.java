/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.*;

/**
 * Test cases for {@link CustomCollectors}.
 *
 * @author M. Grzenia
 */
class CustomCollectorsTest {

    @Test
    void toLinkedHashMap() {
        // Arrange
        List<String> inputElements = Arrays.asList("wolf", "pie", "onion", "by");

        // Act
        Map<String, Integer> result = inputElements.stream()
            .collect(CustomCollectors.toLinkedHashMap(Function.identity(), String::length));

        // Assert
        assertThat(result)
            .hasSize(4)
            .containsExactly(
                entry("wolf", 4),
                entry("pie", 3),
                entry("onion", 5),
                entry("by", 2)
            );
    }

    @Test
    void toLinkedHashMap_duplicateKeyException() {
        // Arrange
        List<String> inputElements = Arrays.asList("shark", "bed", "bed", "cotton");

        var inputStream = inputElements.stream();
        var collectFunction = CustomCollectors.toLinkedHashMap(Function.identity(), String::length);

        // Act & Assert
        assertThatThrownBy(() -> inputStream.collect(collectFunction))
            .isInstanceOf(IllegalStateException.class)
            .hasMessageContaining("Duplicate key.");
    }
}
