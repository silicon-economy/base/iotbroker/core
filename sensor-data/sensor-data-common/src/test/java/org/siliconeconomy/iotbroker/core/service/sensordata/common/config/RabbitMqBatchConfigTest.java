/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link RabbitMqBatchConfig}.
 * <p>
 * Merely tests if the application context can be loaded properly and thus all beans can be
 * initialized successfully if all required configuration properties are provided.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@SpringBootTest(classes = {RabbitMqBatchConfig.class})
@TestPropertySource(
    properties = {
        "rabbitmq.receiveTimeout=1000",
        "rabbitmq.batchSize=1000"
    }
)
class RabbitMqBatchConfigTest {

  @MockBean
  private ConnectionFactory connectionFactory;
  @MockBean(name = "objectMapper")
  private ObjectMapper objectMapper;
  @Autowired
  private ApplicationContext context;

  @Test
  void contextLoads() {
    assertThat(context).isNotNull();
  }
}
