/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.config;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link CouchDbConfig}.
 * <p>
 * Merely tests if the application context can be loaded properly and thus all beans can be
 * initialized successfully if all required configuration properties are provided.
 *
 * @author M. Grzenia
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CouchDbConfig.class})
@TestPropertySource(
    properties = {
        "couchdb.url=http://localhost:5984",
        "couchdb.username=username",
        "couchdb.password=password",
        "couchdb.databasename.messages=database"
    }
)
class CouchDbConfigTest {

    @Autowired
    private ApplicationContext context;

    @Test
    void contextLoads() {
        assertThat(context).isNotNull();
    }
}
