/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatapersistence;

import com.rabbitmq.client.Channel;
import org.ektorp.CouchDbConnector;
import org.ektorp.DocumentOperationResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link SensorDataMessageListener}.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
class SensorDataMessageListenerTest {

    /**
     * Class under test.
     */
    private SensorDataMessageListener sensorDataMessageListener;
    /**
     * Test dependencies.
     */
    private CouchDbConnector dbConnector;
    private Channel channel;

    @BeforeEach
    public void init() {
        dbConnector = mock(CouchDbConnector.class);
        channel = mock(Channel.class);
        sensorDataMessageListener = new SensorDataMessageListener(dbConnector);
    }

    @Test
    void processMessage_AllOk() throws IOException {
        // Arrange
        var messages = generateTestMessages();
        when(dbConnector.executeBulk(anyCollection())).thenReturn(new ArrayList<>());

        // Act
        sensorDataMessageListener.processMessage(getRabbitMessages(messages), channel);

        // Assert & Verify
        verify(channel, times(1)).basicAck(5L, true);
        verify(channel, times(0)).basicNack(anyLong(), eq(false), eq(false));
    }

    @Test
    void processMessage_EveryRequestFails() throws IOException {
        // Arrange
        var messages = generateTestMessages();
        var failedResponses = generateFailedResponses(messages);
        when(dbConnector.executeBulk(anyCollection())).thenReturn(failedResponses);

        // Act
        sensorDataMessageListener.processMessage(getRabbitMessages(messages), channel);

        // Assert & Verify
        verify(channel, times(0)).basicAck(anyLong(), eq(true));
        verify(channel, times(5)).basicNack(anyLong(), eq(false), eq(false));
    }

    @Test
    void processMessage_SingleRequestFails() throws IOException {
        // Arrange
        var messages = generateTestMessages();
        var failedResponses = generateFailedResponses(messages.get(4));
        when(dbConnector.executeBulk(anyCollection())).thenReturn(failedResponses);

        // Act
        sensorDataMessageListener.processMessage(getRabbitMessages(messages), channel);

        // Assert & Verify
        verify(channel, times(1)).basicAck(4L, true);
        verify(channel, times(1)).basicNack(5L, false, false);
    }

    @Test
    void partitionByPersistedEntities() {
        // Arrange
        var messageEntities = List.of(
            new SensorDataMessageEntity("source1-tenant1:1", null, "1", "source1", "tenant1",
                Instant.EPOCH, null, List.of()),
            new SensorDataMessageEntity("source1-tenant1:2", null, "1", "source1", "tenant1",
                Instant.EPOCH, null, List.of()),
            new SensorDataMessageEntity("source1-tenant1:3", null, "1", "source1", "tenant1",
                Instant.EPOCH, null, List.of())
        );
        var deliveryTagsByEntity = Map.of(
            messageEntities.get(0), 42L,
            messageEntities.get(1), 43L,
            messageEntities.get(2), 44L
        );
        var bulkResults = List.of(
            DocumentOperationResult.newInstance(messageEntities.get(1).getId(), "failed", null)
        );

        // Act
        Map<Boolean, Map<SensorDataMessageEntity, Long>> persistedEntities
            = sensorDataMessageListener.partitionByPersistedEntities(deliveryTagsByEntity, bulkResults);

        // Assert
        assertThat(persistedEntities.get(Boolean.TRUE))
            .hasSize(2)
            .containsKeys(messageEntities.get(0), messageEntities.get(2));
        assertThat(persistedEntities.get(Boolean.FALSE))
            .hasSize(1)
            .containsKeys(messageEntities.get(1));
    }

    @Test
    void convertToDeliveryTagsByEntity() {
        // Arrange
        var message = new SensorDataMessage("1", "1", "source1", "tenant1", Instant.EPOCH, null,
            List.of());
        var rabbitMessage = new GenericMessage<>(message, Map.of(AmqpHeaders.DELIVERY_TAG, 42L));

        // Act
        var deliveryTagsByEntity
            = sensorDataMessageListener.convertToDeliveryTagsByEntity(List.of(rabbitMessage));

        // Assert
        SensorDataMessageEntity expectedEntity = new SensorDataMessageEntity(
            "source1-tenant1:1",
            null,
            "1",
            "source1",
            "tenant1",
            Instant.EPOCH,
            null,
            List.of()
        );
        assertThat(deliveryTagsByEntity)
            .hasSize(1)
            .containsExactly(entry(expectedEntity, 42L));
    }

    @Test
    void nackMessages_RejectionFails() throws IOException {
        // Arrange
        var messageEntity = new SensorDataMessageEntity("source1-tenant1:1", null, "1", "source1",
            "tenant1", Instant.EPOCH, null, List.of());
        var bulkResults = List.of(
            DocumentOperationResult.newInstance(messageEntity.getId(), "failed", null)
        );
        var deliveryTagsByEntity = Map.of(messageEntity, 42L);
        doThrow(new IOException("Nack failed"))
            .when(channel).basicNack(anyLong(), anyBoolean(), anyBoolean());

        // Act
        boolean errorOccurred = sensorDataMessageListener.nackMessages(deliveryTagsByEntity, channel, bulkResults);

        // Assert & Verify
        assertTrue(errorOccurred);
        verify(channel, times(1)).basicNack(42L, false, false);
    }

    @Test
    void ackMessages_AcknowledgementFails() throws IOException {
        // Arrange
        var messageEntity = new SensorDataMessageEntity("source1-tenant1:1", null, "1", "source1",
            "tenant1", Instant.EPOCH, null, List.of());
        var deliveryTagsByEntity = Map.of(messageEntity, 42L);
        doThrow(new IOException("Ack failed")).when(channel).basicAck(anyLong(), anyBoolean());

        // Act
        boolean errorOccurred = sensorDataMessageListener.ackMessages(deliveryTagsByEntity, channel);

        // Assert & Verify
        assertTrue(errorOccurred);
        verify(channel, times(1)).basicAck(42L, true);
    }

    // helper functions
    List<DocumentOperationResult> generateFailedResponses(SensorDataMessage message) {
        return generateFailedResponses(List.of(message));
    }

    List<DocumentOperationResult> generateFailedResponses(List<SensorDataMessage> messages) {
        var returnValue = new ArrayList<DocumentOperationResult>();

        for (var message : messages) {
            var id = String.format("%s-%s:%s", message.getSource(), message.getTenant(), message.getId());
            returnValue.add(DocumentOperationResult.newInstance(id, "failed", null));
        }

        return returnValue;
    }

    List<Message<SensorDataMessage>> getRabbitMessages(List<SensorDataMessage> messages) {
        var returnValue = new ArrayList<Message<SensorDataMessage>>();

        for (var message : messages) {
            var headers = new HashMap<String, Object>();
            headers.put(AmqpHeaders.DELIVERY_TAG, Long.valueOf(message.getOriginId()));

            returnValue.add(new GenericMessage<>(message, new MessageHeaders(headers)));
        }

        return returnValue;
    }

    List<SensorDataMessage> generateTestMessages() {
        return List.of(
            new SensorDataMessage("1", "1", "source1", "tenant1", Instant.EPOCH, null, List.of()),
            new SensorDataMessage("2", "2", "source1", "tenant1", Instant.EPOCH, null, List.of()),
            new SensorDataMessage("3", "3", "source1", "tenant2", Instant.EPOCH, null, List.of()),
            new SensorDataMessage("4", "4", "source1", "tenant2", Instant.EPOCH, null, List.of()),
            new SensorDataMessage("5", "5", "source2", "tenant1", Instant.EPOCH, null, List.of())
        );
    }
}
