/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatapersistence;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

/**
 * Microservice for storing {@link SensorDataMessage}s to one or multiple databases.
 * <p>
 * The Service listens for messages on an AMQP message broker for well-formed messages and stores
 * them.
 *
 * @author D. Ronnenberg
 */
@SpringBootApplication(
    exclude = {
        // The SDK comes with a mongodb-driver dependency which will trigger Spring's
        // autoconfiguration for MongoDB. Explicitly disable that.
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class
    }
)
public class SensorDataPersistenceServiceApplication {
    /**
     * Global main.
     */
    public static void main(String[] args) {
        SpringApplication.run(SensorDataPersistenceServiceApplication.class, args);
    }
}
