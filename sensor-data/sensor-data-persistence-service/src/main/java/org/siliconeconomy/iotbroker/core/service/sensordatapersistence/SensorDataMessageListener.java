/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatapersistence;

import com.rabbitmq.client.Channel;
import org.ektorp.CouchDbConnector;
import org.ektorp.DocumentOperationResult;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.util.CustomCollectors;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.util.SensorDataMessageUtils;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.utils.amqp.SensorDataExchangeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Spring {@link Service} to listen for {@link SensorDataMessage}s on an AMQP queue and for storing
 * these messages in a CouchDB.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Service
public class SensorDataMessageListener {
    /**
     * This class's logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorDataMessageListener.class);
    /**
     * Name of the queue for the {@link SensorDataMessageListener}.
     */
    private static final String QUEUE_NAME_COUCHDB = "sensor-data-persistence-service";
    /**
     * Database connector for CouchDB to perform bulk-inserts.
     */
    private final CouchDbConnector dbConnector;

    /**
     * Autowired constructor.
     */
    @Autowired
    public SensorDataMessageListener(CouchDbConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    /**
     * {@link RabbitListener} that is called when a message from the message broker is being
     * persisted.
     */
    @RabbitListener(
        bindings = @QueueBinding(
            value = @Queue(name = QUEUE_NAME_COUCHDB, durable = "true"),
            exchange = @Exchange(
                name = SensorDataExchangeConfiguration.NAME,
                type = SensorDataExchangeConfiguration.TYPE,
                ignoreDeclarationExceptions = "true"
            ),
            key = SensorDataExchangeConfiguration.ROUTING_KEY
        ),
        ackMode = "MANUAL"
    )
    public void processMessage(List<Message<SensorDataMessage>> messages, Channel channel) {
        LOGGER.debug("Processing {} messages", messages.size());

        // Convert DTOs to entities and map them to the corresponding message delivery tag
        var deliveryTagsByEntity = convertToDeliveryTagsByEntity(messages);

        // Execute bulk insert. There will only be results for documents that couldn't be persisted.
        List<DocumentOperationResult> bulkResults
            = dbConnector.executeBulk(deliveryTagsByEntity.keySet());

        Map<Boolean, Map<SensorDataMessageEntity, Long>> persistedEntities
            = partitionByPersistedEntities(deliveryTagsByEntity, bulkResults);

        // Reject not persisted messages
        nackMessages(persistedEntities.get(Boolean.FALSE), channel, bulkResults);

        // Acknowledge persisted messages
        ackMessages(persistedEntities.get(Boolean.TRUE), channel);
    }

    protected Map<Boolean, Map<SensorDataMessageEntity, Long>> partitionByPersistedEntities(
        Map<SensorDataMessageEntity, Long> deliveryTagsByEntity,
        List<DocumentOperationResult> bulkResults) {
        Map<String, DocumentOperationResult> resultsById = bulkResults.stream()
            .collect(Collectors.toMap(DocumentOperationResult::getId, Function.identity()));

        return deliveryTagsByEntity.entrySet().stream()
            .collect(
                Collectors.partitioningBy(
                    // If the bulk results do not contain an entry for a particular entity, the
                    // entity has been successfully persisted.
                    entry -> !resultsById.containsKey(entry.getKey().getId()),
                    // Use a linked hash map to preserve the message order
                    CustomCollectors.toLinkedHashMap(Map.Entry::getKey, Map.Entry::getValue)
                )
            );
    }

    protected boolean ackMessages(Map<SensorDataMessageEntity, Long> deliveryTagsByEntity,
                                  Channel channel) {
        var errorOccurred = new AtomicBoolean(false);

        // Ack all messages by acknowledging the message with the highest delivery tag
        deliveryTagsByEntity.values().stream()
            .max(Long::compare)
            .ifPresent(maxDeliveryTag -> {
                try {
                    LOGGER.debug("Acknowledging messages up to delivery tag '{}'", maxDeliveryTag);
                    channel.basicAck(maxDeliveryTag, true);
                } catch (IOException e) {
                    errorOccurred.set(true);
                    LOGGER.error("Error while acknowledging messages up to delivery tag '{}'",
                        maxDeliveryTag,
                        e
                    );
                }
            });

        return errorOccurred.get();
    }

    protected boolean nackMessages(Map<SensorDataMessageEntity, Long> deliveryTagsByEntity,
                                   Channel channel,
                                   List<DocumentOperationResult> bulkResults) {
        Map<String, DocumentOperationResult> resultsById = bulkResults.stream()
            .collect(Collectors.toMap(DocumentOperationResult::getId, Function.identity()));
        var errorOccurred = new AtomicBoolean(false);

        // Nack all unsuccessful CouchDB inserts by rejecting all messages respectively
        deliveryTagsByEntity.entrySet().stream()
            .forEach(entityByDeliveryTag -> {
                try {
                    LOGGER.warn("Rejecting message for entityId '{}'. (result={})",
                        entityByDeliveryTag.getKey().getId(),
                        resultsById.get(entityByDeliveryTag.getKey().getId())
                    );
                    channel.basicNack(entityByDeliveryTag.getValue(), false, false);
                } catch (IOException e) {
                    errorOccurred.set(true);
                    LOGGER.error("Error while rejecting message for entityId '{}'. (deliveryTag={})",
                        entityByDeliveryTag.getKey().getId(),
                        entityByDeliveryTag.getValue(),
                        e
                    );
                }
            });

        return errorOccurred.get();
    }

    protected Map<SensorDataMessageEntity, Long> convertToDeliveryTagsByEntity(
        List<Message<SensorDataMessage>> messages) {
        // Use a linked hash map to preserve the message order
        Map<SensorDataMessageEntity, Long> result = new LinkedHashMap<>();

        for (var message : messages) {
            var dto = message.getPayload();

            // Convert to entity, we need to copy it since the model is immutable
            var entity = new SensorDataMessageEntity(
                // Set new ID that matches the database's id schema <source>-<tenant>:<id>
                SensorDataMessageUtils.generateEntityId(dto),
                null,
                dto.getOriginId(),
                dto.getSource(),
                dto.getTenant(),
                dto.getTimestamp(),
                dto.getLocation(),
                dto.getDatastreams()
            );

            result.put(entity, message.getHeaders().get(AmqpHeaders.DELIVERY_TAG, Long.class));
        }

        return result;
    }
}
