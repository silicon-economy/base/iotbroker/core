/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.messages;

import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MessageNotFoundException;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MultipleMessagesFoundException;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;

/**
 * Provides REST endpoints to query {@link SensorDataMessage}s of devices.
 *
 * @author D. Ronnenberg
 */
@RestController
@RequestMapping("/messages")
public class MessagesController {
    /**
     * Used to retrieve messages from the database.
     */
    private final MessageService messageService;

    @Autowired
    public MessagesController(MessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * Get messages for a given device, specified by source and tenant.
     *
     * @param source    The source (device type) of the device that is being queried.
     * @param tenant    The tenant (id) of the device that is being queried.
     * @param startTime The lower limit of time that is of interest. All messages earlier will be
     *                  filtered out.
     * @param endTime   The upper limit of time that is of interest. All messages later will be
     *                  filtered out.
     * @param offset    Number of elements to ignore at the beginning.
     * @param limit     Maximum number of elements to return.
     * @param sort      The order in which the elements are sorted (sorted by time).
     * @return A list of {@link SensorDataMessage}.
     */
    @GetMapping("/sources/{source}/tenants/{tenant}")
    public List<SensorDataMessage> getMessagesBySourceAndTenant(
        @PathVariable("source") String source,
        @PathVariable("tenant") String tenant,
        @RequestParam(defaultValue = "1970-01-01T00:00:00Z")
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant startTime,
        @RequestParam(defaultValue = "9999-12-31T23:59:59.999999Z")
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant endTime,
        @RequestParam(defaultValue = "0") int offset,
        @RequestParam(defaultValue = "100") int limit,
        @RequestParam(defaultValue = "asc") String sort) {
        return messageService.findAllBySourceAndTenant(source, tenant, offset, limit,
            Sort.Direction.fromString(sort), startTime, endTime);
    }

    /**
     * Get latest message for a given device, specified by source and tenant.
     *
     * @param source The source (device type) of the device that is being queried.
     * @param tenant The tenant (id) of the device that is being queried.
     * @return The latest message for the given device.
     */
    @GetMapping("/sources/{source}/tenants/{tenant}/latest")
    public SensorDataMessage getLatestMessageBySourceAndTenant(
        @PathVariable("source") String source,
        @PathVariable("tenant") String tenant) {
        return messageService.findLatestBySourceAndTenant(source, tenant);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleIllegalArgumentException(Exception exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(MessageNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleMessageNotFoundException(Exception exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(MultipleMessagesFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleMultipleMessagesFoundException(Exception exception) {
        return exception.getMessage();
    }
}
