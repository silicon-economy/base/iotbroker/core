/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.messages;

import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.util.SensorDataMessageUtils;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MessageNotFoundException;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MultipleMessagesFoundException;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides methods for performing database operations for the {@link SensorDataMessageEntity}.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
@Service
public class MessageService {

    /**
     * The repository used to retrieve messages from the database.
     */
    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    /**
     * Returns all messages for the device with the given source and tenant.
     *
     * @param source    The source.
     * @param tenant    The tenant.
     * @param offset    Number of elements to skip.
     * @param limit     Maximum number of elements to return (excluding the ones skipped).
     * @param sort      The direction in which to sort the elements.
     * @param startTime The start time for message that are of interest.
     * @param endTime   The end time for message that are of interest.
     * @return A list of {@link SensorDataMessage}s.
     */
    public List<SensorDataMessage> findAllBySourceAndTenant(String source,
                                                            String tenant,
                                                            int offset,
                                                            int limit,
                                                            Sort.Direction sort,
                                                            Instant startTime,
                                                            Instant endTime) {
        return messageRepository
            .findByTimestamp(source, tenant, sort.isDescending(), offset, limit, startTime, endTime)
            .stream()
            .map(this::convertToDto)
            .collect(Collectors.toList());
    }

    /**
     * Returns the latest/most recent message for the device with the given source and tenant.
     *
     * @param source The source.
     * @param tenant The tenant.
     * @return The latest/most recent {@link SensorDataMessage}.
     */
    public SensorDataMessage findLatestBySourceAndTenant(String source, String tenant) {
        var startTime = Instant.parse("1970-01-01T00:00:00Z");
        var endTime = Instant.parse("9999-12-31T23:59:59.999999Z");

        var messages
            = messageRepository.findByTimestamp(source, tenant, true, 0, 1, startTime, endTime);

        switch (messages.size()) {
            case 0:
                throw new MessageNotFoundException(source, tenant);
            case 1:
                return convertToDto(messages.get(0));
            default:
                throw new MultipleMessagesFoundException(source, tenant);
        }
    }

    protected SensorDataMessage convertToDto(SensorDataMessageEntity entity) {
        return new SensorDataMessage(
            SensorDataMessageUtils.extractMessageId(entity),
            entity.getOriginId(),
            entity.getSource(),
            entity.getTenant(),
            entity.getTimestamp(),
            entity.getLocation(),
            entity.getDatastreams()
        );
    }
}
