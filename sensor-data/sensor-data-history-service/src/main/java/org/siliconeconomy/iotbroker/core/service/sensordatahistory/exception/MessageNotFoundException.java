/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception;

/**
 * Thrown to indicate that a message for a device with a specific source and tenant could not be
 * found in the database.
 *
 * @author M. Grzenia
 */
public class MessageNotFoundException extends RuntimeException {

    public MessageNotFoundException(String source, String tenant) {
        super(String.format(
            "Could not find message for source '%s' and tenant '%s'.",
            source,
            tenant
        ));
    }
}
