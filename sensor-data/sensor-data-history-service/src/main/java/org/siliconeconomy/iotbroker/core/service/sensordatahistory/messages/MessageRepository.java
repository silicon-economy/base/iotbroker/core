/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.support.View;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.config.CouchDbConfig;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.utils.couchdb.PartitionedCouchDbConnector;
import org.siliconeconomy.iotbroker.utils.couchdb.PartitionedRepositorySupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

/**
 * Provides database access for {@link SensorDataMessageEntity}.
 * <p>
 * Provides basic CRUD operations and methods for more complex database queries.
 *
 * @author D. Ronnenberg
 */
@Repository
public class MessageRepository extends PartitionedRepositorySupport<SensorDataMessageEntity> {

    @Autowired
    public MessageRepository(@Qualifier("messageDatabase") PartitionedCouchDbConnector db,
                             @Qualifier("viewQueryObjectMapper") ObjectMapper viewQueryObjectMapper) {
        super(
            SensorDataMessageEntity.class,
            db,
            CouchDbConfig.MESSAGE_DB_DESIGN_DOC_NAME,
            viewQueryObjectMapper
        );

        // Create or update design documents
        initStandardDesignDocument();
    }

    /**
     * Query {@link SensorDataMessageEntity}s for a single device.
     * <p>
     * This function also supports pagination using the additional parameters.
     *
     * @param source     The source to query.
     * @param tenant     The tenant to query, also known as the deviceId.
     * @param descending Flag that indicates the a descending order should be used.
     * @param offset     Number of elements to ignore at the beginning.
     * @param limit      Maximum number of elements to return.
     * @param startTime  The lower limit of time that is of interest. All messages earlier will be
     *                   filtered out.
     * @param endTime    The upper limit of time that is of interest. All messages later will be
     *                   filtered out.
     * @return A list of {@link SensorDataMessageEntity}.
     */
    @View(name = "by-timestamp", file = "by-timestamp.json")
    public List<SensorDataMessageEntity> findByTimestamp(String source,
                                                         String tenant,
                                                         boolean descending,
                                                         int offset,
                                                         int limit,
                                                         Instant startTime,
                                                         Instant endTime) {
        var partition = String.format("%s-%s", source, tenant);
        var query = createQuery("by-timestamp", partition)
            .skip(offset)
            .limit(limit)
            .includeDocs(true);

        if (!descending) {
            query = query
                .startKey(startTime)
                .endKey(endTime);
        } else {
            query = query
                .startKey(endTime)
                .endKey(startTime)
                .descending(true);
        }

        return db.queryView(query, SensorDataMessageEntity.class);
    }
}
