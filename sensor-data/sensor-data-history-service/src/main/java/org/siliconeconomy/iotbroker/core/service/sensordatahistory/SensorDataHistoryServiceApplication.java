/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication(
    exclude = {
        // The SDK comes with a mongodb-driver dependency which will trigger Spring's
        // autoconfiguration for MongoDB. Explicitly disable that.
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class
    }
)
public class SensorDataHistoryServiceApplication {
    /**
     * Global main.
     */
    public static void main(String[] args) {
        SpringApplication.run(SensorDataHistoryServiceApplication.class, args);
    }
}
