/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception;

/**
 * Thrown to indicate that multiple messages for a device with a specific source and tenant were
 * found in the database when only a single message was expected.
 *
 * @author M. Grzenia
 */
public class MultipleMessagesFoundException extends RuntimeException {

    public MultipleMessagesFoundException(String source, String tenant) {
        super(String.format(
            "Found multiple messages for source '%s' and tenant '%s'.",
            source,
            tenant
        ));
    }
}
