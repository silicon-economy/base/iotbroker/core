/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.messages;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.util.SensorDataMessageUtils;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MessageNotFoundException;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MultipleMessagesFoundException;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link MessageService}.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
@SpringBootTest(classes = {MessageService.class})
class MessageServiceTest {

    /**
     * Class under test.
     */
    @Autowired
    @InjectMocks
    private MessageService service;
    /**
     * Test dependencies.
     */
    @MockBean
    private MessageRepository repository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void findAllBySourceAndTenant() {
        // Arrange
        var entity = getEntity();
        when(repository.findByTimestamp(anyString(), anyString(), anyBoolean(), anyInt(),
            anyInt(), any(Instant.class), any(Instant.class))).thenReturn(List.of(entity));

        // Act
        var messages = service
            .findAllBySourceAndTenant("source1", "tenant1", 0, 100, Sort.Direction.ASC,
                Instant.parse("1970-01-01T00:00:00Z"), Instant.parse("9999-12-31T23:59:59.999999Z"));

        // Assert
        assertThat(messages)
            .hasSize(1)
            .allSatisfy(element -> compareEntityAndDto(entity, element));
    }

    @Test
    void findLatestBySourceAndTenant() {
        var entity = getEntity();
        when(repository.findByTimestamp(anyString(), anyString(), anyBoolean(), anyInt(),
            anyInt(), any(Instant.class), any(Instant.class))).thenReturn(List.of(entity));

        var message = service.findLatestBySourceAndTenant("source1", "tenant1");

        compareEntityAndDto(entity, message);
    }

    @Test
    void findLatestBySourceAndTenant_noMessageFound() {
        var entity = getEntity();
        when(repository.findByTimestamp(anyString(), anyString(), anyBoolean(), anyInt(),
            anyInt(), any(Instant.class), any(Instant.class))).thenReturn(List.of());

        assertThatThrownBy(() -> service.findLatestBySourceAndTenant("source1", "tenant1"))
            .isInstanceOf(MessageNotFoundException.class);
    }

    @Test
    void findLatestBySourceAndTenant_multipleMessagesFound() {
        var entity = getEntity();
        when(repository.findByTimestamp(anyString(), anyString(), anyBoolean(), anyInt(),
            anyInt(), any(Instant.class), any(Instant.class))).thenReturn(List.of(entity, entity));

        assertThatThrownBy(() -> service.findLatestBySourceAndTenant("source1", "tenant1"))
            .isInstanceOf(MultipleMessagesFoundException.class);
    }

    @Test
    void convertToDto() {
        var entity = getEntity();
        var dto = service.convertToDto(entity);

        compareEntityAndDto(entity, dto);
    }

    private SensorDataMessageEntity getEntity() {
        return new SensorDataMessageEntity(
            "source1-tenant1:uuid",
            null,
            "origin-id",
            "soruce1",
            "tenant1",
            Instant.EPOCH,
            null,
            List.of()
        );
    }

    private void compareEntityAndDto(SensorDataMessageEntity entity, SensorDataMessage dto) {
        assertEquals(SensorDataMessageUtils.extractMessageId(entity), dto.getId());
        assertEquals(entity.getOriginId(), dto.getOriginId());
        assertEquals(entity.getSource(), dto.getSource());
        assertEquals(entity.getTenant(), dto.getTenant());
        assertEquals(entity.getTimestamp(), dto.getTimestamp());
        assertEquals(entity.getLocation(), dto.getLocation());
        assertEquals(entity.getDatastreams(), dto.getDatastreams());
    }
}
