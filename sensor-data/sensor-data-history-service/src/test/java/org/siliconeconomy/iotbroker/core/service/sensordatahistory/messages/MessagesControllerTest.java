/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.messages;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.config.WebConfig;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MessageNotFoundException;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MultipleMessagesFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link MessagesController}.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@SpringBootTest(classes = {MessagesController.class, WebConfig.class})
class MessagesControllerTest {

    /**
     * Class under test.
     */
    @Autowired
    @InjectMocks
    private MessagesController messagesController;
    /**
     * Test dependencies.
     */
    @MockBean
    private MessageService messageService;
    /**
     * Test environment.
     */
    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        this.mockMvc = MockMvcBuilders
            .standaloneSetup(messagesController)
            .build();
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getMessagesBySourceAndTenant() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/messages/sources/{source}/tenants/{tenant}", "source1", "tenant1")
            )
            .andReturn();

        // Assert & Verify
        verify(messageService)
            .findAllBySourceAndTenant("source1", "tenant1", 0, 100, Sort.Direction.ASC,
                Instant.parse("1970-01-01T00:00:00Z"), Instant.parse("9999-12-31T23:59:59.999999Z"));
    }

    @Test
    void getMessagesBySourceAndTenant_invalidSort() throws Exception {
        // Act & Assert
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/messages/sources/{source}/tenants/{tenant}", "source1", "tenant1")
                .queryParam("sort", "no-valid-imput")
            )
            .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void getLatestMessageBySourceAndTenant() throws Exception {
        // Act
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/messages/sources/{source}/tenants/{tenant}/latest", "source1", "tenant1")
            )
            .andReturn();

        // Assert & Verify
        verify(messageService).findLatestBySourceAndTenant("source1", "tenant1");
    }

    @Test
    void getLatestMessageBySourceAndTenant_noMessageFound() throws Exception {
        // Arrange
        when(messageService.findLatestBySourceAndTenant(anyString(), anyString()))
            .thenThrow(new MessageNotFoundException("somesource", "sometenant"));

        // Act & Assert
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/messages/sources/{source}/tenants/{tenant}/latest", "source1", "tenant1")
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void getLatestMessageBySourceAndTenant_multipleMessagesFound() throws Exception {
        // Arrange
        when(messageService.findLatestBySourceAndTenant(anyString(), anyString()))
            .thenThrow(new MultipleMessagesFoundException("somesource", "sometenant"));

        // Act & Assert
        mockMvc
            .perform(MockMvcRequestBuilders
                .get("/messages/sources/{source}/tenants/{tenant}/latest", "source1", "tenant1")
            )
            .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }
}
