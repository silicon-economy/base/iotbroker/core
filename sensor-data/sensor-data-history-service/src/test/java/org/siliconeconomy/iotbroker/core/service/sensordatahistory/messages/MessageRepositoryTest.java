/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.ektorp.ViewQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.model.SensorDataMessageEntity;
import org.siliconeconomy.iotbroker.utils.couchdb.PartitionedCouchDbConnector;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link MessageRepository}.
 *
 * @author M. Grzenia
 */
class MessageRepositoryTest {

    /**
     * Class under test.
     */
    private MessageRepository repository;
    /**
     * Test dependencies.
     */
    private PartitionedCouchDbConnector couchDbConnector;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        couchDbConnector = mock(PartitionedCouchDbConnector.class);
        objectMapper = new ObjectMapper()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .registerModule(new JavaTimeModule());

        repository = new MessageRepository(couchDbConnector, objectMapper);
    }

    @Test
    void findByTimestamp_sortAscending() {
        // Arrange
        Instant startTime = Instant.parse("2021-08-16T08:35:05.0Z");
        Instant endTime = Instant.parse("2021-08-16T08:35:15.0Z");

        // Act
        repository.findByTimestamp(
            "somesource",
            "sometenant",
            false,
            0,
            10,
            startTime,
            endTime
        );

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(couchDbConnector)
            .queryView(viewQueryCaptor.capture(), eq(SensorDataMessageEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getStartKey,
                ViewQuery::getEndKey,
                ViewQuery::isDescending,
                ViewQuery::getSkip,
                ViewQuery::getLimit,
                ViewQuery::isIncludeDocs
            )
            .containsExactly(
                "_partition/somesource-sometenant/_design/messages",
                "by-timestamp",
                startTime,
                endTime,
                false,
                0,
                10,
                true
            );
    }

    @Test
    void findByTimestamp_sortDescending() {
        // Arrange
        Instant startTime = Instant.parse("2021-08-16T08:35:05.0Z");
        Instant endTime = Instant.parse("2021-08-16T08:35:15.0Z");

        // Act
        repository.findByTimestamp(
            "somesource",
            "sometenant",
            true,
            0,
            10,
            startTime,
            endTime
        );

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(couchDbConnector)
            .queryView(viewQueryCaptor.capture(), eq(SensorDataMessageEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getStartKey,
                ViewQuery::getEndKey,
                ViewQuery::isDescending,
                ViewQuery::getSkip,
                ViewQuery::getLimit,
                ViewQuery::isIncludeDocs
            )
            .containsExactly(
                "_partition/somesource-sometenant/_design/messages",
                "by-timestamp",
                endTime,
                startTime,
                true,
                0,
                10,
                true
            );
    }
}
