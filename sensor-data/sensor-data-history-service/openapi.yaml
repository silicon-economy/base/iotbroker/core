openapi: 3.0.3
info:
  title: Sensor Data History Service
  description: Definition of the REST API for the Sensor Data History Service
  license:
    name: Open Logistics License, Version 1.0
  version: 1.0.0
servers:
  - url: http://sensor-data-history-service.apps.sele.iml.fraunhofer.de:8080
    description: Default instance on IoT Broker cluster.
  - url: http://localhost:8080
    description: URL for local debugging.
tags:
  - name: Messages
    description: Endpoint for querying Sensor Data Messages for a given tenant.
paths:
  /messages/sources/{source}/tenants/{tenant}:
    get:
      tags:
        - Messages
      summary: Get messages for a given tenant.
      parameters:
        - name: source
          in: path
          required: true
          description: Source or type of the device that is being queried.
          example: sensingpuck
          schema:
            type: string
        - name: tenant
          in: path
          required: true
          description: ID of the device that is being queried.
          example: 23421
          schema:
            type: string
        - in: query
          name: startTime
          description: Limit the query to messages after this timestamp.
          schema:
            type: string
            format: date-time
            default: "1970-01-01T00:00:00Z"
        - in: query
          name: endTime
          description: Limit the query to messages before this timestamp.
          schema:
            type: string
            format: date-time
            default: "9999-12-31T23:59:59.999999Z"
        - in: query
          name: offset
          description: "Number of messages to skip."
          schema:
            type: integer
            format: int32
            default: 0
        - in: query
          name: limit
          description: "The maximum number of items to return."
          schema:
            type: integer
            format: int32
            default: 100
        - in: query
          name: sort
          description: The sorting to be applied.
          schema:
            type: string
            enum: [ ASC, asc, DESC, desc ]
            default: "asc"
      responses:
        "200": # status code
          description: A JSON array of messages.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/SensorDataMessage"
        "400":
          description: Invalid parameter value(s).
  /messages/sources/{source}/tenants/{tenant}/latest:
    get:
      tags:
        - Messages
      summary: Get the latest message for the given tenant.
      parameters:
        - name: source
          in: path
          description: Source or type of the device that is being queried.
          required: true
          example: sensingpuck
          schema:
            type: string
        - name: tenant
          in: path
          description: ID of the device that is being queried.
          required: true
          example: 23421
          schema:
            type: string
      responses:
        "200": # status code
          description: A single message.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/SensorDataMessage"
        "404":
          description: Referencing source or tenant for which the latest message could not be found.
        "500":
          description: Referencing source or tenant for which there are multiple latest messages.
components:
  schemas:
    SensorDataMessage:
      type: object
      required:
        - id
        - originId
        - source
        - tenant
        - timestamp
      properties:
        id:
          type: string
          description: A unique identifier for the message.
          example: sensingpuck-2413:e4c0d8dd-7ff7-4e7c-b31c-e22215cb1707
        originId:
          type: string
          description: >-
            Reference to the message that originally contained the sensor data.
            Optional: May be an empty string if no reference back to the original device message is necessary or exists.
          example: sensingpuck-2413:123e4567-e89b-12d3-a456-426614174000
        source:
          type: string
          description: The source that the device represents. (E.g. as a source of messages that contain data.)
          example: sensingpuck
        tenant:
          type: string
          description: >-
            A unique identifier for the device. (E.g. the device ID.)
            This identifier is only unique in the context of the device's source.
          example: 2413
        timestamp:
          type: string
          format: date-time
          description: >-
            The time the device sent the data.
            This time may be identical to the time of one or multiple observations in one or multiple datastreams.
            However, this may not be the case for observations made by the device before this time.
          example: "2020-11-12T12:56:55Z"
        location:
          description: >-
            The (optional) location of the device.
            If the device cannot determine its location, the value is `null`.
          oneOf:
            - $ref: "#/components/schemas/LatitudeLongitudeLocation"
            - $ref: "#/components/schemas/MapcodeLocation"
            - $ref: "#/components/schemas/SimpleLocation"
          discriminator:
            propertyName: encodingType
        datastreams:
          type: array
          description: >-
            A list of datastreams, each grouping a collection of observations measuring the same observed property.
          items:
            anyOf:
              - $ref: "#/components/schemas/CountResultDatastream"
              - $ref: "#/components/schemas/MeasurementResultDatastream"
              - $ref: "#/components/schemas/SimpleResultDatastream"
              - $ref: "#/components/schemas/TruthResultDatastream"
            discriminator:
              propertyName: observationType
    Location:
      description: A location describes the location of a device or the location where an observation happened.
      type: object
      required:
        - name
        - description
        - encodingType
        - details
      properties:
        name:
          type: string
          description: >-
            A label for the location, commonly a descriptive name.
            May be an empty string.
          example: Tenant 1 location name
        description:
          type: string
          description: >-
            A description for the location.
            May be an empty string.
          example: Tenant 1 location description
        encodingType: # This field is used as the descriminator.
          type: string
          description: The (encoding) type of the location details.
        details:
          description: The details of the actual location.
    LatitudeLongitudeLocation:
      allOf: # Inherit from Location
        - $ref: '#/components/schemas/Location'
      description:  A location represented using a tuple of latitude and longitude coordinates.
      properties:
        encodingType:
          default: LATLONG
        details:
          $ref: "#/components/schemas/LatitudeLongitudeDetails"
    LatitudeLongitudeDetails:
      required:
        - latitude
        - longitude
      type: object
      description: The details of a LatitudeLongitudeLocation described using latitude and longitude coordinates.
      properties:
        latitude:
          type: number
          example: 51.493941
        longitude:
          type: number
          example: 7.407436
    MapcodeLocation:
      allOf: # Inherit from Location
        - $ref: '#/components/schemas/Location'
      description: A location represented using a Mapcode.
      properties:
        encodingType:
          default: MAPCODE
        details:
          $ref: "#/components/schemas/MapcodeDetails"
    MapcodeDetails:
      required:
        - territory
        - code
      type: object
      description: The details of a MapcodeLocation described using a Mapcode.
      properties:
        territory:
          type: string
          example: "DEU"
        code:
          type: string
          example: "90D.8V7"
    SimpleLocation:
      allOf: # Inherit from Location
        - $ref: '#/components/schemas/Location'
      description: A location represented using a string.
      properties:
        encodingType:
          default: SIMPLE
        details:
          $ref: "#/components/schemas/SimpleDetails"
    SimpleDetails:
      type: string
      description: The details of a SimpleLocation described in the simplest and most generic way using a string.
      example: "A comfy place."
    Datastream:
      required:
        - observedProperty
        - observationType
        - unitOfMeasurement
        - observations
      type: object
      description: >-
        A datastream groups a collection of observations measuring the same observed property.
        An observed property corresponds to a specific type of data (e.g. ambient temperature, ambient humidity, CPU temperature or battery voltage).
      properties:
        observedProperty:
          $ref: "#/components/schemas/ObservedProperty"
        observationType: # This field is used as the descriminator.
          type: string
          description: The type of observations grouped by the datastream.
        unitOfMeasurement:
          $ref: "#/components/schemas/UnitOfMeasurement"
        observations:
          type: array
          description: A list of observations, each describing the act of measuring or otherwise determining the value of an observed property.
          items:
            $ref: "#/components/schemas/Observation"
    CountResultDatastream:
      allOf: # Inherit from Datastream
        - $ref: '#/components/schemas/Datastream'
      description: Datastream for observations with datatype `int`.
      properties:
        observationType:
          default: COUNT
        observations:
          items:
            $ref: "#/components/schemas/CountObservation"
    MeasurementResultDatastream:
      allOf: # Inherit from Datastream
        - $ref: '#/components/schemas/Datastream'
      description: Datastream for observations with datatype `number`.
      properties:
        observationType:
          default: MEASUREMENT
        observations:
          items:
            $ref: "#/components/schemas/MeasurementObservation"
    SimpleResultDatastream:
      allOf: # Inherit from Datastream
        - $ref: '#/components/schemas/Datastream'
      description: Datastream for generic observations with datatype `string`.
      properties:
        observationType:
          default: SIMPLE
        observations:
          items:
            $ref: "#/components/schemas/SimpleObservation"
    TruthResultDatastream:
      allOf: # Inherit from Datastream
        - $ref: '#/components/schemas/Datastream'
      description: Datastream for observations with datatype `boolean`.
      properties:
        observationType:
          default: TRUTH
        observations:
          items:
            $ref: "#/components/schemas/TruthObservation"
    Observation:
      type: object
      description: An observation describes the act of measuring or otherwise determining the value of an observed property.
      required:
        - timestamp
        - result
        - parameters
      properties:
        timestamp:
          type: string
          format: date-time
          description: The time the observation happened.
          example: "2020-11-12T12:56:55Z"
        result:
          description: The result of the observation.
        location:
          description: >-
            The (optional) location where the observation happened.
            The location of an observation may be identical to the location of the device that made the observation.
            If the device that made the observation cannot determine the location of the observation, the value is `null`.
          oneOf:
            - $ref: "#/components/schemas/LatitudeLongitudeLocation"
            - $ref: "#/components/schemas/MapcodeLocation"
            - $ref: "#/components/schemas/SimpleLocation"
          discriminator:
            propertyName: encodingType
        parameters:
          type: object
          description: >-
            A map of generic parameters.
            These parameters can be used to attach more information to the observation.
          example: { "paramKey": "paramValue" }
    CountObservation:
      allOf: # Inherit from Observation
        - $ref: '#/components/schemas/Observation'
      description: The result of counting.
      properties:
        result:
          type: integer
          example: 21
    MeasurementObservation:
      allOf: # Inherit from Observation
        - $ref: '#/components/schemas/Observation'
      description: The result of a measurement.
      properties:
        result:
          type: number
          example: 3.142
    SimpleObservation:
      allOf: # Inherit from Observation
        - $ref: '#/components/schemas/Observation'
      description: The result of a generic observation.
      properties:
        result:
          type: string
          example: "Moving at the speed of sound."
    TruthObservation:
      allOf: # Inherit from Observation
        - $ref: '#/components/schemas/Observation'
      description: The result of a boolean operation.
      properties:
        result:
          type: boolean
          example: true
    ObservedProperty:
      type: object
      description: An abstract representation of an observed property.
      required:
        - name
        - description
      properties:
        name:
          type: string
          description: A label for the observed property, commonly a descriptive name.
          example: Property 1
        description:
          type: string
          description: A description for the observed property.
          example: Property 1 description
    UnitOfMeasurement:
      description: A representation for units of measurement.
      type: object
      required:
        - name
        - symbol
      properties:
        name:
          description: >-
            The full name of the unit of measurement.
            If there is no unit name, the value is an empty string.
          type: string
          example: milliwatt
        symbol:
          description: >-
            The textual form of the unit symbol.
            If there is no unit symbol, the value is an empty string.
          type: string
          example: mW
